//ZeroTrace.m

//Initialization
clear;

//Defining the parameters;
p := 31;
n := 3;


//Definig the function fields
k := FiniteField(p);

// These objects are not necessay if we define the function field 
// using curve instead of polynomial
kx<x> := RationalFunctionField(k);
kxy<y> := PolynomialRing(kx);

A2<x,y> := AffineSpace(k,2);
a := k!1;
b := k!2;
HPoly := y^2 - x^3 -a*x -b;  //y^2 - x^5 + 5*x^3 - 4*x;
HCrv<x,y> := Curve(A2, HPoly);

print "The disc of the Curve is: ", 16*(a^3 + 27* b^2);
//These two function fields are essentially the same however,
//In the first one is less friendly with fields operations
//while the second can't handle Riemann-Roch stuffs. :(
FHC := FunctionField(HCrv); 
FHP<x,y> := FunctionField(HPoly);

print "The genus of the function field is: ", Genus(FHP);

//A random point hopefully works
RanP := Random(Points(HCrv));
Div3P := 3* Divisor(RanP);
SubFuncSpace, TheFunc := RiemannRochSpace(Div3P);
SubFunBasis := Basis(SubFuncSpace);

print "The generators of the Riemann-Roch space are:";
for BasisElm in SubFunBasis do
    print TheFunc(BasisElm);
end for;

GenFuncInFH := TheFunc(SubFunBasis[1]);

//As Magma is a retard I need to tell it that these two fields are equal
IdFHCtoFHP := hom<FHC->FHP| x,y>;

kbxyb := PolynomialRing(k, 2);
kx<x> := RationalFunctionField(k);
kxbTb<T> := PolynomialRing(kx);

IdkbxybtokxbTb := hom<kbxyb->kxbTb| x,T>;
HPolyCo := kbxyb ! HPoly;

HPolyA := IdkbxybtokxbTb(HPolyCo);
FHA<y> := FunctionField(HPolyA);

IdFHCtoFHA := hom<FHC->FHA | x,y>;
GenFuncA := IdFHCtoFHA(GenFuncInFH);

MinPGenFunc := MinimalPolynomial(GenFuncA);

ComDen :=1;
for CurCoef in Coefficients(MinPGenFunc) do
  ComDen := LCM(ComDen, Denominator(CurCoef));
end for;
NewMinPMulti := MinPGenFunc * ComDen;

print "We are here";
//The whole plan is to look at x as element of K(T)
//I should stop Magma from insisting that T is in K(x)
//hence Global := false

/*kT<T> := RationalFunctionField(k : Global := false);
kTbxb<x> := PolynomialRing(kT : Global := false);

Revkx2kT := hom<kx->kTbxb|x>;
RevkxbTb2kTbxb := hom<kxbTb -> kTbxb | Revkx2kT, T>;

RevMinPMulti := RevkxbTb2kTbxb(NewMinPMulti);
print "";
print "Find the minimal polynomials among:";
Factorization(RevMinPMulti);
*/

kT<T> := RationalFunctionField(k);
kTbxb<x> := PolynomialRing(kT);

Revkx2kT := hom<kx->kTbxb|x>;
RevkxbTb2kTbxb := hom<kxbTb -> kTbxb | Revkx2kT, T>;

RevMinPMulti := RevkxbTb2kTbxb(NewMinPMulti);
print "";
print "Find the minimal polynomials among:";
Factorization(RevMinPMulti);

//RH<x,y> := CoordinateRing(H); 
//FFrH<x,y> := FieldOfFractions(RH);                                                                    
//GenFunc := FFrH ! GenFuncInFH;

//MinimalPolynomial(GenFunc);
//$.1^2 + (7*x^2 + 27*x + 24)/(x^3 + 23*x^2 + 11*x + 4)*$.1 + (30*x^2 + 12*x + 5)/(x^3 + 23*x^2 + 11*x + 4)
//> SH<T> := PolynomialRing(TF);
//> MinGenFunc := SH ! MinimalPolynomial(GFunct);
//  TF<y> := FunctionField(CurvePoly); 
