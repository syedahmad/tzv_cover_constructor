#!/usr/bin/env python
import optparse
import os
import subprocess

def main():
#This is for parsing options that I'm not using for now
    p = optparse.OptionParser(description="Python 'ls' command clone",
                                prog="pyls",
                                version="0.1a",
                                usage="%prog [directory]")
    p.add_option("--chatty", "-c", action="store", type="choice",
                    dest="chatty",
                    choices=["normal", "verbose", "quiet"],
                    default="normal")
    options, arguments = p.parse_args()
    print options

    if len(arguments) >= 1:
        if options.chatty == "verbose":
            print "Verbose Mode Enabled"
        for filename in arguments:
            if (filename.find('C') == -1) or (filename.find('.mgm')==-1):
                print "Invalid filename convention: " + filename
                pass
            else:
                MagmaCmd = 'magma ' + filename + ' > ' + filename[filename.find('C'):filename.find('.mgm')] +'_res.txt &'
                print MagmaCmd
                subprocess.call(MagmaCmd, shell=True)
    else:
        p.print_help()
if __name__ == '__main__':
    main()
