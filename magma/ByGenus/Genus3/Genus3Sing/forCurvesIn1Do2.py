#!/usr/bin/env python
import optparse
import os
import subprocess

def main():
    #I'm not using option processing for now
    p = optparse.OptionParser(description="Python 'ls' command clone",
                                prog="pyls",
                                version="0.1a",
                                usage="%prog [directory]")
    p.add_option("--chatty", "-c", action="store", type="choice",
                    dest="chatty",
                    choices=["normal", "verbose", "quiet"],
                    default="normal")
    options, arguments = p.parse_args()
    print options
    
    if len(arguments) == 2:
        valueList = open(arguments[0], 'r');
        if options.chatty == "verbose":
            print "Verbose Mode Enabled"

        templateFilename = arguments[1];
        if (templateFilename.find('.mgm')==-1):
            print "Invalid filename convention: " + filename
            pass

        else:
            #Making a folder to store results, it's not a big deal if
            #it already exits
            try:
                os.mkdir('results');
            except OSError:
                print 'results directory already exists, overwriting results!'

            tempFile = open(templateFilename, 'r');
            for curValue in valueList :
                print curValue
                valueInd, valueLine = curValue.split(',');

                indFilename = templateFilename[:templateFilename.find('.mgm')]+valueInd+'.mgm';
                indFile = open(indFilename, 'w')
                tempFile.seek(0, os.SEEK_SET)
                for templine in tempFile:
                    if (templine == 'EXTERNAL_VALUE\n'):
                        indFile.write(valueLine)
                    else:
                        indFile.write(templine)

                indFile.close()
                MagmaCmd = 'magma ' + indFilename + ' > results/' + valueInd +'_res.txt &'
                print MagmaCmd
                subprocess.call(MagmaCmd, shell=True)
    else:
        p.print_help()
if __name__ == '__main__':
    main()
