#This file is to generate the group of candidates of genus 3 curves and their fixed field.
#/lu101/sahosse/doc/RecherchePhD/Codes/sage/AutGrps/
#this is for the finite field as constant field

from os import chdir

#chdir('/lu101/sahosse/doc/phd/sage/autgrp/')
set_verbose(0)

#from fixed_field_comp import fixed_field
from sys import exit, stdout

from sage.misc.misc import set_verbose


#@profile
def compute_all_fixed_field(coverField, coverAutGrp):

    print "Computing fixed field of ", coverField, " with automorphism groupid ", coverAutGrp.group_id()
    print "from", len(AutGrp.conjugacy_classes_representatives()), "conjugacy classes, we need to check", sum([g.order() == 2 for g in AutGrp.conjugacy_classes_representatives()])
    print
    stdout.flush()
    y = coverField.gens()[0]
    x = coverField.rational_function_field().gens()[0]
    for f in coverAutGrp.conjugacy_classes_representatives():
        if (f.order() == 3):
            print "Class rep f with"
            stdout.flush()
            print 'f(x) = ', f(x)
            stdout.flush()
            print 'f(y) = ', f(y)
            stdout.flush()
            (curfield, subEmbed) = f.fixed_field(['t','z'])
            stdout.flush()
            print '*****************************RESULT*****************************'
            print 'The fixed field of f is'
            print curfield
            try:
                print 'of genus ', curfield.genus()
            except:
                print 'unable to compute genus'
            print
            print 'using embedding'
            print subEmbed
            stdout.flush()
            zImg = subEmbed(curfield.gens()[0])
            tImg = subEmbed(curfield.rational_function_field().gen())
            assert(zImg == f(zImg))
            assert(tImg == f(tImg))

set_verbose(2)

nicePrime = 11
extenPower = 1 #this will give me 8'th root of unity

k = FiniteField(nicePrime^extenPower)

# a = k(primitive_root(nicePrime))
# zeta_3 = a^((k.order()-1)/3)
# zeta_6 = a^((k.order()-1)/6)
# zeta_4 = a^((k.order()-1)/4)
#zeta_8 = a^((k.order()-1)/8)

#zeta_7 = a^((k.order()-1)/7)
#sqtn7 = k(-7).square_root()


#(12,4)
#(D_12)
#nicePrime = 13

#k = FiniteField(nicePrime)
# kB.<b, c, d> = PolynomialRing(k)
# kb = kB.fraction_field()
# b = kb(b)
# c = kb(c)
# d = kb(d)
kx.<x> = FunctionField(k)
kxY.<Y> = kx[];

kxy.<y> = kx.extension(Y^6 + 9*Y^5*x^4 + 7*Y^5*x^3 + 7*Y^5*x^2 + 7*Y^5*x + 9*Y^5 + 9*Y^4*x^8 + 8*Y^4*x^7 + 2*Y^4*x^6 + 5*Y^4*x^5 + Y^4*x^4 + 2*Y^4*x^3 + 10*Y^4*x^2 + 4*Y^4*x + Y^4 + 7*Y^3*x^12 + 8*Y^3*x^10 + 10*Y^3*x^9 + 6*Y^3*x^8 + 5*Y^3*x^7 +6*Y^3*x^6 + Y^3*x^5 + 10*Y^3*x^4 + 6*Y^3*x^3 + 9*Y^3*x^2 + Y^2*x^16 + 10*Y^2*x^15 + 5*Y^2*x^14 + 10*Y^2*x^13 +10*Y^2*x^12 + 6*Y^2*x^11 + 8*Y^2*x^10 + 10*Y^2*x^8 + 9*Y^2*x^7 + 6*Y^2*x^6 + 10*Y^2*x^5 + 9*Y^2*x^4 + 5*Y*x^20 + 5*Y*x^17 + 8*Y*x^16 + 7*Y*x^15 + 6*Y*x^14 + 7*Y*x^12 + 3*Y*x^10 + 5*Y*x^9 + 3*Y*x^8 + 3*Y*x^7 + 3*Y*x^6 + 4*x^24 + 7*x^23 + 9*x^22 + 4*x^21 + 10*x^20 + 7*x^19 + 9*x^18 + 4*x^17 + x^16 + 4*x^15 + 6*x^14 + x^13 + 3*x^12 + 9*x^11 + 5*x^10 + 8*x^9 + 9*x^8)

#Defining the group

gensXImg = [(7*x^5*y^13 + x^5*y^12 + 7*x^5*y^11 + 5*x^5*y^10 + 9*x^5*y^9 + 7*x^5*y^8 + 4*x^5*y^7 + 3*x^5*y^6 + 7*x^5*y^5 + 5*x^5*y^4 + 9*x^5*y^3 +
7*x^5*y^2 + 8*x^5*y + 2*x^5 + 9*x^4*y^17 + 8*x^4*y^16 + 6*x^4*y^15 + 5*x^4*y^14 + 6*x^4*y^13 + x^4*y^12 + 10*x^4*y^11 + 5*x^4*y^10 + x^4*y^9 + 3*x^4*y^8 + 8*x^4*y^7 + x^4*y^6 + 8*x^4*y^5 + 4*x^4*y^4 + 10*x^4*y^3 + 2*x^4*y^2 + 9*x^4*y + 7*x^4 + x^3*y^21 + 2*x^3*y^20 + 4*x^3*y^19 + 4*x^3*y^18 + 8*x^3*y^17 + 4*x^3*y^15 + 9*x^3*y^14 + 2*x^3*y^13 + 3*x^3*y^12 + 10*x^3*y^11 + 5*x^3*y^10 + 5*x^3*y^9 + 9*x^3*y^8 + 3*x^3*y^7 + x^3*y^6 + 2*x^3*y^5 + 5*x^3*y^4 + 2*x^3*y^3 + 2*x^3*y^2 + 5*x^3*y + 2*x^3 + x^2*y^25 + 3*x^2*y^24 + 5*x^2*y^23 + 3*x^2*y^22 + 7*x^2*y^21 + 2*x^2*y^20 + 9*x^2*y^19 + 3*x^2*y^18 + 2*x^2*y^17 + 2*x^2*y^16 + 5*x^2*y^15 + x^2*y^14 + 8*x^2*y^12 + 9*x^2*y^9 + 7*x^2*y^8 + 8*x^2*y^6 + 6*x^2*y^5 + x^2*y^4 + 8*x^2*y^3 + 8*x^2*y^2 + 10*x*y^29 + 10*x*y^28 + 3*x*y^27 + 2*x*y^26 + 8*x*y^25 + 4*x*y^24 + 8*x*y^23 + 9*x*y^22 + 5*x*y^21 + 8*x*y^20 + 7*x*y^19 + 8*x*y^18 + 5*x*y^17 +2*x*y^16 + 4*x*y^15 + 10*x*y^14 + 10*x*y^13 + 5*x*y^12 + 8*x*y^11 + 7*x*y^10 + 8*x*y^9 + 8*x*y^8 + x*y^7 + 9*x*y^6 + x*y^5 +8*x*y^4 + y^32 + 10*y^30 + 5*y^29 + 9*y^28 + 9*y^27 + 4*y^26 + 3*y^25 + 2*y^24 + 10*y^23 + 3*y^22 + 6*y^21 + 7*y^20 + 3*y^19 + 3*y^18 + 3*y^17 + 10*y^16 + 8*y^15 + 4*y^14 + 2*y^13 + 7*y^11 + 4*y^10 + 8*y^8 + 3*y^7 + 10*y^6)/(y^26 + 2*y^25 + 5*y^23 + 3*y^22 + 3*y^21 + 5*y^20 + 9*y^19 + 8*y^18 + 5*y^17 + 9*y^16 + 9*y^15 + 2*y^14 + 6*y^13 + 6*y^12 + 6*y^11 + 3*y^10 + y^9 + 2*y^8 + 9*y^7 + 5*y^6)]
gensYImg = [y]

gensImg = tuple(zip(gensYImg, gensXImg))
import pdb
pdb.set_trace
AutGrp = kxy.automorphism_group(gensImg, (3,1))

compute_all_fixed_field(kxy, AutGrp)

# S^2 + (x + (10*T^2 + 9*T + 10)/T^2)*S + x^2 + (10*T^2 + 9*T + 10)/T^2*x + (4*T+ 2)/T  
# T^2*x^3 + (10*T^2 + 9*T + 10)*x^2 + (4*T^2 + 2*T)*x + 2*T^2 + 10*T + 7


                                                                         
# Y^6 + 9*Y^5*x^4 + 7*Y^5*x^3 + 7*Y^5*x^2 + 7*Y^5*x + 9*Y^5 + 9*Y^4*x^8 + 8*Y^4*x^7 + 2*Y^4*x^6 + 5*Y^4*x^5 + Y^4*x^4 + 2*Y^4*x^3 + 10*Y^4*x^2 + 4*Y^4*x + Y^4 + 7*Y^3*x^12 + 8*Y^3*x^10 + 10*Y^3*x^9 + 6*Y^3*x^8 + 5*Y^3*x^7 +6*Y^3*x^6 + Y^3*x^5 + 10*Y^3*x^4 + 6*Y^3*x^3 + 9*Y^3*x^2 + Y^2*x^16 + 10*Y^2*x^15 + 5*Y^2*x^14 + 10*Y^2*x^13 +10*Y^2*x^12 + 6*Y^2*x^11 + 8*Y^2*x^10 + 10*Y^2*x^8 + 9*Y^2*x^7 + 6*Y^2*x^6 + 10*Y^2*x^5 + 9*Y^2*x^4 + 5*Y*x^20 + 5*Y*x^17 + 8*Y*x^16 + 7*Y*x^15 + 6*Y*x^14 + 7*Y*x^12 + 3*Y*x^10 + 5*Y*x^9 + 3*Y*x^8 + 3*Y*x^7 + 3*Y*x^6 + 4*x^24 + 7*x^23 + 9*x^22 + 4*x^21 + 10*x^20 + 7*x^19 + 9*x^18 + 4*x^17 + x^16 + 4*x^15 + 6*x^14 + x^13 + 3*x^12 + 9*x^11 + 5*x^10 + 8*x^9 + 9*x^8        

#  G,M := AutomorphismGroup(F);                                                                                                                               
# > IdentifyGroup(G);                                                                                                                                         
# <6, 1>                                                                                                                                                      
# > G;                                                                                                                                                        
# Finitely presented group G on 2 generators                                                                                                                  
# Relations                                                                                                                                                   
#     G.1^2 = Id(G)                                                                                                                                           
#     G.2^2 = Id(G)                                                                                                                                               (G.1 * G.2)^3 = Id(G)     G,M := AutomorphismGroup(F);                                                                                                                               
# > M(G.1)(F.1);                                                                                                                                              (3*x^5*y^11 + 2*x^5*y^10 + 10*x^5*y^9 + 7*x^5*y^7 + 7*x^5*y^6 + 7*x^5*y^5 + 6*x^5*y^4 + 10*x^5*y^3 + 6*x^5*y^2 + 3*x^5*y + 9*x^5 + 7*x^4*y^15 + 5*x^4*y^14 + 10*x^4*y^13 + 7*x^4*y^12 + 9*x^4*y^11 + 10*x^4*y^10 + 2*x^4*y^9 + x^4*y^8 + 7*x^4*y^7 + 3*x^4*y^6 + 9*x^4*y^5 + x^4*y^4 + 10*x^4*y^3 + 5*x^4*y^2 + 2*x^4*y + 4*x^4 + 2*x^3*y^19 + 4*x^3*y^18 + 9*x^3*y^17 + 10*x^3*y^16 + 8*x^3*y^15 + 2*x^3*y^14 + 10*x^3*y^13 + 10*x^3*y^12 + 2*x^3*y^11 + 5*x^3*y^10 + 10*x^3*y^9 + 2*x^3*y^8 + 8*x^3*y^7 + 10*x^3*y^6 + 10*x^3*y^5 + x^3*y^4 + 3*x^3*y^3 + 6*x^3*y + 9*x^3 + 2*x^2*y^23 + 6*x^2*y^22 + 9*x^2*y^20 + 7*x^2*y^19 + 4*x^2*y^18 + 7*x^2*y^17 + 10*x^2*y^16 + 6*x^2*y^15 + 7*x^2*y^14 + 7*x^2*y^13 + 8*x^2*y^12 + 7*x^2*y^11 + 5*x^2*y^10 + 9*x^2*y^6 + 2*x^2*y^5 + 7*x^2*y^4 + 3*x^2*y^3 +3*x^2*y^2 + 9*x*y^27 + 10*x*y^26 + 7*x*y^25 + 4*x*y^24 + 5*x*y^23 + 2*x*y^22 + 7*x*y^21 + 6*x*y^20 + 8*x*y^19 + 5*x*y^18 + x*y^17 + 7*x*y^15 + 7*x*y^14 + 10*x*y^13 + 5*x*y^11 + 5*x*y^10 + 7*x*y^9 + 10*x*y^8 + 2*x*y^7 + 5*x*y^6 + 10*x*y^5 + 3*x*y^4 + 2*y^30 + 9*y^28 + 6*y^27 + 10*y^26 + y^25 + 10*y^24 + 4*y^23 + 2*y^22 + 5*y^21 + 9*y^19 + 7*y^18 + 5*y^17 + 8*y^16 + 10*y^15 + 3*y^14 + 4*y^13 + y^12 + 7*y^11 + 7*y^10 + 3*y^9 + 10*y^8 + 5*y^7 + 6*y^6)/(y^26 + 2*y^25 + 5*y^23 + 3*y^22 + 3*y^21 + 5*y^20 + 9*y^19 + 8*y^18 + 5*y^17 + 9*y^16 + 9*y^15 + 2*y^14 + 6*y^13 + 6*y^12 + 6*y^11 + 3*y^10 + y^9 + 2*y^8 + 9*y^7 +5*y^6)                                                                                                                                                 
# > M(G.2)(F.1);                                                                                                                                              (7*x^5*y^11 + x^5*y^10 + 4*x^5*y^8 + 10*x^5*y^7 + 7*x^5*y^5 + 7*x^5*y^4 + 7*x^5*y^3 + 6*x^5*y^2 + 6*x^5*y + 7*x^5 + 9*x^4*y^15 + 8*x^4*y^14 + 8*x^4*y^13 + 8*x^4*y^12 + 4*x^4*y^11 + 2*x^4*y^10 + 5*x^4*y^9 + 8*x^4*y^8 + 4*x^4*y^7 + 10*x^4*y^6 + 5*x^4*y^5 + 8*x^4*y^4 + 4*x^4*y^3 + 4*x^4*y + 8*x^4 + x^3*y^19 + 2*x^3*y^18 + 3*x^3*y^17 + 2*x^3*y^16 + 2*x^3*y^15 + 3*x^3*y^14 + 9*x^3*y^13 + 10*x^3*y^12 + x^3*y^11 + 3*x^3*y^10 + 9*x^3*y^9 + 3*x^3*y^8 + 4*x^3*y^7 + 10*x^3*y^6 + 10*x^3*y^5 + x^3*y^4 + 2*x^3*y^3 + 5*x^3*y^2 + x^3*y + 7*x^3 + x^2*y^23 + 3*x^2*y^22 + 4*x^2*y^21 + 4*x^2*y^18 + 2*x^2*y^17 + 3*x^2*y^16 + 8*x^2*y^15 + 6*x^2*y^14 + 7*x^2*y^13 + 4*x^2*y^9 + 6*x^2*y^8 + 2*x^2*y^7 + 2*x^2*y^6 + 2*x^2*y^5 + x^2*y^4 + 6*x^2*y^3 + 6*x^2*y^2 + 10*x*y^27 + 10*x*y^26 + 4*x*y^25 + 3*x*y^24 + 7*x*y^23 + 2*x*y^22 + 2*x*y^21 + x*y^20 + 9*x*y^19 + 5*x*y^18 + 3*x*y^17 + 3*x*y^15 + 3*x*y^14 + 4*x*y^13 + 10*x*y^12 + x*y^11 + 8*x*y^10 + 4*x*y^9 + 7*x*y^8 + 4*x*y^6 + 7*x*y^5 + y^30 + 9*y^28 + 5*y^27 + 9*y^26 + 8*y^25 + 3*y^24 + 10*y^23 + 6*y^22 + 5*y^21 + 7*y^20 + 6*y^19 + 3*y^18 + 10*y^16 + 9*y^15 + y^14 + 7*y^13 + 2*y^12 + 5*y^11 + y^10 + 5*y^9 + 9*y^8 + 2*y^7 + 7*y^6)/(y^24 + 2*y^23 + 5*y^21 + 3*y^20 + 3*y^19 + 5*y^18 + 9*y^17 + 8*y^16 + 5*y^15 + 9*y^14 + 9*y^13 + 2*y^12 + 6*y^11 + 6*y^10 + 6*y^9 + 3*y^8 + y^7 + 2*y^6 + 9*y^5 + 5*y^4)

# M(G.1*G.2)(F.1);                                                                                                                                           
# (7*x^5*y^13 + x^5*y^12 + 7*x^5*y^11 + 5*x^5*y^10 + 9*x^5*y^9 + 7*x^5*y^8 + 4*x^5*y^7 + 3*x^5*y^6 + 7*x^5*y^5 + 5*x^5*y^4 + 9*x^5*y^3 +
# 7*x^5*y^2 + 8*x^5*y + 2*x^5 + 9*x^4*y^17 + 8*x^4*y^16 + 6*x^4*y^15 + 5*x^4*y^14 + 6*x^4*y^13 + x^4*y^12 + 10*x^4*y^11 + 5*x^4*y^10 + x^4*y^9 + 3*x^4*y^8 + 8*x^4*y^7 + x^4*y^6 + 8*x^4*y^5 + 4*x^4*y^4 + 10*x^4*y^3 + 2*x^4*y^2 + 9*x^4*y + 7*x^4 + x^3*y^21 + 2*x^3*y^20 + 4*x^3*y^19 + 4*x^3*y^18 + 8*x^3*y^17 + 4*x^3*y^15 + 9*x^3*y^14 + 2*x^3*y^13 + 3*x^3*y^12 + 10*x^3*y^11 + 5*x^3*y^10 + 5*x^3*y^9 + 9*x^3*y^8 + 3*x^3*y^7 + x^3*y^6 + 2*x^3*y^5 + 5*x^3*y^4 + 2*x^3*y^3 + 2*x^3*y^2 + 5*x^3*y + 2*x^3 + x^2*y^25 + 3*x^2*y^24 + 5*x^2*y^23 + 3*x^2*y^22 + 7*x^2*y^21 + 2*x^2*y^20 + 9*x^2*y^19 + 3*x^2*y^18 + 2*x^2*y^17 + 2*x^2*y^16 + 5*x^2*y^15 + x^2*y^14 + 8*x^2*y^12 + 9*x^2*y^9 + 7*x^2*y^8 + 8*x^2*y^6 + 6*x^2*y^5 + x^2*y^4 + 8*x^2*y^3 + 8*x^2*y^2 + 10*x*y^29 + 10*x*y^28 + 3*x*y^27 + 2*x*y^26 + 8*x*y^25 + 4*x*y^24 + 8*x*y^23 + 9*x*y^22 + 5*x*y^21 + 8*x*y^20 + 7*x*y^19 + 8*x*y^18 + 5*x*y^17 +2*x*y^16 + 4*x*y^15 + 10*x*y^14 + 10*x*y^13 + 5*x*y^12 + 8*x*y^11 + 7*x*y^10 + 8*x*y^9 + 8*x*y^8 + x*y^7 + 9*x*y^6 + x*y^5 +8*x*y^4 + y^32 + 10*y^30 + 5*y^29 + 9*y^28 + 9*y^27 + 4*y^26 + 3*y^25 + 2*y^24 + 10*y^23 + 3*y^22 + 6*y^21 + 7*y^20 + 3*y^19 + 3*y^18 + 3*y^17 + 10*y^16 + 8*y^15 + 4*y^14 + 2*y^13 + 7*y^11 + 4*y^10 + 8*y^8 + 3*y^7 + 10*y^6)/(y^26 + 2*y^25 + 5*y^23 + 3*y^22 + 3*y^21 + 5*y^20 + 9*y^19 + 8*y^18 + 5*y^17 + 9*y^16 + 9*y^15 + 2*y^14 + 6*y^13 + 6*y^12 + 6*y^11 + 3*y^10 + y^9 + 2*y^8 + 9*y^7 + 5*y^6)
