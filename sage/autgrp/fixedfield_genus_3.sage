#This file is to generate the group of candidates of genus 3 curves and their fixed field.
#/lu101/sahosse/doc/RecherchePhD/Codes/sage/AutGrps/

set_verbose(0)

#from fixed_field_comp import fixed_field
from sys import exit, stdout

#@profile
def compute_all_fixed_field(coverField, coverAutGrp):

    print "from", len(AutGrp.conjugacy_classes_representatives()), "conjugacy classes, we need to check", sum([g.order() == 2 for g in AutGrp.conjugacy_classes_representatives()])
    print
    stdout.flush()
    y = coverField.gens()[0]
    x = coverField.rational_function_field().gens()[0]
    for f in coverAutGrp.conjugacy_classes_representatives():
        if (f.order() == 2):
            print "Class rep f with"
            stdout.flush()
            print 'f(x) = ', f(x)
            stdout.flush()
            print 'f(y) = ', f(y)
            stdout.flush()
            (curfield, subEmbed) = fixed_field(f, ['t','z'])
            stdout.flush()
            print '*****************************RESULT*****************************'
            print 'The fixed field of f is'
            print curfield
            #print 'of genus ', curfield.genus()
            print
            stdout.flush()
            #import pdb; pdb.set_trace()
            zImg = subEmbed(curfield.gens()[0])
            tImg = subEmbed(curfield.rational_function_field().gen())
            assert(zImg == f(zImg))
            assert(tImg == f(tImg))


#(D_12)
#(12,4)
nicePrime = 13

import pdb
pdb.set_trace()

k = FiniteField(nicePrime)
kA.<a> = PolynomialRing(k)
ka = kA.fraction_field()
kx.<x> = FunctionField(ka)
kxY.<Y> = kx[];

proot = k(primitive_root(nicePrime))
zeta_3 = proot^((k.order()-1)/3)
zeta_6 = proot^((k.order()-1)/6)

kxy.<y> = kx.extension(Y^2 -x*(x^6 + a*x^3 + 1))

#Defining the group

#x->zeta_3*x
#y->zeta_6*y
#x->1/x
#y->y/x^4

gensXImg = [1/x, zeta_3*x]#, 1/x]  #,  x]
gensYImg = [y/x^4, zeta_6*y]#, y/x^4]#, -y]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [12,4])
#import pdb; pdb.set_trace();
compute_all_fixed_field(kxy, AutGrp)

exit()

#x->zeta_3*x
#y->zeta_3^2*y
#x->1/x
#y->y/x^4

#(S_4)
#(24,12)
nicePrime = 13 #we only needs second root of unity

k = FiniteField(nicePrime)
kA.<a> = PolynomialRing(k)
ka = kA.fraction_field()
#import pdb; pdb.set_trace();
kx.<x> = FunctionField(ka)
kxY.<Y> = kx[];

kxy.<y> = kx.extension(Y^4 + x^4 + 1 + a*x^2*Y^2 + a*Y^2 + a*x^2)

#Defining the group

#x-> -x
#y-> -y
#x-> y, y-> x
#x-> 1/x, y->y/x

#searching for zeta_8
f = kxy.hom([-x, y])

gensXImg = [-x, x, 1/x, y]
gensYImg = [y, -y, y/x, x]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [24,12])
# #import pdb; pdb.set_trace();
compute_all_fixed_field(kxy, AutGrp)

exit(1)


#(168, 42)
nicePrime = 29 #i need 7th root of unity and -7 be quadratic res
k = FiniteField(nicePrime)
kx.<x> = FunctionField(k)
kxY.<Y> = kx[];
kxy.<y> = kx.extension(x^3*Y + Y^3 + x)

a = k(primitive_root(nicePrime))

zeta_7 = a^((k.order()-1)/7)
sqtn7 = k(-7).square_root()

gensXImg = [zeta_7^3*x, y/x, ((zeta_7-zeta_7^6)*x+(zeta_7^2-zeta_7^5)*y+zeta_7^4-zeta_7^3)/((zeta_7^4-zeta_7^3)*x+(zeta_7-zeta_7^6)*y+ zeta_7^2-zeta_7^5)]
gensYImg = [zeta_7*y, 1/x, ((zeta_7^2 - zeta_7^5)*x+(zeta_7^4-zeta_7^3)*y + zeta_7-zeta_7^6)/((zeta_7^4-zeta_7^3)*x+(zeta_7-zeta_7^6)*y + zeta_7^2-zeta_7^5)]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [168,42])
compute_all_fixed_field(kxy, AutGrp)

#(96,64)
nicePrime = 73
extenPower = 1 #this will give me 8'th root of unity

k = FiniteField(nicePrime^extenPower)
kx.<x> = FunctionField(k)
kxY.<Y> = kx[];

a = k(primitive_root(nicePrime))
zeta_6 = a^((k.order()-1)/6)
zeta_4 = a^((k.order()-1)/4)
zeta_8 = a^((k.order()-1)/8)

kxy.<y> = kx.extension(Y^3 - x^3*Y + x)

#Defining the group

#x-> ix
#y-> iy
#x-> (zeta_8)y, y->(zeta_8)x
#x-> 1/x, y->y/x

#searching for zeta_8

gensXImg = [zeta_4*x, x, 1/x, zeta_8*y]
gensYImg = [y, zeta_4*y, y/x, zeta_8*x]

gensImg = zip(gensYImg, gensXImg)

#import pdb; pdb.set_trace();
AutGrp = kxy.automorphism_group(gensImg, [96,64])

compute_all_fixed_field(kxy, AutGrp)

exit(1)

#import pdb; pdb.set_trace()
#(48,33)
kxy.<y> = kx.extension(Y^4 - x^3 + 1)

#Defining the group
G = gap.SmallGroup(48,33)
gs = G.GeneratorsOfGroup()
G2GFp = G.IsomorphismFpGroupByGenerators([gs[1],gs[2],gs[3]])
GFp = G2GFp.Image(G)
GFp2Perm = GFp.IsomorphismPermGroup()

#We need that 9 has root 4. This is to say that we need 3 to have root 2. which 
#can be checked using Quadratic Resiprocity, this rule says:
#p = +- 1 mod 12.
#further more we need fourth root of unity so 
# p = +1 mod 4 => p - 1 = q 4 = 12 q' + t 4 where t = 0, 1, 2, 3
# p = + 1 + tq (12) = 1, 5, 9, 13 but not 11 = -1 (12) so that
# case is impossible so we need to choose a prime that
# p = 1 (12)
# otherwise you lose some of the fourth root of 9
# and that will take some of the automorphisms
gensXImg = [(x+2)/(x-1), zeta_6^2*x, x]
gensYImg = [zeta_4*a^(k(9).generalised_log()[0]/4)/(x -1)*y, y, zeta_4*y]

gensImg = zip(gensYImg, gensXImg)
gensComplex = zip([GFp2Perm.Image(curGen) for curGen in GFp.GeneratorsOfGroup()], gensImg)

AutGrp = kxy.automorphism_group(gensImg, [48,33])

compute_all_fixed_field(kxy, AutGrp)
#exit(0)
#(48,48) y^2=x^8 + 14*x^4 + 1
kxy.<y> = kx.extension(Y^2 - x^8 -14*x^4 - 1)

#Defininig gap group based on the generators
ZoG = gap.CyclicGroup(2)
Gb = gap.Group(Permutation((1,2,3,4)), Permutation((1,2,3)))
G = gap.DirectProduct([ZoG, Gb])
G2Perm = G.IsomorphismPermGroup();

gensXImg = [x , zeta_4*x, zeta_4*(x+1)/(x-1)]
gensYImg = [-y, y       , -4*y/(x-1)^4       ]
gensImg = zip(gensYImg, gensXImg)

gensComplex = zip([G2Perm.Image(curGen) for curGen in G.GeneratorsOfGroup()], gensImg)

AutGrp = kxy.automorphism_group(gensImg, [48,48])
assert(str(gap.IsomorphismGroups(AutGrp._gap_(), gap.SmallGroup(48,48))) != 'fail')
compute_all_fixed_field(kxy, AutGrp)

#exit(0)
#(24, 5) y^2=x(x^6-1)
kxy.<y> = kx.extension(Y^2 - x*(x^6-1))

G = gap.SmallGroup(24, 5)
G2Perm = G.IsomorphismPermGroup();

#Defininig generators based on gap gens
zeta_6 = a^((k.order()-1)/6)
zeta_4 = a^((k.order()-1)/4)
gensXImg = [-1/x, zeta_6^3*x, x, zeta_6^2*x]
gensYImg = [y/x^4, zeta_4 * y, -y, zeta_6*y]
gensImg = zip(gensYImg, gensXImg)

gensComplex = zip([G2Perm.Image(curGen) for curGen in G.GeneratorsOfGroup()], gensImg)

AutGrp = kxy.automorphism_group(gensImg, [24,5])
assert(str(gap.IsomorphismGroups(AutGrp._gap_(), gap.SmallGroup(24,5))) != 'fail')
compute_all_fixed_field(kxy, AutGrp)

        

