"""
Improvement to the FractionField class

AUTHORS:

- Syed Lavasani
"""
import FractionField from sage.rings.fraction_field

class FractionField_improved(fraction_field):
    """
    The imporved version of the fraction field:

    - Iteration support
    """

            def __iter__(self):
            """
            Create an infinite sequence of members of this field.
            
            EXAMPLES::

            sage: import itertools
            sage: K.<x> = FunctionField(QQ)
            sage: [a for a in itertools.islice(K,17)]
            [0, 1, -1, 1/2, -1/2, 2, -2, 1/3, -1/3, 3, -3, 2/3, -2/3, 3/2, -3/2, 1/4, -1/4]
            sage: K.<x> = FunctionField(GF(2))
            sage: [a for a in itertools.islice(K,17)]
            [0, 1, x, x/(x + 1), (x + 1)/x, x + 1, x^2, x^2/(x^2 + 1), x^2/(x + 1), x^2/(x^2 + x + 1), x^2 + x, (x^2 + x)/(x^2 + x + 1), (x^2 + 1)/x^2, (x^2 + 1)/x, x^2 + 1, (x^2 + 1)/(x^2 + x + 1), (x^2 + x + 1)/x^2]
        """
        import pdb; pdb.set_trace()
        k = self.base_ring()
        if not k.is_finite():
            for a in k:
                yield self(a)
        else:
            degree = -1
            yield self(0)

            while True:
                degree +=1

                from sage.rings.monomials import monomials
                curMonomials = monomials(self.gens(), [degree]* self.ngens())

                coefficients = [iter(self.base_ring()) for d in range(2*len(curMonomials))]+[[0,1]]
                from itertools import product
                for coefficient in product(*coefficients):
                    n = sum([c*curMonomials[e] for (e,c) in enumerate(coefficient[:degree+1])])
                    d = sum([c*curMonomials[e] for (e,c) in enumerate(coefficient[degree+1:])])
                    if d==0:
                        continue
                    if n.element().numerator().degree()!=degree and d.element().numerator().degree()!=degree:
                        continue
                    ret = n/d
                    if ret.element().numerator().degree()!=degree:
                        continue
                    yield ret

    
