#Shows all nonsolvable groups up to some order
maxOrd = 200;
for grpOrd in range(100, maxOrd):

    if not is_power_of_two(grpOrd):
        noIsoClass = gap.NumberSmallGroups(grpOrd)
        print "Processing groups with order", grpOrd, "with", noIsoClass, "groups..."
        #    for G bin gap.AllSmallGroups(12):

        for curGrpIdx in range(1,noIsoClass):
            curGrp = gap.SmallGroup(grpOrd, curGrpIdx)
            if (not curGrp.IsSolvable()): 
               print "(",grpOrd,",",curGrpIdx,")"
               print curGrp
    else:
     print "Skipping", grpOrd



