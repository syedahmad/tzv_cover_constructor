#We compute the genus of k(H) = k(C)^\sigma where \sigma is 
#the hypothetical automorphisms. We only check that genus
#using the ramification structure:

#Unramified
#Genus3
#2(2-1) = |G|/2*2(0-1) + Sum |G|/2c_i(c_i -1)
#2 = -|G| +  Sum |G|/(2c_i)(c_i -1)
f = open('smallgen/data/genus03/groups03')
coverDeg = 2

for line in f:
    if (line[0:4] == 'GROU'):
        sig = line[line.index('[')+1:line.index(']')].split(',');
        sig = [int(c) for c in sig]
        
        afixgen = sig[0]
        sig = sig[1:]

        line = line[line.index(']')+1:]
        grp =  line[line.index('[')+1:line.index(']')].split(',');
        grp = [int(c) for c in grp]

        if (not (grp[0] % (3 * coverDeg))) and (not(sum([(grp[0]/ram_idx) % coverDeg for ram_idx in sig]))):
            fixedfield_genus = (2/coverDeg*grp[0]*(afixgen-1) + sum([(grp[0]/(coverDeg*ram_idx))*(ram_idx - 1) for ram_idx in sig ]))/2 + 1
            if (fixedfield_genus == 2):
                print sig, grp, fixedfield_genus


#Genus4
#Ramified only in one place.
#2(2-1) = |G|/2*2(0-1) + Sum |G|/(2c_i)(c_i -1) + (|G|/(2c_i)-1)*(c_j-1) + 2(c_j/2 - 1)
#2 = -|G| +  Sum |G|/(2c_i)(c_i -1)
print "Genus04, Cover deg 2, ram over one place"
f = open('smallgen/data/genus04/groups04')
coverDeg = 2

for line in f:
    if (line[0:4] == 'GROU'):
        sig = line[line.index('[')+1:line.index(']')].split(',');
        sig = [int(c) for c in sig]
        
        afixgen = sig[0]
        sig = sig[1:]

        line = line[line.index(']')+1:]
        grp =  line[line.index('[')+1:line.index(']')].split(',');
        grp = [int(c) for c in grp]

        i = 0
#       for i in range(0, len(sig)-len(sig)):
#        if (not (sig[i] % coverDeg)):


        if (not (grp[0] % (3 * coverDeg))) and (not(sum([(grp[0]/ram_idx) % coverDeg for ram_idx in sig]))):
            ramLoc = sig[i]
            unramsig = sig[:i]
            unramsig.extend(sig[i+1:])

            fixedfield_genus = ((2/coverDeg)*grp[0]*(afixgen-1) + sum([(grp[0]/(coverDeg*ram_idx))*(ram_idx - 1) for ram_idx in unramsig ])+ ( (((grp[0]/ramLoc)-2)/coverDeg)*(ramLoc-1)) + 2*(ramLoc/2 - 1))/2 + 1
            if (fixedfield_genus == 2):
                print tuple(sig), tuple(grp);


#Genus4
#Ramified only in two places.
#2(2-1) = |G|/2*2(0-1) + Sum |G|/(2c_i)(c_i -1) + ((|G|/cj1-1)/2))*(c_j-1) + (c_j1/2 - 1) and the same for cj2
#2 = -|G| +  Sum |G|/(2c_i)(c_i -1)
print "Genus04, Cover deg 2, ram over two places"
f = open('/smallgen/data/genus04/groups04')
coverDeg = 2

for line in f:
    if (line[0:4] == 'GROU'):
        sig = line[line.index('[')+1:line.index(']')].split(',');
        sig = [int(c) for c in sig]
        
        afixgen = sig[0]
        sig = sig[1:]

        line = line[line.index(']')+1:]
        grp =  line[line.index('[')+1:line.index(']')].split(',');
        grp = [int(c) for c in grp]

        acceptedfield = False
        for i in range(0, len(sig)):
            if acceptedfield: break
            if (not (sig[i] % coverDeg)):
                for j in range(i+1, len(sig)):
                    if not (sig[j] % coverDeg):
                        ramLoc = [sig[i], sig[j]]
                        unramsig = sig[:i]
                        unramsig.extend(sig[i+1:j])
                        unramsig.extend(sig[j+1:])
                
                        if (not (grp[0] % (3 * coverDeg))) and (not(sum([(grp[0]/ram_idx) % coverDeg for ram_idx in unramsig]))) and \
                            (not(sum([((grp[0]/ram_idx) - 1) % coverDeg for ram_idx in ramLoc]))):
                            fixedfield_genus = ((2/coverDeg)*grp[0]*(afixgen-1) + sum([(grp[0]/(coverDeg*ram_idx))*(ram_idx - 1) for ram_idx in unramsig ])+ sum([ (((grp[0]/curramLoc)-1)/coverDeg)*(curramLoc-1) + (curramLoc/2 - 1) for curramLoc in ramLoc]))/2 + 1 
                            if (fixedfield_genus == 2):
                                print tuple(sig), tuple(grp)#, ramLoc, fixedfield_genus;
                                acceptedfield = True
                                break

