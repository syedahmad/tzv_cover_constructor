#Here, we retrieving the pc structure of the automorphism groups of Genus 4 curves
Gtypes = [(9,2), (36,12), (72, 40), (72,42), (36,10),(18,3), (36,10)]

testSubgrp = direct_product_permgroups([CyclicPermutationGroup(3),CyclicPermutationGroup(3)])

for curGTyp in Gtypes:
    G = gap.SmallGroup(curGTyp)
    G2Perm = G.IsomorphismPermGroup()
    GPerm = G2Perm.Image(G)
    GPermSage = PermutationGroup(GPerm.GeneratorsOfGroup())

    tstInG = [testSubgrp.is_isomorphic(H) for H in GPermSage.subgroups()]
    if (True in tstInG):
        print curGTyp;
         

#     if (GPermSage.is_abelian()): 
#     print "Abelian of type:", G.AbelianInvariants()
# else:
#     ZofG = GPermSage.center(); ZofG
#     ZofG.is_cyclic()

#     #This group is direct product of S3 by the Z4
#     print GPermSage.is_isomorphic(direct_product_permgroups([SymmetricGroup(3), CyclicPermutationGroup(4)]))

#     #Hence a curve like this should have the same automorphism group
#     #Not really, because Infinity is ramified here to so $x -> 1/x'
#     #somehow works. but we gets another morphism that I couldn't
#     #Figure out.
#     # magma.eval('k := FiniteField(25);')
#     # magma.eval('kx<x> := RationalFunctionField(k);')
#     # magma.eval('kxY<Y> := PolynomialRing(kx);')
#     # magma.eval('kxy<y> := FunctionField(Y^4 - (x-1)*(x-2)*(x-3));')
#     # magma.eval('G, Phi := AutomorphismGroup(kxy)')
#     # magma.eval('print Order(G);')
   
#     cenGen = ZofG.gen()
#     invC = cenGen^2

#     GalkCokx = GPermSage.subgroup([GPermSage(invC)])
#     Gprime = GPermSage.quotient(GalkCokx);
#     print Gprime.is_isomorphic(DihedralGroup(Integer(6)))

#     #I would like to find out which coset of G/<Inv>(G) correspond to gen of Z
#     invSub = GPermSage.subgroup([invC]);
#     CosOfZ = GPermSage.cosets(invSub);
#     print [i for i in range(len(CosOfZ)) if cenGen in CosOfZ[i]];

#     #I have no way but using Gap directlyn
#     gZofG = G.Center()
#     gZGens = gZofG.GeneratorsOfGroup()
#     gInvGrp = G.Subgroup([gZGens[1]^2])

#     gHomG2Gp = G.NaturalHomomorphismByNormalSubgroup(gInvGrp);
#     gD6 = gHomG2Gp.Image()
    
#     gImgZGen = gHomG2Gp.Image(gZGens[1])

#     gHomCycSub = gD6.IsomorphicSubgroups(gap.CyclicGroup(6))
#     gD6CycSub = gHomCycSub[1].Image()
#     print gImgZGen in gD6CycSub


#     gQoImgZ = gD6 / gHomG2Gp.Image(gZofG);

#     #print GPermSage.cayley_table()

#     # RepsG = G.IrreducibleRepresentations()
#     # T = RepsG[len(RepsG)-1].Image(G)
#     # print T.Elements()
#     # print GPermSage.is_isomorphic(DihedralGroup(12))

# k.<a> = FiniteField(25); #All we need is 4th root of one and 3rd root of unity

# zeta_6 = 1
# for elm in k:
#     if (not ((elm == 1) or (elm^2 == 1) or (elm^3 == 1))) and (elm^6 ==1):
#         zeta_6 = elm
#         break;

# kx.<x> = FunctionField(k);
# fx = x*(x^6 -1)

# fgens = [kx.hom(1/x, kx), kx.hom(zeta_6^3*x, kx), kx.hom(zeta_6^2*x, kx)]

# for curf in fgens:
#     print sqrt(curf(fx)/fx)


# #Finding all nonsolvable groups

# # #    if not is_power_of_two(grpOrd):
# #         noIsoClass = gap.NumberSmallGroups(grpOrd)
# #         print "Processing groups with order", grpOrd, "with", noIsoClass, "groups..."
# #         #    for G bin gap.AllSmallGroups(12):

# #         for curGrpIdx in range(1,noIsoClass):
# #             curGrp = gap.SmallGroup(grpOrd, curGrpIdx)
# #             if (not curGrp.IsSolvable()): 
# #                print "(",grpOrd,",",curGrpIdx,")"
# #                print curGrp
# #     else:
# #      print "Skipping", grpOrd



