#This file is to generate the group of candidates of genus 3 curves and their fixed field.
#/lu101/sahosse/doc/RecherchePhD/Codes/sage/AutGrps/
#this is for the finite field as constant field

from os import chdir

#chdir('/lu101/sahosse/doc/phd/sage/autgrp/')
set_verbose(0)

#from fixed_field_comp import fixed_field
from sys import exit, stdout


#@profile
def compute_all_fixed_field(coverField, coverAutGrp):

    print "Computing fixed field of ", coverField, " with automorphism groupid ", coverAutGrp.group_id()
    print "from", len(AutGrp.conjugacy_classes_representatives()), "conjugacy classes, we need to check", sum([g.order() == 2 for g in AutGrp.conjugacy_classes_representatives()])
    print
    stdout.flush()
    y = coverField.gens()[0]
    x = coverField.rational_function_field().gens()[0]
    for f in coverAutGrp.conjugacy_classes_representatives():
        if (f.order() == 2):
            print "Class rep f with"
            stdout.flush()
            print 'f(x) = ', f(x)
            stdout.flush()
            print 'f(y) = ', f(y)
            stdout.flush()
            (curfield, subEmbed) = f.fixed_field(['t','z'])
            stdout.flush()
            print '*****************************RESULT*****************************'
            print 'The fixed field of f is'
            print curfield
            try:
                print 'of genus ', curfield.genus()
            except:
                print 'unable to compute genus'
            print
            print 'using embedding'
            print subEmbed
            stdout.flush()
            zImg = subEmbed(curfield.gens()[0])
            tImg = subEmbed(curfield.rational_function_field().gen())
            assert(zImg == f(zImg))
            assert(tImg == f(tImg))


nicePrime = 13
extenPower = 1 #this will give me 8'th root of unity

k = FiniteField(nicePrime^extenPower)

a = k(primitive_root(nicePrime))
zeta_3 = a^((k.order()-1)/3)
zeta_6 = a^((k.order()-1)/6)
zeta_4 = a^((k.order()-1)/4)
#zeta_8 = a^((k.order()-1)/8)

#zeta_7 = a^((k.order()-1)/7)
#sqtn7 = k(-7).square_root()


#(12,4)
#(D_12)
#nicePrime = 13

#k = FiniteField(nicePrime)
kB.<b, c, d> = PolynomialRing(k)
kb = kB.fraction_field()
b = kb(b)
c = kb(c)
d = kb(d)
kx.<x> = FunctionField(kb)
kxY.<Y> = kx[];

#zeta_3 = proot^((k.order()-1)/3)
#zeta_6 = proot^((k.order()-1)/6)

kxy.<y> = kx.extension(Y^2 -x*(x^6 + b*x^3 + 1))

#Defining the group

#x->zeta_3*x
#y->zeta_6*y
#x->1/x
#y->y/x^4

gensXImg = [1/x,zeta_3*x]#, 1/x]  #,  x]
gensYImg = [y/x^4, zeta_6*y]#, y/x^4]#, -y]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [12,4])

compute_all_fixed_field(kxy, AutGrp)

#Special prime is needed     

