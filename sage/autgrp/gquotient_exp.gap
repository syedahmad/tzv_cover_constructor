F := FreeGroup(2);
G := F / [F.1^2, F.2^6];
D12 := SmallGroup(12,4);
D12Fp := Image(IsomorphismFpGroup(D12),D12);
Epms := GQuotients(G,D12Fp);
gs := [Image(Epms[1],(G.1)),Image(Epms[1],(G.2))];
GinD := Subgroup(D12Fp, gs);
GFp := Image(IsomorphismFpGroupByGenerators(GinD, gs), GinD);
Rel2Check := RelatorsOfFpGroup(GFp);


