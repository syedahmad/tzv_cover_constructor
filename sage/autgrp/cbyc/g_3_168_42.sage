from comp_all_conj_classes import compute_all_fixed_field

set_verbose(0)

R.<x> = QQ[]

k.<a> = QQ.extension(R.cyclotomic_polynomial(7))

ord_a = 7

zeta_7 = a
sqrt7 = sqrt(k(-7))

kx.<x> = FunctionField(k)
kxY.<Y> = kx[];

kxy.<y> = kx.extension(x^3*Y + Y^3 + x)

gensXImg = [zeta_7^3*x, y/x, ((zeta_7-zeta_7^6)*x+(zeta_7^2-zeta_7^5)*y+zeta_7^4-zeta_7^3)/((zeta_7^4-zeta_7^3)*x+(zeta_7-zeta_7^6)*y+ zeta_7^2-zeta_7^5)]
gensYImg = [zeta_7*y, 1/x, ((zeta_7^2 - zeta_7^5)*x+(zeta_7^4-zeta_7^3)*y + zeta_7-zeta_7^6)/((zeta_7^4-zeta_7^3)*x+(zeta_7-zeta_7^6)*y + zeta_7^2-zeta_7^5)]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [168,42])
compute_all_fixed_field(kxy, AutGrp)
