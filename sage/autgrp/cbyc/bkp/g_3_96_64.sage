from comp_all_conj_classes import compute_all_fixed_field

set_verbose(1)

R.<x> = QQ[]

k.<a> = QQ.extension(R.cyclotomic_polynomial(8))

ord_a = 8

zeta_4 = a^(ord_a/4)
zeta_8 = a

kx.<x> = FunctionField(k)
kxY.<Y> = kx[];


kxy.<y> = kx.extension(Y^4 - x^4 -1)#Y^3 - x^3*Y + x)

#Defining the group

#x-> ix
#y-> iy
#x-> (zeta_8)y, y->(zeta_8)x
#x-> 1/x, y->y/x

gensXImg = [zeta_4*x, x, 1/x, zeta_8*y]
gensYImg = [y, zeta_4*y, y/x, zeta_8*x]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [96,64])

compute_all_fixed_field(kxy, AutGrp)
