from comp_all_conj_classes import compute_all_fixed_field

set_verbose(1)

R.<x> = QQ[]

k.<a> = QQ.extension(R.cyclotomic_polynomial(6))

ord_a = 6

zeta_3 = a^(ord_a/3)
zeta_6 = a

#(12,4)
#(D_12)

kB.<b> = PolynomialRing(k)
kb = kB.fraction_field()
kx.<x> = FunctionField(kb)
kxY.<Y> = kx[];

kxy.<y> = kx.extension(Y^2 -x*(x^6 + b*x^3 + 1))

#Defining the group

#x->zeta_3*x
#y->zeta_6*y
#x->1/x
#y->y/x^4

gensXImg = [1/x,zeta_3*x]#, 1/x]  #,  x]
gensYImg = [y/x^4, zeta_6*y]#, y/x^4]#, -y]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [12,4])

compute_all_fixed_field(kxy, AutGrp)
