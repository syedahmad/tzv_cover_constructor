from comp_all_conj_classes import compute_all_fixed_field

set_verbose(1)

R.<x> = QQ[]

k.<a> = QQ.extension(R.cyclotomic_polynomial(4))

ord_a = 4

zeta_4 = a

kx.<x> = FunctionField(k)
kxY.<Y> = kx[];

#(48,48) y^2=x^8 + 14*x^4 + 1
kxy.<y> = kx.extension(Y^2 - x^8 -14*x^4 - 1)

gensXImg = [x , zeta_4*x, zeta_4*(x+1)/(x-1)]
gensYImg = [-y, y       , -4*y/(x-1)^4       ]
gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [48,48])

compute_all_fixed_field(kxy, AutGrp)
