#!/bin/bash

while [ "$1" != "" ]; do
    sage $1>$1.out 2>$1.err &
    shift
done
