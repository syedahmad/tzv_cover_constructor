from comp_all_conj_classes import compute_all_fixed_field

set_verbose(0)

R.<x> = QQ[]

k = QQ
kB.<b> = PolynomialRing(k)
kb = kB.fraction_field()

kx.<x> = FunctionField(kb)
kxY.<Y> = kx[];

kxy.<y> = kx.extension(Y^4 + x^4 + 1 + b*x^2*Y^2 + b*Y^2 + b*x^2)

#Defining the group

#x-> -x
#y-> -y
#x-> y, y-> x
#x-> 1/x, y->y/x

gensXImg = [-x, x, 1/x, y]
gensYImg = [y, -y, y/x, x]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [24,12])
 
compute_all_fixed_field(kxy, AutGrp)

