from comp_all_conj_classes import compute_all_fixed_field

set_verbose(0)

R.<x> = QQ[]

k.<a> = QQ.extension(R.cyclotomic_polynomial(12))

ord_a = 12

zeta_3 = a^(ord_a/3)
zeta_6 = a^(ord_a/6)
zeta_4 = a^(ord_a/4)

kx.<x> = FunctionField(k)
kxY.<Y> = kx[];

kxy.<y> = kx.extension(Y^2 - x*(x^6-1))

#Defininig generators based on gap gens
gensXImg = [-1/x, zeta_6^3*x, x, zeta_6^2*x]
gensYImg = [y/x^4, zeta_4 * y, -y, zeta_6*y]
gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [24,5])

compute_all_fixed_field(kxy, AutGrp)
