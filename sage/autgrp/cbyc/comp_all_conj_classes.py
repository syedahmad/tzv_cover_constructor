from sys import stdout

def compute_all_fixed_field(coverField, coverAutGrp):

    print "Computing fixed field of ", coverField, " with automorphism groupid ", coverAutGrp.group_id()
    print "from", len(coverAutGrp.conjugacy_classes_representatives()), "conjugacy classes, we need to check", sum([g.order() == 2 for g in coverAutGrp.conjugacy_classes_representatives()])
    print
    stdout.flush()
    y = coverField.gens()[0]
    x = coverField.rational_function_field().gens()[0]
    for f in coverAutGrp.conjugacy_classes_representatives():
        if (f.order() == 2):
            print "Class rep f with"
            stdout.flush()
            print 'f(x) = ', f(x)
            stdout.flush()
            print 'f(y) = ', f(y)
            stdout.flush()
            (curfield, subEmbed) = f.fixed_field(['t','z'])
            stdout.flush()
            print '*****************************RESULT*****************************'
            print 'The fixed field of f is'
            print curfield
            try:
                print 'of genus ', curfield.genus()
            except:
                print 'unable to compute genus'
            print
            print 'using embedding'
            print subEmbed
            stdout.flush()
            zImg = subEmbed(curfield.gens()[0])
            tImg = subEmbed(curfield.rational_function_field().gen())
            assert(zImg == f(zImg))
            assert(tImg == f(tImg))

