#!/usr/bin/env python
import os
from scriptine import path

def __toggle_verbose_dir_command(curve_files_dir):
    """ 
    Runs the --toggle-verbose command on all *.sage files in curve-files-dir

    :param curve_file_dir: the directory that contains the curve files 
    the curve
    """
    for curve_file in path(curve_files_dir).files('*.sage'):
        __toggle_verbose_command(curve_file)

def __toggle_verbose_command(curve_file):
    """ 
    Reads from curve file and find the verbose statement. Change it to 1 if it is 0, or to 0 if it is 1

    :param curve_file: the name of the file that compute the fixed filed of 
    the curve
    """
    curve_file_handle = open(curve_file)
    newcrv_handle = open(curve_file+".toggled","w")
    for cur_line in curve_file_handle:
        verbose_index = cur_line.find("set_verbose")
        if (verbose_index != -1):
            verbose_value = cur_line[verbose_index+len("set_verbose(")];
            verbose_value = (verbose_value == '0') and "1" or "0"
            cur_line = cur_line[:verbose_index + len("set_verbose(")] + verbose_value + cur_line[verbose_index + len("set_verbose(")+1:]

        newcrv_handle.write(cur_line)

    os.renames(curve_file+".toggled", curve_file)



if __name__ == '__main__':
    import scriptine
    scriptine.run()

            
