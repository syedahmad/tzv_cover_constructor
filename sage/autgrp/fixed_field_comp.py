#from sage.rings.polynomial import PolynomialRing

from sage.all_cmdline import *   # import sage library

def fixed_field(self, names=['x','y']):
    """
    Return the fixed_field of self and the maps from the super field to the 
    fixed field and its inverse
    """

    #TODO check that superField is a simple extension.
    #import pdb; pdb.set_trace();
    superField = self.parent().function_field()
    x_sig = _fixed_element(self)

    print "Changing to invariant rational function field..."
    (superInvRational, to_Inv, from_Inv)  = superField.change_generators(x_sig, names=names)
    #import pdb; pdb.set_trace();
    selfOnInv = to_Inv*self.as_hom()*from_Inv

#    R = PolynomialRing(superField, 'T')
#    T = R.gen()

#    print 'Super'
#    curConj = superField.gens()[0]
#    pOfSyms = 1
#    for i in range(0, self.order()):
#        pOfSyms *= T - curConj
#        curConj = self(curConj)
#        print curConj


#    print pOfSyms.coeffs()
#    print [self(curCof) for curCof in pOfSyms.coeffs()]

    from sys import stdout
    print "Computing invarient polynomial..."
    stdout.flush()
    y = superInvRational.gens()[0]
    R = PolynomialRing(superInvRational, 'T')
    T = R.gen()

#    print 'SuperInv'
    curConj = y
    pOfSyms = 1
    for i in range(0, self.order()):
        pOfSyms *= T - curConj
        if (i < self.order() - 1): curConj = selfOnInv(curConj)


#    print pOfSyms.coeffs()
#    print [selfOnInv(curCof) for curCof in pOfSyms.coeffs()]

    print "Extending to the fixed field..."
    (SupV, from_V, to_V) = superInvRational.relative_vector_space(superInvRational.rational_function_field())

    expectedDeg = superInvRational.degree()/self.order()
    subFixedField = superInvRational.rational_function_field()
    subFFDeg = 1
    #a map from the the subFixedField to super field.
    subEmbed = subFixedField.hom(superInvRational(subFixedField.gens()[0]))
        
    coeffIdx = len(pOfSyms.coeffs())

    while(subFixedField.degree() < expectedDeg):
        coeffIdx -= 1
        curCoeff = pOfSyms.coeffs()[coeffIdx]
        if (not curCoeff.is_constant()):
            (subFixedField, subEmbed) = subFixedField._extend_subfield(subEmbed, curCoeff, names[1])

    #make subEmbed to embed in the original field
    subEmbed = from_Inv * subEmbed

    return (subFixedField, subEmbed)

def _fixed_element(self):
    """
    Returns a non constant element of the function field that is fixed by
    self.
    """
    superRationalField = self.parent().function_field().rational_function_field()

    x = superRationalField.gens()[0]
    R = PolynomialRing(superRationalField,'T')
    T = R.gen()

    curConj = x
    pOfSyms = 1

    for i in range(0, self.order()):
        pOfSyms *= T - curConj
        if (i < self.order() - 1): 
            curConj = self(curConj)

#    import pdb; pdb.set_trace()
    invCoeffs = pOfSyms.coeffs();
    invCoeffs.reverse()
    for invCoeff in invCoeffs:
        if not invCoeff.is_constant():
            return invCoeff


