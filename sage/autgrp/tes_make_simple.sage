
kz.<z> = FunctionField(FiniteField(13))
Rkz.<x> = kz[]
kx = kz.extension(z^4 + 4*x*z^3 + 6*x^2*z^2 + 4*x^3*z + (x^8 + 2*x^4))
Rkx.<y> = kx[]
kxy = kx.extension(y^4 - x^4 - 1)
kzt = kxy.make_simple(name='t')

