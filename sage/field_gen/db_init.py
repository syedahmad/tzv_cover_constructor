"""
Take care of all Database connections and creation

AUTHORS:

- Syed Ahmad Lavasani (ahmad.lavasani@gmail.com)
- Syed Ahmad Lavasani, Jan 2014, Moving to Alchamy structure, moved the functions to db_tools
"""
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, create_engine
import datetime

DB_NAME = "auto_func_field"
DB_USER = "root"
DB_PASS = ""
DB_HOST = ""

Base = declarative_base()

class CurveEquation(Base):
    """
    table: curve_equation
    id: unique id used for relation.
    group_order: automorophism group order
    group_index: the index in the small group database. (order, index)
    signature: the signature of the group. (for now we keep it as an string, if we need more
    flexibility I'll change it to a table for signatures, signature doesn't consists of cycles name but the generator ids)
    const_field_order: order of const field
    no_of_params: how many params are used in defining the equation of
                  the function field, it should be in a_0,...,a_{n-1}
    curve_equataion: the plannar equation of the function field which
                     safely can be singular in text format.
    """
    __tablename__ = "curve_equation"
    id = Column(Integer, autoincrement=True, primary_key=True)
    group_order = Column(Integer)
    group_index = Column(Integer)
    signature = Column(String(4096))
    const_field_order = Column(Integer)
    no_of_params = Column(Integer)
    curve_equation = Column(String(4096))

# I wrote this class but I realized that this is just
# the same as FixedField
#
# class VulnerableSubfields(Base):
#     """
#     table: vulnerable_subfields
#     id: unique id used for relation.
#     cover_id: is the id of the cover from curve_equation
#     sigma_x: the image of x under automorphism whose fixed field is the subfield
#     sigma_y: the image of y under automorphism whose fixed field is the subfield
#     subfield_equataion: the equation of the function field which is vulnerable
#                         to lower genus cover attack
#     """
#     __tablename__ = "vulnerable_subfield"
#     id = Column(Integer, autoincrement=True, primary_key=True)
#     cover_id = Column(Integer)
#     sigma_x = Column(String(4096))
#     sigma_y = Column(String(4096))
#     subfield_equataion =  Column(String(4096))

    def __init__(self, group_order, group_index, signature, const_field_order, no_of_params, curve_equation):
        self.group_order = group_order
        self.group_index = group_index
        self.signature = signature
        self.const_field_order = const_field_order
        self.no_of_params = no_of_params
        self.curve_equation = curve_equation

class Ramification(Base):
    """
    table: ramifications
    id: unique id as always
    curve_id: the curve id that relate them to the curve, ramified there
    ramification index
    generator id: this relates the ramification with a element of the
    group. it is simply the order of the element in sage group
    """
    __tablename__ = "ramification"
    id = Column(Integer, autoincrement=True, primary_key=True)
    group_order = Column(Integer)
    group_index = Column(Integer)
    curve_id = Column(Integer)
    ramification_index = Column(Integer)
    generator_id = Column(Integer)

    def __init__(self, group_order, group_index, curve_id, ramification_index, generator_id):
        self.group_order = group_order
        self.group_index = group_index
        self.curve_id = curve_id
        self.ramification_index = ramification_index
        self.generator_id = generator_id

#Don't know why I need this
#class SmallGroupElements(Base)
        # #table: small group elements
        # #id: unique as always
        # #order:
        # #curve_id
        # self.db_cursor.execute("create table group_elements (id INT NOT NULL AUTO_INCREMENT, order INT, curve_id INT), PRIMARY KEY(id) ) ENGINE=INNODB;")

#class ElementExponents
        # #table: small group elements exponent
        # #id: unique as always
        # #elment_id: relation to the item
        # #gen_index: which gen exponent in small group db we are specifying
        # #exponent:
        # self.db_cursor.execute("create table element_exponents (id INT NOT NULL AUTO_INCREMENT, group_order INT, element_id INT, gen_index INT, exponent INT), PRIMARY KEY(id) ) ENGINE=INNODB;")
class Automorphism(Base):
    """
    table: automorphism
    id: unique as always
    element_id: the element in the group
    x_img
    y_img
    """
    __tablename__ = "automorphism"
    id = Column(Integer, autoincrement=True, primary_key=True)
    group_order = Column(Integer)
    group_index = Column(Integer)
    element_id = Column(Integer)
    element_rep = Column(String(4096))
    element_ord = Column(Integer)
    x_img = Column(String(4096))
    y_img = Column(String(4096))

    def __init__(self, group_order, group_index, element_id, element_rep, element_ord, x_img, y_img):
        self.group_order = group_order
        self.group_index = group_index
        self.element_id = element_id
        self.element_rep = element_rep
        self.element_ord = element_ord
        self.x_img = x_img
        self.y_img = y_img

class FixedField(Base):
    """
    Here I'm going to store the fixed fields of each automorphis
    table: fixed fields
    id
    curve_id
    automorphism_id
    fixed_genus
    fixed_equation
    """
    __tablename__ = "fixed_field"
    id = Column(Integer, autoincrement=True, primary_key=True)
    curve_id = Column(Integer)
    automorphism_id = Column(Integer)
    #fixed_genus = Column(Integer)
    fixed_equation = Column(String(4096))

    def __init__(self, curve_id, automorphism_id, fixed_genus, fixed_equation):
        self.curve_id = curve_id
        self.automorphism_id = automorphism_id
        self.fixed_genus = fixed_genus
        self.fixed_equation = fixed_equation

class TimeCheck(Base):
    """
    This record the time that the database
    was accessed to be modified.
    It mostly works as to check if we 
    have connection or not
    """
    __tablename__ = "time_check"
    id = Column(Integer, autoincrement=True, primary_key=True)
    checkin_time = Column(String(256))

    def __init__(self, time_str):
        self.checkin_time = time_str

if __name__ == "__main__":
    #Constants
    engine = create_engine('mysql://'+DB_USER+":"+DB_PASS+"@"+DB_HOST, echo=True, pool_recycle=3600)

    #HACK: Create the database
    conn = engine.connect()
    engine.execute("CREATE DATABASE IF NOT EXISTS "+DB_NAME)
    engine.execute("USE "+ DB_NAME)

    Session = sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    session = Session()
    # q = session.query(CDBLog)
    # print q
