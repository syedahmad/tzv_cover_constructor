"""
Kummer extension Function Fields of deg q with G/C_q = C_n

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
from pdb import set_trace
from sage.rings.function_field.constructor import FunctionField
from sage.rings.arith import is_prime, gcd
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing

from sage.misc.misc_c import prod

from signatured_function_field import SignaturedFunctionField

class SignaturedFunctionFieldCyclic(SignaturedFunctionField) :
    """
    Covering Brandt TypI, TypIIa and TypIIb
    Simply calling the parent init, for most cases this should be enough
    as the compute_function_field is overriden
    """

    def _compute_number_of_parameters(self):
        """
        First we need to deal with the two unique ramified places of
        F_1/F_0, after that we just put parameters for other ramified
        places
        """
        #First we need to deal with other ramified places,

        #F
        #|
        #F_0
        #|     self.quotient_group_id[0] = n
        #F_1
        # Hurwitz 2(0-1) = n(2(0-1)) + (n - 1)*(no of ram places) =>
        # 2 (n - 1) = (n - 1)* (no of ram places) => no of ram = 2

        # now the question is how many of these places are ramified F/F_0

        #each ramification is a degree of freedom beside if 0 and infinity
        #are ramified. We only need to know if they are also (partially)
        #ramified in the kummer extension
        self._no_of_double_ramified_places = sum([not cur_Versweiguntyp[0] == cur_Versweiguntyp[1] and 1 or 0 for cur_Versweiguntyp in zip(self._reduziert_Verzweigungstyp, self._Versweiguntyp)]) #either 0, 1 or 2

        #Beside the double ramified places, the no of rest of ramified places
        #need to be divisible by the quotient_group order. Because P_1...P_m'
        #lives in F_0 and not ramified oven F_1 hence, there shouldn't be an
        #automorphism g such that g(P_1) = P_1. Because Z is cyclic then
        #g^i(P_1) = P_i hence ord(g) = m'
        self._var_str = ['a'+str(i) for i in range(0, (len(self._signatured_group.sub_signature(self._rationalizing_subgroup)[1])-self._no_of_double_ramified_places)/self._quotient_group_order)]
        self.no_of_params = len(self._var_str)

    def _compute_kummer_factors(self):
        """
        We return an array each element is a factor corresponding to a
        ramified place. The last factor is always correspond to places
        purely ramify in the kummer extension

        CONVENTION: If infinity is ramified we set the place corresponding to infinity
        equal to 1
        """
        # Sei F_0/k ein rationaler Funktionenkorper und F/F_0 eine Galois-
        # Erweiterung vom Grad q >= 2,R_1
        # q prime, char K = P,  (q,p) = 1, sigma in Aut(F_0) sei nach F
        # fortsetzbar und
        # simga' eine Fortsetzung von sigma. Sei P in F_0 mit sigma(P) = P und
        #ord(sigma) = q.
        # Dann gilt:
        # 1) P is unverzweigt <=> ord(sigma') = q  (C_q (+) C_q)
        # 2) P is unverzweigt <=> ord(sigma') = q^2 (C_q^2)

        #Rules for cyclic extensions

        #reduzierte verzweigugstyp is always (n, n) the first is infinty
        #the second is (x-0)
        self.kummer_factors = [self._rational_function_field(1), self._rational_function_field.gen()]
        self.kummer_factors.extend([self._rational_function_field.gen()**self._quotient_group_order - cur_param.parameter for cur_param in self._params])
        #just for test of using 1/x automorphism instead
        #self.kummer_factors.extend([(self._rational_function_field.gen()**self._quotient_group_order + 1)/self._rational_function_field.gen() - cur_param.parameter for cur_param in self._params])

    def _quotient_automorphism_generators(self):
        """
        This is to be overloaded. The generators of the restricted
        automorphism group to the base field need to be hard coded

        In case of cyclic quotient and large prime we always have
        P_0 and P_oo ramified only down stairs so the only automorphism

        x -> \eta x where eta is n'th root of unity

        OUTPUT::
            Returns [\eta x]
        """
        #First we need to generate the root of unity that we need
        #zeta_n = self._primitive_nth_root_of_unity(self._quotient_group_order)
        zeta_qn = self._primitive_nth_root_of_unity(self._quotient_group_order * self._kummer_ext_deg)
        zeta_n = zeta_qn ** self._kummer_ext_deg

        #computing y coeffiecient based on Br88 Lemma 5.5
        set_trace()
        if (not self._rational_function_field.gen() in self.kummer_factors): #x^k_0
            y_coeff = 1
        else:
            y_coeff = zeta_qn**self._kummer_factor_powers[self.kummer_factors.index(self._rational_function_field.gen())]
        return [(y_coeff* self._gen, self(zeta_n * self._rational_function_field.gen()))]
        #just for test 1/x automorphism
        #return (1/self._rational_function_field.gen(),)

    @classmethod
    def have_compatible_structure_for(cls, signatured_group, rationalizing_subgroup):
        """
        We assume that the validity test has been done. So we only need to
        check if the quotient is cyclic

        We also need to check the genus of the fixed field to be 0
        """
        if not (rationalizing_subgroup.is_cyclic() and signatured_group.quotient(rationalizing_subgroup).is_cyclic() and signatured_group.sub_signature(rationalizing_subgroup)[0]==0):
            return False

        #direct produdct check
        return cls.is_direct_product_extension(signatured_group, rationalizing_subgroup)
