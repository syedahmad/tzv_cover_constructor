"""
Tries to generates signatured function field with automorphism group

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
import  sys
sys.path.append('/lu101/sahosse/doc/phd/sage/field_gen')
from jennifer_digest_gen10_problem import JenniferDigest

class EligibleCurveFinder:
    # cover genus   =   0  1     2      3             4
    #eligible_deg_gen = [[],[],[(1,2)],[(2,2)], [(2, 2), (3,2)], [(2,2),(3,2),(4,2)]]
    eligible_deg_gen = [[],[],[(1,2)],[(2,2)], [(2, 2)], [(2,2),(3,2)]]
    full_auto_list = {3: [(168,42), (6,1),(2,1),(4,2),(8,3),(24,12),(96,64),(16,13),(48,33),(3,1),(6,2),(9,1),(48,48),(32,9),(24,5),(14,2),(16,11),(12,4),(8,2),(8,5),(4,1),(4,2),(2,1)], 4:[(120,34),(72,42),(72,40),(40,8),(36,12),(32,19),(24,3),(18,2),(15,1),(12,2),(10,2),(36,10),(6,2),(6,1),(24,12),(20,4),(18,3),(16,7),(12,5),(12,4),(12,3),(10,1),(8,4),(6,2),(5,1),(12,4),(8,3),(8,3),(6,2),(4,1),(6,1),(4,2),(4,1),(3,1),(3,1),(3,1),(4,2),(4,2),(2,1),(2,1),(2,1)]}

    def  __init__(self, sig_group_filename, genus):
        """
        for now we are doing it genus by genus
        """
        self.genus = genus
        self.sig_group_filename = sig_group_filename
        self.sig_group_list = []
        self.eligible_groups = {}

    def generate_all_signatured_group(self):
        file_digester = JenniferDigest(self.genus, self.sig_group_filename)
        self.sig_group_list = file_digester.digest_monodromies(full_auto_only=True, no_repeat=True)

    def is_eligibile(self, sign_grp):
        """
        We check eligibility by computing the fixed signatures of monodromies
        of proper degree. This varies genus by genus
        """
        #return False
        #just for test constructability of everything
        #return True
        eligible = False
        S3 = SymmetricGroup(3)
        for h in sign_grp.subgroups():
            if  h.group_id() == [6,1]:
                print sign_grp.group_id(), " has S3"
                print "S3 is normal? ",h.is_normal(sign_grp)
                cur_sub_sig = sign_grp.sub_signature(h)
                if cur_sub_sig[0] == 2:
                    sub_ram = [i.order() for i in cur_sub_sig[1]]
                    print "S3 has genus 2 fixed field with signature ", sub_ram
                    if (sub_ram == [2, 2]):
                        mid_sig = sign_grp.sub_signature(h.normal_subgroups()[1])
                        if (mid_sig[0]) == 4:
                            print "mid signature of genus 4 with ram ", [i.order() for i in mid_sig[1]]
                            for mid_subgrp in h.subgroups():
                                if (mid_subgrp.order() == 2):
                                    mid_sig = sign_grp.sub_signature(mid_subgrp)
                                    print "mid signature of the other side ",mid_sig[0], [i.order() for i in mid_sig[1]]
                                    return True
                        

                            
                            

    def eligible_morphisms(self, sign_grp):
        pass

    def check_all_eligibility(self):
        self.eligible_groups[self.genus] = []
        for cur_sig_grp in self.sig_group_list:
            if self.is_eligibile(cur_sig_grp):
                self.eligible_groups[self.genus].append(cur_sig_grp)

    def check_all_constructibility(self):
        for  cur_sig_grp in self.eligible_groups[self.genus]:
            print cur_sig_grp.group_id()
            continue
            print "Checking curve with group:", cur_sig_grp.group_id(), cur_sig_grp.signature_orders()
            constructable = self.constructable(cur_sig_grp)
            print cur_sig_grp.group_id(), " is ",  constructable and "constructable" or "unconstructable"

if __name__ == "__main__":
    #test
    # all_eligible_curves = EligibleCurveFinder("/lu101/sahosse/doc/phd/sage/field_gen/test_g4.txt", genus = 4)

    # all_eligible_curves.generate_all_signatured_group()
    # all_eligible_curves.check_all_eligibility()
    # all_eligible_curves.check_all_constructibility()

    all_eligible_curves = EligibleCurveFinder("grpmono10.txt", genus = 10)
    all_eligible_curves.generate_all_signatured_group()
    all_eligible_curves.check_all_eligibility()
    all_eligible_curves.check_all_constructibility()

        # The code sniept suggesting that [12, 1][1,3] isn't full-auto grp.
        # if (sign_grp.group_id() == [24, 8]):
        #     import pdb
        #     pdb.set_trace()
        #     gap_group = gap.SmallGroup(12, 1)
        #     gap_perm_group_iso = sign_grp._gap_().IsomorphicSugroups(gap_group)[1]
        #     gap_gens = gap_group.GeneratorsOfGroup()
        #     gap_perm_group = gap_perm_group_iso.Image(gap_group)

        #     fix_group = sign_grp.subgroup([gap_perm_group.GeneratorsOfGroup()])
        #     sign_grp.sub_signature(fix_group)[0], [i.order() for i in sign_grp.sub_signature(fix_group)[1]]
