"""
Kummer extension Function Fields of deg q with G/C_q = C_n

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
from sage.rings.function_field.function_field import FunctionField

class SignaturedFunctionFieldCyclic(SignaturedFunctionField) :
    """
    Covering Brandt TypI, TypIIa and TypIIb
    Simply calling the parent init, for most cases this should be enough
    as the compute_function_field is overriden
    """
    def compute_function_field():
        """
        should be implemented by appropriate subclass
        """
    def cyclic_function_field():
        """
        First we need to deal with the two unique ramified places of
        F_1/F_0, after that we just put parameters for other ramified 
        places
        """
        #First we need to deal with other ramified places, 

        #each ramification is a degree of freedom beside if 0 and infinity
        #are ramified.
        no_of_double_ramified_places = sum[cur_Versweiguntyp == self.cyclic_ext_size * self.quotient_group_id and 1 or 0 for cur_Versweiguntyp in self.Versweiguntyp] #either 0, 1 or 2

        var_str = ['a'+str(i) for i in range(0, len(signatured_group.sub_signatures(cyclic_sub_grp))-no_of_double_ramified_places)]

        #then we make the polynomial ring we works on.
        self._ring = PolynomialRing(self.constant_field, var_str)
        self._gens = self._ring.gens()
        self._base_field = self._ring.fraction_field()
        self._rational_function_field = FunctionField(self._base_field)

        #we put all of them on power 1 because repeated factors means
        #singularity in kummer extensions and doesn't  change the 
        #genus, so why should I just make the matter more complicated
        self.kummer_factors = [self._rational_function_field.gen() - cur_param for cur_param in self._base_field.gens()]

        #now if zero is double ramified we multiply x to kummer product
        Versweiguntyp[0] == self.cyclic_ext_size * self.quotient_group_id[0] and self.kummer_factors.append(self._rational_function_field.gen())

        #for infinity if it is ramified we need to make sure that the powers
        #doesn't add up to q
        if (self.Versweiguntyp[1] == self.cyclic_ext_size * self.quotient_group_id[0]):
            if (gcd(len(self.kummer_factors),self.cyclic_ext_size)==self.cyclic_ext_size):
                #if self.cyclic_ext_size = 2 we are doomed
                self.kummer_factors.append(kummer_factors[len(kummer_factors)])
        else:
            #we need to make sure that the powers are adds up to q
            i = 0
            while(gcd(len(self.kummer_factors),self.cyclic_ext_size)!=self.cyclic_ext_size):
                self.kummer_factors.append(kummer_factors[i])
                i += 1
        
        R = PolynomialRing(_rational_function)
        return self._rational_function.extension(R.gen**self.cycic_ext_size-prod(self.kummer_factors)

    def have_compatible_structure(self, genus, signtured_group, rationalizing_subgroup, sub_signature):
        """
        This function should be implemented by each child. It's called by the
        class factory to check if the child can generate the function field e
        equation for specific group/subgroup/ramification situation.
        """
        pass


