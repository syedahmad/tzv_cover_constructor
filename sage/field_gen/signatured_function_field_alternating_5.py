"""
Kummer extension Function Fields of deg q with G/C_q = A_5

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
from pdb import set_trace
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.groups.perm_gps.permgroup_named import DihedralGroup
from sage.misc.misc_c import prod

from signatured_function_field import SignaturedFunctionField

class SignaturedFunctionFieldAlternatvie5(SignaturedFunctionField) :
    """
    Covering all cases with altenatvie A4 group as factor group
    Simply calling the parent init, for most cases this should be enough
    as the compute_function_field is overriden
    """
    def _compute_fundamental_values(self):
        """
        This to compute the values needed in Brandt's thesis to 
        compute the equation specific to this family.
        """
        #For the sake of following Brandt's naming
        SignaturedFunctionField._compute_fundamental_values(self)

    def _compute_number_of_parameters(self):
        """
        To construct the appropriate rational function field which is 
        needed to represent a general function field with given
        automorphism group with a4 as quotient, we need to know
        how many parameters we need.
        
        We also need to extend our constants if necessary
        """
        self.no_of_params = len(self._purely_kummer_ramified)

        self._var_str = ['a'+str(i) for i in range(0, self.no_of_params)]

        #we also need to add i and sqrt(3) to the constant field
        #then we make the polynomial ring we works on.
        aux_ring = PolynomialRing(self._constant_field, names = 'T')
        T = aux_ring.gen()
        self._extend_constant_field([T**2 + 1, (T**5-1)/(T-1)])

        #also we need to make choice for i and sqrt and we need to be 
        #consistant all the way long
        self._zeta_4 = self._constant_field.multiplicative_generator()**((self._constant_field.order()-1)/4)
        self._zeta_5 = self._constant_field(3).sqrt()

    def _compute_kummer_factors(self):
        #we put all of them on power 1 because repeated factors means
        #singularity in kummer extensions and doesn't  change the 
        #genus, so why should I just make the matter more complicated

        #we need these values
        b = -self._zeta_4*(self._zeta_5 + self._zeta_5**4)
        #infinity is always as ramified as place (x) so we add 1 at the end
        x = self._rational_function_field.gen()
        i = self._zeta_4
        import pdb
        pdb.set_trace()
        self.kummer_factors = [x**30+522*i*x**26 + 10005*(x**20-x**10)-522*i*x**5-1, x**20-228*i*x**15-494*x**10-228*i*x**5+1, x**11+11*i*x**6+x, x.parent().one()]

        #set the power of infinity so it ramifies as much as (x) ramifies
        self._kummer_factor_powers = self._kummer_factor_powers[:3]+[self._kummer_factor_powers[2]]+ self._kummer_factor_powers[3:]

        #purely kummers
        for cur_a in  self._params:
            self.kummer_factors.append((x**20 - 228*i*x**15-494*x**10-228*i*x**5+1)**3 - cur_a.parameter*(x**11+11*i*x**6+x)**5)
        
    @classmethod
    def have_compatible_structure_for(cls, signatured_group, rationalizing_subgroup):
        """
        We assume that the validity test has been done. So we only need to 
        check if the quotient is cyclic 
        """
        if rationalizing_subgroup.is_cyclic() and  signatured_group.quotient(rationalizing_subgroup).group_id() == [60,5]:
            return True
