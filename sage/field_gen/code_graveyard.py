#normally_decompose using tree or greedy element by elements
        if not abl_sub_grp.is_normal(self) or not abl_sub_grp.is_abelian():
            verbose("%s supposed to be a normal belian"%abl_sub_grp);
            return None

        class SubgroupNode(object):
            def __int__(self, subgroup, parent, children):
                """
                INPUT:
                   Subgroup: the subgroup this node pointing at
                   children: a list of index indicating the place of this node
                      's children in the list of all normal subgroup
                """
                self.Subgroup = Subgroup
                #self.parent = parent
                self.children = children
                self.traversed = False
                self.direct_product = None

        all_normal_list = self.normal_subgroups()
        subsub_normal_list = [cur_subgrp in all_normal_list if cur_subgrp.is_subgroup(abl_sub_grp) and cur_subgrp.is_cyclic()]
        subsub_normal_list = sorted([(subgrp, subgrp.order()) for subgrp in subsub_normal_list], key=itemgetter(1))

        subgroup_partial_order_tree = []
        for i in range(0, len(subsub_normal_list)):
            subgroup_partial_order_tree.append(i, [j for j in range(i+1, len(subsub_normal_list)) \
                if subsub_normal_list[j].is_subgroup(subsub_normal_list[i])])
        
        for cur_subgroup in subgroup_partial_order_tree:
            if cur_subgroup.traversed: continue
            cur_subgroup.traversed = True
            
            to_crawl = cur_subgroup.childern
            parent  = cur_subgroup
            while to_crawl:
                current = to_crawl.popleft()
                current.direct_product = parent.direct_product.direct_product(current)

                if (current.direct_product.order() = abl_sub_grp.order()):
                    

                to_crawl.extend(current.children)
                
            
            
        #now we try to extract as biggest cyclic subgroup as possible out of
        #the file.
        cur_gen_list = []
        cur_dir_prod_rep = self.subgroup([abl_sub_grp.identity()])
        abg_elms_sorted = sorted([(g, g.order()) for g in abl_sub_grp], key=itemgetter(1))
        #et_trace()
        while(not cur_dir_prod_rep.order() == abl_sub_grp.order()):
            try:
                cur_elm = abg_elms_sorted.pop(-1)[0]
                while(cur_elm in cur_dir_prod_rep or not self.subgroup([cur_elm]).is_normal()):
                    cur_elm = abg_elms_
orted.pop(-1)[0]
            except IndexError:
                #no normal decomposition
                return None
            
            cur_gen_list.append(cur_elm)
            cur_dir_prod_rep = self.subgroup(cur_gen_list)

        return cur_gen_list
