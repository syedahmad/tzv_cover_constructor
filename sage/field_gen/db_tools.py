"""
The interface for reading and writing into the database

AUTHORS:

- Syed Ahmad Lavasani (ahmad.lavasani@gmail.com)
- Syed Ahmad Lavasani, Jan 2014, Moving to Alchamy structure
"""
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, create_engine
import sqlalchemy
import datetime

from time import strftime

import db_init as curve_db

class CurveDatabase:
    def __init__(self):
        """
        Just create the engine and the session class
        """
        self.engine = create_engine('mysql://'+curve_db.DB_USER+":"+curve_db.DB_PASS+"@"+curve_db.DB_HOST, echo=True, pool_recycle=3600)
        self._check_and_reconnenct()
        
        #self.engine.execute("USE "+curve_db.DB_NAME)
        #self.Session = sessionmaker(bind=self.engine)

    def _check_and_reconnenct(self):
        """
        Because it takes a long time to finish a computation
        we have to open and close the database to write in any
        transaction.

        Connect to mysql and connect to the database.
        """
        try:
            test_connection = self.engine.connect()
            print "testing ", test_connection # is there any better way to simply check if connection to mysql is alive?
            test_connection.execute("select now()")
            self.engine.execute("USE "+curve_db.DB_NAME)
            self.Session = sessionmaker(bind=self.engine)

            session = self.Session()
            #writing time and date in db
            new_checkin = curve_db.TimeCheck(strftime("%Y-%m-%d %H:%M:%S"))

            session.add(new_checkin)
            session.flush()
            session.close()
            
        except sqlalchemy.exc.OperationalError:
            print "We got disconnected from db, trying to reconnect..."
            self.engine.execute("USE "+curve_db.DB_NAME)
            self.Session = sessionmaker(bind=self.engine)

    def __del__(self):
        """
        Destructor just to commit the session
        I'm not sure it is a good thing to do
        """
        pass
        #self.session.commit()


    def _create_db_structure(self):
        """
        It is called when the db doesn't exists and it makes
        all tables. (obsolete: this happens in db_init
        """
        pass
        # self.db_conn = MySQLdb.connect(host=self.DB_HOST,user=DB_USER,passwd=DB_PASS)

        # self.db_cursor = self.db_conn..cursor()
        # self.db_cursor.execute('CREATE DATABASE ' + DB_NAME)

    def insert_curve_equation(self, signatured_function_field):
        """
        When the program finds a equation it should ask this function
        to store it into the database

        INPUT::
            signatured_function_field: the function field contains
            the equation and the group with the signature.

        OUTPUT::
            the id of the curve in database for later use
        """
        #we fill from detail to main, so first the signature elements components
        #then the curve itself. Because we need the auto-ids be generated before
        #we can put them in other tables.

        #group_order: automorophism group order
        #group_index: the index in the small group database. (order, index)
        #signature: the signature of the group.
        #const_field: order of const field
        #no_of_params: how many params are used in defining the equation of
        #              the function field, it should be in a_0,...,a_{n-1}
        #curve_equataion: the plannar equation of the function field which

        #connect to databes
        session = self.Session()

        curve_base_const_ring_order = signatured_function_field.rational_function_field().constant_field().base_ring().order()
        new_curve = curve_db.CurveEquation(signatured_function_field._signatured_group.group_id()[0], signatured_function_field._signatured_group.group_id()[1], str(signatured_function_field._signatured_group.signature_element_indices()).strip('[').strip(']'),curve_base_const_ring_order, signatured_function_field.no_of_params, str(signatured_function_field.polynomial()))

        session.add(new_curve)
        session.flush()

        curve_id = new_curve.id
        #go through the siganture and generate elements
        for cur_auto_grp in signatured_function_field._signatured_group.signature():
            #reverse mapping to find the element index in Sage group
            for i in range(0,signatured_function_field._signatured_group.order()):
                if (signatured_function_field._signatured_group[i] == cur_auto_grp.gen()):
                    #table: ramifications
                    #id: unique id as always
                    #curve_id: the curve id that relate them to the curve, ramified there
                    #ramification index
                    #generator id: this relates the ramification with a element of the
                    #group. it is simply the order of the element in sage group
                    curve_ram = curve_db.Ramification(signatured_function_field._signatured_group.group_id()[0], signatured_function_field._signatured_group.group_id()[1], curve_id, cur_auto_grp.order(),i)
                    session.add(curve_ram)
                    break

        session.flush()
        session.commit()
        session.close()
        return curve_id

    def insert_vulnerable_subfield(self, cover_id, vulnerable_subfield, fixed_genus, fixing_aut, img_x, img_y):
        """
        When a subfield is found, the automorphism of for which the subfield is vulnerable
        will be stored and then the equation of the subfield

        INPUT::
           cover_id: the id of the curve in the curveEquation for which there
                     is a vulnerable subfield.
           vulnerable_subfield: the function field of the fixed field of the
                                automorphism
           fixing_aut: the automorphism whose fixed field is
                       vulnerable
           img_x: fixing_aut(x)
           img_y: fixing_aut(y)
        """
        #connect to databes
        #check if the engine is sane, otherwise we make a new engine
        
        print strftime("%Y-%m-%d %H:%M:%S")
        self._check_and_reconnenct();
        session = self.Session()

        #discover element gap id
        element_id = -1
        for i in range(0, fixing_aut.parent().order()):
            if fixing_aut == fixing_aut.parent()[i]:
                element_id = i
                break

        if element_id == -1:
            raise ValueError, "Unable to spot the element in the group"

        new_automorphism = curve_db.Automorphism(fixing_aut.parent().group_id()[0], fixing_aut.parent().group_id()[1], element_id, str(fixing_aut), fixing_aut.order(), str(img_x), str(img_y))

        session.add(new_automorphism)
        session.flush()
        
        new_subfield = curve_db.FixedField(cover_id, new_automorphism.id, fixed_genus, str(vulnerable_subfield.polynomial()))

        session.add(new_subfield)
        session.flush()
        session.commit()
        session.close()

