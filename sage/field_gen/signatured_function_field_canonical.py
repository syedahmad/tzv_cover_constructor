﻿"""
Signatured Function Fields with canonical embedding

This

The design will be as follows:
   - The parent class Signatured Function field daughter of  FunctionField_polymod.

   - There would be a factory class thet returns the correct class for choice    G/C

AUTHOR:

- Syed Ahmad Lavasani (2014)


"""
from pdb import set_trace

from sage.modules.free_module_element import vector
from sage.rings.finite_rings.constructor import FiniteField
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.function_field.constructor import FunctionField
from sage.rings.function_field.function_field import FunctionField_polymod
from sage.misc.misc_c import prod

from sage.misc.sage_eval import sage_eval
from sage.calculus.var import var
from sage.matrix.constructor import matrix
from signatured_function_field import SignaturedFunctionField

class SignaturedFunctionFieldCanonical(SignaturedFunctionField) :
    """
    This function field is having the following properties:

    - signature over the fixed field of whole automorphism group.

    However to generate this function field unlike FunctionField_polymod
    we only feed the automorphism_group, signature.

    It extracts the ideal of the curve by analyzing the action of the
    group over the ring that contain the canonical embedding of the curve

    The downside is that it might fail.
    """
    NO_OF_CHUNKS = 7
    CHK_GENUS = 0
    CHK_GROUP_ID = 1
    CHK_CONST_EXTENSION = 2
    CHK_FREE_PARAM = 3
    CHK_VARS = 4
    CHK_EQUATIANS = 5
    CHK_MAT_GENS = 6

    def __init__(self, signatured_group, definition_line, constant_field, names = None):
        """
        This read the equations defining the ideal in P^g and the matrices
        generating the automorphism group from an String and generate
        the function field with the automorphism group.

        INPUT::
            definition_line: is a string which define the curve as follows:
            Genus, Group, (constant field paramter, constant field extending equation), free params, variables in P^g, equations, gen matrices
            ex.
            {4}, {36,10}, {w, w^2+w+1}, {t,}, {a,b,c,d}, {a*b+c*d,a^3-b^3+t*(c^3-d^3)}, {[[0,w,0,0],[w^2,0,0,0],[0,0,-1,0],[0,0,0,-1]],[[-1,0,0,0],[0,-1,0,0],[0,0,0,1],[0,0,1,0]],[[0,-1,0,0],[-1,0,0,0],[0,0,0,-w],[0,0,-w^2,0]]}
        """
        #http://stackoverflow.com/questions/8569201/get-the-string-within-brackets-in-python-using-regex
        #First step we break the definition string in chunk of data
        import re #using regular expression

        #gens' name is the only info not encoded in the def string
        self._signatured_group = signatured_group
        self._all_gens_names = (names == None) and ['y','x'] or names

        chunk_matcher = r"([\w \,\^*/+\-\(\)\[\]]*)"
        chunk_delim = r"}\ *\,\ *{"
        chunker = (chunk_matcher + chunk_delim)*(SignaturedFunctionFieldCanonical.NO_OF_CHUNKS-1) + chunk_matcher
        #set_trace()
        try:
            definition_chunks = re.search(chunker, definition_line).groups()
        except ValueError:
            raise ValueError, "Syntax error in defitition info"

        try:
            self._genus = int(definition_chunks[SignaturedFunctionFieldCanonical.CHK_GENUS])
        except ValueError:
            raise ValueError, "error in defining genus defitition"

        try:
            self._group_id = definition_chunks[SignaturedFunctionFieldCanonical.CHK_GROUP_ID].split(',')
            self._group_id = map(int, self._group_id)

        except ValueError:
            raise ValueError, "error in defining automorphism group id"

        try:
            constant_field_gen = None
            const_field_info = definition_chunks[SignaturedFunctionFieldCanonical.CHK_CONST_EXTENSION].strip()
            if const_field_info == '':
                self._constant_field = constant_field
            else:
                constant_field_gen = const_field_info.split(',')[0]
                const_ext_poly_str = const_field_info.split(',')[1]

                const_poly_ring = PolynomialRing(constant_field, constant_field_gen)
                const_ext_poly = const_poly_ring(const_ext_poly_str)

                if (not const_ext_poly.is_irreducible()):
                    #raise ValueError, "polynomial used to extend the constant field is not irreducible"
                    const_ext_poly = const_ext_poly.factor()[0][0]

                #self._constant_field = constant_field
                self._constant_field = FiniteField(constant_field.order()**const_ext_poly.degree(), constant_field_gen+"p")

                #used to replace orginal gen by the default sage gen
                #const_ext_var = var(constant_field_gen)
                const_gen_in_const_field = const_ext_poly.roots()[0][0]

                #constant_field.extension(const_ext_poly, constant_field_gen)

        except ValueError:
            raise ValueError, "error in definining the constant field"

        try:
            if not definition_chunks[SignaturedFunctionFieldCanonical.CHK_FREE_PARAM].strip() == '':
                self._constant_field = PolynomialRing(self._constant_field, definition_chunks[SignaturedFunctionFieldCanonical.CHK_FREE_PARAM]).fraction_field()
                self.no_of_params = self._constant_field.ngens()
            else:
                self.no_of_params = 0

        except ValueError:
            raise ValueError, "error in definining the free parameters of constant field"

        try:
            P_genus = PolynomialRing(self._constant_field, definition_chunks[SignaturedFunctionFieldCanonical.CHK_VARS], order='lex')

        except ValueError:
            raise ValueError, "error in definining the variables of the canonical space"

        try:
            #making a comprehensive translation dictionary
            comp_gen_dict = P_genus.gens_dict()
            comp_gen_dict.update(P_genus.base_ring().gens_dict())
            #comp_gen_dict['t'] = 1
            if (constant_field_gen):
                comp_gen_dict[constant_field_gen] = const_gen_in_const_field#P_genus.base_ring().base_ring().gen()
                
            self._canonical_curve_equations = [sage_eval(cur_eq, comp_gen_dict) for cur_eq in definition_chunks[SignaturedFunctionFieldCanonical.CHK_EQUATIANS].split(',')]

        except ValueError:
            raise ValueError, "error in definining the curve equations in the canonical spac,comp_gen_dicte"

        try:
            self._matrix_automorphism_gens = map(matrix,sage_eval(definition_chunks[SignaturedFunctionFieldCanonical.CHK_MAT_GENS],comp_gen_dict))

        except ValueError:
            raise ValueError, "error in definining the generators of the automorphism group representation"

        #making the ideal of the curve:
        self._construct_rational_function_field()
        from sage.misc.misc import set_verbose
        self._canonical_ideal = P_genus.ideal(self._canonical_curve_equations)

        set_verbose(2)
        #set_trace()
        self._images_of_ideal_generators(self._canonical_ideal)

        self._function_field_tower, self._var_to_tower_map = self.function_field_of_curve(self._canonical_ideal)
        #how to map the automorphism:
        #trans_auto = re-write the automorphism map in term of cur_poly_map
        #set_trace()
        self.tower_automorphic_maps = self._automorphisms_to_tower(self._var_to_tower_map, self._function_field_tower[-1], self._canonical_ideal)

        (simple_top_field, to_sim, from_sim) = self._function_field_tower[-1].make_simple()
        my_poly = simple_top_field.polynomial()

        #my_poly = self._compute_polynomial()
        print "Decanonization info:"
        print "canonical vars to tower", self._var_to_tower_map
        print "from tower to simple", to_sim
        print "from simple to tower", from_sim
        print "check", self._var_to_tower_map[0]*self._var_to_tower_map[1]+self._var_to_tower_map[2]

        FunctionField_polymod.__init__(self, my_poly, names=(self._all_gens_names[0],))

        print "The algebraic function field of the canonical ideal", self

        self.to_sim  = simple_top_field.hom((self.gen(), self.rational_function_field().gen())) * to_sim
        self.from_sim  = from_sim * self.hom((simple_top_field.gen(), simple_top_field.rational_function_field().gen()))

        self._compute_automorphism_group()

    def _compute_automorphism_group(self):
        """
        Compute the automorphism group by determining a set of generators and then
        Asking the group to generate the automorphism group in traditional way
        (mapping gap generators to automorphisms images)
        """
        self._auto_gens = self._compute_automorphism_generators()
        print "provided generators for automorphism group", self._auto_gens
        #we call the automorphis_group function to compute and store the
        #automorphism group using
        #set_trace()
        self._automorphism_group = self.automorphism_group(tuple(self._auto_gens), tuple(self._group_id))

    def _compute_automorphism_generators(self):
        auto_gen_imgs = []

        for cur_tower_auto_map in self.tower_automorphic_maps:
            #make the unsimplified function field intermediary automorhpism
            simple_aut = self.to_sim * cur_tower_auto_map * self.from_sim
            auto_gen_imgs.append((simple_aut(self.gen()), simple_aut(self.rational_function_field().gen())))

        return auto_gen_imgs
        #map from_sim(y)
        #[to_sim(trans_auto(from_sim(y))), to_sim(trans_auto(x))]


    def _images_of_ideal_generators(self, curve_ideal):
        for cur_mat in self._matrix_automorphism_gens:
            cur_can_space_map = []

            for cur_var_i in range(0,len(curve_ideal.ring().gens())):
                if (curve_ideal):
                    cur_can_space_map.append(cur_mat.column(cur_var_i) * vector(curve_ideal.ring().gens()))

            image_ideal_gens = []
            print cur_mat
            for cur_gen in curve_ideal.gens():
                print "fr: ", cur_gen
                print "to: ", cur_gen(cur_can_space_map)
                    
            #set_trace()

    def _automorphisms_to_tower(self, var_to_tower_map, tower_tip, curve_ideal = None, projective_coordinate = True):
        tower_automorphic_maps = []
        for cur_mat in self._matrix_automorphism_gens:
            cur_map = []
            if curve_ideal:
                cur_can_space_map = []

            for cur_var_i in range(0,len(var_to_tower_map)):
                if (curve_ideal):
                    cur_can_space_map.append(cur_mat.column(cur_var_i) * vector(curve_ideal.ring().gens()))

                cur_map.append(cur_mat.column(cur_var_i) * vector(var_to_tower_map))

            if (projective_coordinate):
                cur_map = [cur_img / cur_map[-1] for cur_img in cur_map]
                cur_map = cur_map[:-1]

            #set_trace()
            if (curve_ideal): #to test the sanity of automorphisms
                image_ideal_gens = []
                for cur_gen in curve_ideal.gens():
                    image_ideal_gens.append(cur_gen(cur_can_space_map))
                    
                image_ideal =  curve_ideal.ring().ideal(image_ideal_gens)
                if (curve_ideal != image_ideal):
                    raise ValueError, "Not an automorphism of the curve ideal: %s \n it takes %s to %s"%(cur_mat, curve_ideal, image_ideal)

            cur_aut = tower_tip.hom(tuple(cur_map))
            tower_automorphic_maps.append(cur_aut)

        return tower_automorphic_maps

    # def __init__(self, signatured_group, constant_field, names = None, elimination_info = None):
    #     """
    #     INPUT::
    #     Compute the representation of the automorphism group over canonical embedding
    #     and compute an ideal that contain the function field equation
    #     """
    #     self._signatured_group = signatured_group
    #     self._genus = signatured_group.genus()
    #     self._constant_field = constant_field
    #     self._paramless_constant_field = constant_field

    #     #if (not self.check_structure_validity()):
    #     #        raise ValueError, "The fixed field of the subgroup is not rational"

    #     #we need to call Magma here and get the result
    #     #first load all of our Magma functions
    #     set_trace()
    #     magma.eval('load "autcv5_arbit_gen_len.mgm"')
    #     self._auto_gen_mats, quadratics, cubics, projmats = magma.RunExample(self._signatured_group, self._genus, self._signatured_group._signature_orders)

    #     self._compute_fundamental_values()
    #     self._construct_rational_function_field()
    #     my_poly = self._compute_polynomial()
    #     FunctionField_polymod.__init__(self, my_poly, names=(self._all_gens_names[0],))
    #     set_trace()
    #     self._compute_automorphism_group()

    # """
    # def __init__(self, polynomaial, names, autmorhpism_gens, galois_group, full_signature):
    # """    # Create a signatured function field

    # INPUT:

    #     - ``polynomial`` -- a univariate polynomial over a function field.
    #     - ``names`` -- variable names (as a tuple of length 1 or string)
    #     - ``automorphism_gens`` -- data to be send to the automorphism function to generate the automorphism group.
    #     - ``galois_group`` the subgroup of the automorphism group such that it's the automorphism group of F/k(x).
    #     - ``signature`` a tuple that contains the ramification of the field over k(x).
    # """
    def _compute_fundamental_values(self):
        """
        This is to compute the values needed in Brandt's thesis to
        compute the equation.
        """
        #For the sake of following Brandt's naming
        self._quotient_group = self._signatured_group.quotient(self._rationalizing_subgroup)
        #we use this so much that makes sense to store it in a variable
        self._quotient_group_order = self._quotient_group.order()
        self._kummer_ext_deg = self._rationalizing_subgroup.order()

        #this is the total ramification index only for those ramfied downstairs
        (self._reduziert_Verzweigungstyp, self._Versweiguntyp, self._purely_kummer_ramified, self._versweigun_gruppen) = self._signatured_group.Versweiguntyp_of(self._rationalizing_subgroup)
        print "Ramification:",(self._reduziert_Verzweigungstyp, self._Versweiguntyp, self._purely_kummer_ramified)
        #e(P|P_0) = _kummer_ext_deg/gcd(kummer_factor_power, _kummer_ext_deg)
        #= _kummer_ext_deg/kummer_factor_power =>
        # kummer_factor_power = _kummer_ext_deg/e(P|P_0)
        self._kummer_factor_powers = [self._kummer_ext_deg/(full_ram/red_ram) if not red_ram == full_ram else 0 for (red_ram, full_ram) in zip(self._reduziert_Verzweigungstyp, self._Versweiguntyp)]
        self._kummer_factor_powers.extend([self._kummer_ext_deg/(full_ram/1) for full_ram in self._purely_kummer_ramified])
        #power _kummer_ext deg is waste of resources but i doubt such a thig
        #note on purely kummer ramification. There will be |G|/|C| number
        #of places with this degree as they are unnramified in F^G/C but I
        #believe that Brandt algorithm group all of them in one factor
        #print "Kummer factor powers (before infinity repair):", self._kummer_factor_powers

    def _compute_number_of_parameters(self):
        """
        any further actoin should be implemented by appropriate subclass
        This is because each sub class needs different number of parameters
        added to the function field
        """
        self.no_of_params = len(self._purely_kummer_ramified)
        self._var_str = ['a'+str(i) for i in range(0, self.no_of_params)]

    def _construct_rational_function_field(self):
        """
        Using the parameters given from subclass generate the rational function
        field
        """
        #it is useful to have param names only array cause we need to use them
        #to evaluate at different x
        self._rational_function_field = FunctionField(self._constant_field, names = self._all_gens_names[-1]) #the last one will be gone due to affanization

    def function_field_of_curve(self, curve_ideal, projective_coordinate=True):
        """
        compute the function field of the projective curve_ideal as non-composite extension
        of the rational_function_field

        example:

        if the ambient_space has variable [a,b,c,d] then d is eliminated through
        projective to affine setting d = 1, then c would be considered to be the
        rational function field var. a will be eliminated using lex grobner basis,
        and the polynomial in b and c is used to extend the the rational function field.
        """
        ambient_space = curve_ideal.ring()
        #if the coordinate is projective, we get rid of one variable
        if projective_coordinate:
            ambient_space = PolynomialRing(ambient_space.base_ring(), ambient_space.gens()[:-1], order='lex')
            self._var_map = ambient_space.gens()+(1,)
            curve_ideal = ambient_space.ideal([p(self._var_map) for p in curve_ideal.gens()])
            deprojection_map = [1]
        else:
            deprojection_map = []

        curve_groebner_basis = curve_ideal.groebner_basis()

        cur_poly_map = [self._rational_function_field.gen()]
        cur_function_field = self._rational_function_field
        function_field_tower = [cur_function_field]

        funcfield_embed_auto_maps = [] #funcfield_embed_auto_maps[j] maps j'th generator image in j+1 field.
        for i in range(len(curve_groebner_basis)-1, -1, -1):
            cur_poly = curve_groebner_basis[i]
            cur_mapped_min_poly = self._map_min_poly_into_tower(cur_function_field, cur_poly_map, cur_poly)
            if (cur_mapped_min_poly):
                ext_function_field = cur_function_field.extension(cur_mapped_min_poly)
                cur_poly_map = [ext_function_field.gen()] + cur_poly_map
                
                #we need to simplify the tower so we can define the 
                #automorphism

                funcfield_embed_auto_maps.append(ext_function_field.hom((ext_function_field.gen(),ext_function_field(cur_function_field.gen()))))

                function_field_tower.append(ext_function_field)
                cur_function_field = ext_function_field

        #map all the image of the variable to the top level function field
        for tower_level in range(0, len(function_field_tower)-1):
            #Embedding up the generator up into the tower one field at the time
            var_no = len(function_field_tower) - tower_level - 1
            cur_var_img = cur_poly_map[var_no]
            for j in range(tower_level+1, len(function_field_tower)-1):
                cur_var_img = funcfield_embed_auto_maps[j](cur_var_img)

            cur_poly_map[var_no] = cur_var_img

        return function_field_tower, cur_poly_map + deprojection_map

        # self._var_map = [0]*self._no_eliminated_var
        # self._var_map.extend(R.gens())
        # self._var_map += [self._rational_function_field.gen(), 1]

        # R = PolynomialRing(self._rational_function_field, P_genus.gens[-3])

        # #Now we starting with the function field of the projected curve
        # #And extend it one equation at the time
        # cur_cuv_poly = self._gbase[-1]
        # cur_func_field = self._rational_function_field
        # for var_selector in range(0, P_genus.gens() - 2):
        #     ext_func_field = cur_func_field.extend(self._gbase[-var_selector-1](self._var_map)

        # #get the 2 dim equation in projective space
        # self._P2_equation = self._gbase[-1]
        # self._no_eliminated_var = len(P_genus.gens())-3
        # self._all_gens_names = (names == None) and ['y','x'] or names

        # self._global_var_count = 0
        # self._longest_cycle_gen = self._abelian_model[0]
        # quot_sig_grp, big_cycle_sub_field = self._generate_signatured_subfield(self._longest_cycle_gen)

        # cur_signatured_field = self._rational_function_field.extension(self._map_min_poly_into_tower(quot_sig_grp, big_cycle_sub_field, self._rational_function_field))

        # for g in self._abelian_model:
        #     if g is not self._longest_cycle_gen:
        #         cur_subfield = self._generate_signatured_subfield(g)
        #         embeded_ext_poly = self._map_poly_into_tower(cur_subfield, cur_signatured_field)

        #         live_factors = [cur_factor[0] for cur_factor in embeded_ext_poly.factor()]
        #         if (live_factors[0].degree() > 1): #Otherwise nothing to do
        #             ext_field = cur_signatured_field.extension(live_factors[0])
        #             (cur_signatured_field, to_sim, from_sim) = ext_field.make_simple()

        # set_trace()
        # FunctionField_polymod.__init__(self, cur_signatured_field.polynomial(), names=(self._all_gens_names[0],))

    def _map_min_poly_into_tower(self, cur_function_field, cur_poly_map, cur_poly):
        """
        Get a field over the self.rational_function_field and map
        the cur_poly of ambient space to a polynomial over the former
        field using tower of maps for every variables

        as we proceed with lex order, the cur_poly_map is used to map
        the last bunch of variable in P_genus to cur_function_field,
        there should be one variable remaining to map to
        PolynomialRing(cur_function_field) and the rest are mapped to zero
        """
        #We assume for every variable that we can't map then it should be set zero
        #that fails when the ideal isn't for a curve
        set_of_gens = cur_poly.parent().gens()

        #find which generator we need to tackle
        just_verify = False
        if (len(set_of_gens) > len(cur_poly_map)):
            extending_gen = set_of_gens[len(set_of_gens) - len(cur_poly_map) - 1]
        else:
            just_verify = True
            extending_gen = set_of_gens[0]
        #we need to make sure that current polynomial is dependant on the extending
        #gen
        if (not extending_gen in cur_poly.variables()):
            extending_gen = set_of_gens[len(set_of_gens) - len(cur_poly_map)] #try the last variable, we should not be able to extend and the factor should of degree 1
            just_verify = True

        R  = PolynomialRing(cur_function_field, extending_gen)
        # This fails silently if ideal isn't a curve
        if (not just_verify):
            new_poly_map = [0]*(len(set_of_gens)-len(cur_poly_map)-1)+[extending_gen]+cur_poly_map
        else:
            new_poly_map = [0]*(len(set_of_gens)-len(cur_poly_map)) + [extending_gen]+ cur_poly_map[1:]
            ext_min_poly = R(cur_poly(new_poly_map))
            assert(ext_min_poly(R.base_ring().gen())==0)
            return None

        ext_min_poly = R(cur_poly(new_poly_map))

        #this might bite me back, it is problem with the factorization implemementation
        #it could be that if you can't factor just return the polynomial with exception
        #handling
        #however, the ultimate solution is to colapse before extending 
        #further
        try:
            if not ext_min_poly.is_irreducible():
                print "intermediate grobener extension poly is not irreducible: %s"%ext_min_poly
                #set_trace()
                ext_min_poly = ext_min_poly.factor()[0][0]
        except TypeError:
            print "Failed to factor!!"
            pass
        return ext_min_poly

    # def _map_min_poly_into_tower(self, qout_sig_group,sub_sig_field, cur_tower_field):
    #     """
    #     Get a field with over the self.rational_function_field and map
    #     the min_poly of the sub_sig_field to a polynomial over the former
    #     field, keeps track of ramified places to keep the automorphism
    #     structure
    #     """
    #     set_trace()

    #     hom_sub_2_tower = self._compute_hom_subfield_2_tower(qout_sig_group, sub_sig_field)
    #     extending_polynomial = sub_sig_field.polynomial()
    #     extend_polyring = PolynomialRing(cur_tower_field, extending_polynomial.parent().gen.variable_name())

    #     return cur_tower_poly_ring([hom_sub_2_tower(cur_coeff) for cur_coeff in list(exteding_polynomial)])

    # def _compute_hom_subfield_2_tower(self, quot_sig_group, sub_sig_field):
    #     """
    #     The hardest part of mapping is mapping of rational parameters
    #     We use the following method:
    #       - Keep track of which generator in F is mapped to the inertia
    #       generator in F_q_i which is associated to the parametrs in the
    #       rational field. then map that parameter to the assoicated
    #       parameter in rational field of F.

    #     Only one way of the computation is required.
    #     """
    #     #We assume that we can map the constants, we only need to map
    #     #the parameters then.

    #     gen_inertia_seq = [cur_sig_grp.gen() for cur_sig_grp in self._versweigun_gruppen[PURELY_RAM_UPSTAIRS]]

    #     gen_inertia_im = []
    #     for cur_red_inertia_grp in sub_sig_field._versweigun_gruppen[PURELY_RAM_UPSTAIRS]:
    #         cur_red_inertia_gen = cur_red_inertia_grp.gen()
    #         for i in range(0, len(gen_inertia_seq)):
    #             if quot_sig_group._natural_ext_2_gap_perm.Image(gen_inertia_seq[i]) == cur_red_inertia_gen:
    #                 gen_inertia_im.append(self._rational_function_field.constant_field().gens()[i])

    #     return sub_sig_field.rational_function_field().hom(self._rational_function_field.gen(), sub_sig_field.rational_function_field().constant_field().hom(gen_inertia_im))
    #     #we check which inertia group generator maps to the generator
    #     #in subfield and then we define the rule.

    def _compute_polynomial(self):
        """
        should be implemented by appropriate subclass
        """
        # self.function_field_of_curve(self._canonical_ideal)

        # R = PolynomialRing(self._rational_function_field, names =  self._all_gens_names[0])
        # return self._gbase[-1](self._var_map)
        pass

    def check_structure_validity(self):
        """
        Hurwitz:
        (g - 1) - 1/2*sum(signatured_group.sub_signatures.order() - 1) + q
        == 0
        """
        #((self._genus - 1) - 1/2*(sum([cur_ram.order() -1 for cur_ram in self._signatured_group])) + self._rationalizing_subgroup.order()) == 0
        #this is done by the sub_signature module so we don't need to        #repeat it
        return self._sub_signature[0] == 0
