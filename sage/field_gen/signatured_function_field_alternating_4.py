"""
Kummer extension Function Fields of deg q with G/C_q = A_4

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
from pdb import set_trace
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.groups.perm_gps.permgroup_named import DihedralGroup
from sage.misc.misc_c import prod

from signatured_function_field import SignaturedFunctionField

class SignaturedFunctionFieldAlternatvie4(SignaturedFunctionField) :
    """
    Covering all cases with altenatvie A4 group as factor group
    Simply calling the parent init, for most cases this should be enough
    as the compute_function_field is overriden
    """
    def _compute_fundamental_values(self):
        """
        This to compute the values needed in Brandt's thesis to 
        compute the equation specific to this family.
        """
        #For the sake of following Brandt's naming
        SignaturedFunctionField._compute_fundamental_values(self)

        self._middle_fixed_group_order = 4

    def _compute_number_of_parameters(self):
        """
        To construct the appropriate rational function field which is 
        needed to represent a general function field with given
        automorphism group with a4 as quotient, we need to know
        how many parameters we need.
        
        We also need to extend our constants if necessary
        """
        self.no_of_params = len(self._purely_kummer_ramified)

        self._var_str = ['a'+str(i) for i in range(0, self.no_of_params)]

        #we also need to add i and sqrt(3) to the constant field
        #then we make the polynomial ring we works on.
        aux_ring = PolynomialRing(self._constant_field, names = 'T')
        T = aux_ring.gen()
        self._extend_constant_field([T**2 + 1, T**2-3])

        #also we need to make choice for i and sqrt and we need to be 
        #consistant all the way long
        self._zeta_4 = self._constant_field.multiplicative_generator()**((self._constant_field.order()-1)/4)
        self._sqrt_3 = self._constant_field(3).sqrt()

    def _compute_kummer_factors(self):
        #we put all of them on power 1 because repeated factors means
        #singularity in kummer extensions and doesn't  change the 
        #genus, so why should I just make the matter more complicated

        #infinity is always as ramified as place (x) so we add 1 at the end
        self.kummer_factors = [self._rational_function_field.gen()**5-self._rational_function_field.gen() , self._rational_function_field.gen()**self._middle_fixed_group_order - 2*self._zeta_4*self._sqrt_3*self._rational_function_field.gen()**2+ 1, self._rational_function_field.gen()**self._middle_fixed_group_order + 2*self._zeta_4*self._sqrt_3*self._rational_function_field.gen()**2+ 1, self._rational_function_field(1)]

        #set the power of infinity so it ramifies as much as (x) ramifies
        self._kummer_factor_powers = self._kummer_factor_powers[:3]+[self._kummer_factor_powers[0]]+ self._kummer_factor_powers[3:]

        #purely kummers
        for cur_b in  self._params:
            b = [cur_b.parameter] #b_1
            b.append((2*b[0] + 12)/(2-b[0])) #b_2
            b.append((2*b[0] - 12)/(2+b[0])) #b_3
            
            cur_factor = prod([self._rational_function_field.gen()**self._middle_fixed_group_order - bjr*self._rational_function_field.gen()**2+1 for bjr in b])
            self.kummer_factors.append(cur_factor)
        
    def _quotient_automorphism_generators(self):
        """
        This is to be overloaded. The generators of the restricted
        automorphism group to the base field need to be hard coded

        In case of dihedral quotient and large prime we always have

        x -> \eta x where eta is n'th root of unity
        x -> 1/x

        OUTPUT::
            Returns [-x, i(x+1)/(x-1)]
        """
        crude_aut_imgs = []
        #first automorphism x -> -x
        crude_aut_imgs.append((self.gen(),-1 * self._rational_function_field.gen()))

        #second auto x -> 1/x
        #First we need to generate the root of unity that we need
        zeta_4 = self._primitive_nth_root_of_unity(4)
        r = (1/self._kummer_ext_deg)*(self._kummer_rational_side.numerator().degree())
        crude_aut_imgs.append((self.gen()/((self._rational_function_field.gen()-1)**r),zeta_4*(self._rational_function_field.gen()+1)/(self._rational_function_field.gen()-1)) )

        #cook the constant
        return [(cur_auto[0]*self._compute_y_img_constant(cur_auto[0], cur_auto[1]), self(cur_auto[1])) for cur_auto in crude_aut_imgs]

    @classmethod
    def have_compatible_structure_for(cls, signatured_group, rationalizing_subgroup):
        """
        We assume that the validity test has been done. So we only need to 
        check if the quotient is cyclic 
        """
        if not(rationalizing_subgroup.is_cyclic() and  signatured_group.quotient(rationalizing_subgroup).group_id() == [12,3]):
            return False

        #direct produdct check
        return cls.is_direct_product_extension(signatured_group, rationalizing_subgroup)
