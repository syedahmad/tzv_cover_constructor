﻿"""
Signatured Group

AUTHOR:

- Syed Ahmad Lavasani (2012)


"""
from operator import itemgetter #need it for sorting

from sage.groups.perm_gps.permgroup import PermutationGroup_generic, PermutationGroup

from pdb import set_trace

class SignaturedGroup(PermutationGroup_generic):
    r"""
    This is a group that also contains a signature.

    INPUT::
    group: a permutation group
    signature: a list of subgroups of group
    """
    def __init__(self, genus, group, signature):
        """
        """
        PermutationGroup_generic.__init__(self,group.gens())
        #First we need to verifiy the signature
        assert(all([C.is_subgroup(self) for C in signature]))
        self._signature = signature
        self._genus = genus

        self._signature_orders = [cur_auto.order() for cur_auto in self._signature]

    def genus(self):
        """
        Simple interface to retrieve the genus
        """
        return self._genus

    def signature(self):
        """
        Simple interface to retrieve the signature
        """
        return self._signature

    def signature_orders(self):
        """
        A convinient function that returns the order of each automorphism
        instead of automorphism themselves. We only needs the orders most
        of the time
        """
        return self._signature_orders

    def signature_element_indices(self):
        """
        return the index of each signature element in the sage permutation group.
        that is a way to store the signature as an string in a way that one can
        reconstruct the signature from the db as well.
        """
        sig_element_indices = []
        for cur_sig_element in self._signature:
            for i in range(0, self.order()):
                if (cur_sig_element.gen() == self[i]): #knowing the sig groups are cyclic
                    sig_element_indices.append(i)
                    break

        return sig_element_indices

    def sub_signature(self, fixing_subgroup):
        """
        Returns taking a subgroup H of G returns the signature of H as aut
        grp of the same funcfield over F(C)^H.
        """
        #first we assert the given object is a subgroup of self
        assert fixing_subgroup.is_subgroup(self)
        #now we are using Shask's method to compute the new signature.
        #we take every conjugacy subgroup.
        fix_signature = []
        for cur_conj_class in self._signature:
            #we need to compute the double coset cur_conj_class\self/fixing_subgroup
            #for that we need to go to gap and come back to sage
            double_coset_reps = self._gap_().DoubleCosetRepsAndSizes(cur_conj_class._gap_(), fixing_subgroup._gap_())

            #now we need to check for each element of double coset sigma if
            #for which m we have sigma^-1 gamma sigma \in fixing_subgroup
            gamma = cur_conj_class.gen(); #assuming no wild ramification
            for cur_coset in double_coset_reps:
                sigma = cur_coset[1]
                for m in range(1, gamma.order()):
                    cur_elm = (sigma**(-1))*(gamma**m)*sigma
                    if (cur_elm != self.identity() and cur_elm in fixing_subgroup):
                        fix_signature.append(fixing_subgroup.subgroup([cur_elm]))
                        break

        fix_genus =int(1.0/(fixing_subgroup.order())* (self._genus - 1 - sum([fixing_subgroup.order() - fixing_subgroup.order()/cur_inertia.order() for cur_inertia in fix_signature])/2.0)+1)

        return (fix_genus, fix_signature)

    def Versweiguntyp_of(self, fixing_subgroup):
        """
        In Brandt's definition Versweiguntyp is total ramification of
        places that are ramified between F_0/F_1 and the order is also
        about how much of ramification is happening downstair the more
        ramified comes erlier.

        so we need a more sophisticated method of computing sub signature

         F    P
         |    |
        F_0  P_0
         |    |
        F_1  P_1

        we have e(P/P_1) in the signature and we need to copy that later
        but we need to compute e(P_0/P_1) = e(P/P_1)/e(P/P_0) at least
        as long as we deal with Galois extensions.
        So in Galois extensinos when we take the inertia group of P_1 and
        we see that its break into P_0 inertia group we hope that those
        group are all of the same size of e(P/P_0). The we divid e(P/P_1)
        by this value and we re-arrange e(P/P_0) in e(P_0/P_1) order kicking
        those with e(P_0/P_1) = 1 out.

        We also compute two more values: reduzierte Verzweigungstyp, which
        is ramification index of e(P_0,P_1). Also purely kummer ramified, for
        those who don't show up in Verzweigungstyp. These are necessary
        for the generalization of the construction to prime powers

        meanwhile we store an inertia group corresponding to each ramified
        place downstair (a rep of conjugacy class of those groups) in a
        parallel array called self._Versweigungroups

        self._Versweigungruppen: tuple of two lists, first list contains
            the inertia group reps of places ramified downstairs the second is
            for purely kummer places
        """
        #we need fixing_subgroup be a normal subgroup
        assert fixing_subgroup.is_normal(self)
        #defining some constants so the code is more readable:
        DOWNSTAIRS_RAM = 0
        TOTAL_RAM = 1
        INERTIA_GROUP = 2
        #now we are using Shask's method to compute the new signature.
        #we take every conjugacy subgroup.
        verweigung_gruppen_down = []
        verweigung_gruppen_purely_kummer = []
        Versweiguntyp_data = []
        for cur_conj_class in self._signature:
            #each element of Versweigunty_data is a list of pair, the first
            #coordinate is \bar{e}_i=e(P_0/P), the second is e_i = e(P/P_1)
            #we will sort using the first as key

            #we need to compute the double coset :
            #cur_conj_class\self/fixing_subgroup
            #for that we need to go to gap and come back to sage
            double_coset_reps = self._gap_().DoubleCosetRepsAndSizes(cur_conj_class._gap_(), fixing_subgroup._gap_())

            #in real life we need to check only for one element of double
            # coset sigma if for which m we have sigma^-1 gamma sigma
            #\in fixing_subgroup. Because fixing_subgroup is normal all of
            # them should have the same order but for now to test my theory
            # in real life I'm going to compute all and check if they are
            # equal
            cur_upstairs_orders = []
            gamma = cur_conj_class.gen(); #assuming no wild ramification
            for cur_coset in double_coset_reps:
                sigma = cur_coset[1]
                for m in range(1, gamma.order()+1): #here we care about identity
                    cur_elm = (sigma**(-1))*(gamma**m)*sigma
                    #in this setting we like identity as its mean totally
                    #ramified downstairs
                    if ( cur_elm in fixing_subgroup):
                        cur_upstairs_orders.append(cur_elm.Order()._sage_())
                        break

            #just to check my theory
            assert(all([each_order == cur_upstairs_orders[0] for each_order in cur_upstairs_orders]))

            # I had written this but I don't know why and doesn't make sense
            #(not gamma.order()/cur_upstairs_orders[0] == 1) and
            Versweiguntyp_data.append((gamma.order()/cur_upstairs_orders[0], gamma.order(), cur_conj_class))

        Versweiguntyp_data = sorted(Versweiguntyp_data, key=itemgetter(0))

        #storing the inertia group conjugacy reps for later use */
        #([ramified downstairs], [purely ramified upstairs])
        Versweigun_gruppen = ([cur_versweigun[INERTIA_GROUP] for cur_versweigun in Versweiguntyp_data if not cur_versweigun[DOWNSTAIRS_RAM] == 1], [cur_versweigun[INERTIA_GROUP] for cur_versweigun in Versweiguntyp_data if cur_versweigun[DOWNSTAIRS_RAM] == 1])

        reduize_Versweiguntyp = [cur_versweigun[DOWNSTAIRS_RAM] for cur_versweigun in Versweiguntyp_data if not cur_versweigun[DOWNSTAIRS_RAM] == 1]
        Versweiguntyp = [cur_versweigun[TOTAL_RAM] for cur_versweigun in Versweiguntyp_data if not cur_versweigun[DOWNSTAIRS_RAM] == 1]
        purely_upstairs = [cur_versweigun[TOTAL_RAM] for cur_versweigun in Versweiguntyp_data if cur_versweigun[DOWNSTAIRS_RAM] == 1]

         #(reduize Versweiguntyp, Versweiguntyp, purely kummer ramfied)
        return (reduize_Versweiguntyp, Versweiguntyp, purely_upstairs, Versweigun_gruppen)

    def signatured_quotient(self, normal_fixing_subgroup):
        """
        Simply create a SignaturedQuotientGroup and return it
        """
        return SignaturedQuotientGroup(self, normal_fixing_subgroup)

    def is_central_extension_of(self, cyclic_subgroup):
        """
        Checks if cyclic_subgroup is in the centre of our group and
        if G/cyclic_subgroup extending the cyclic_subgroup to G
        this is that G/cyclic_subgroup semi_direct_product cyclic_subgroup =
        G
        """
        #as far as I understand the only obstacle is that F^C is rational or not
        # which makes sense. I also need to takle the central restriction.

        # we need to compute the new genus. this is using the hurwitz

        #The reality is that being a normal subgroup is enough, we don't
        #really need to be in the center but for the correctness of the
        #function name I'll put his condition, later we can write a function
        #with less restriction
        # The big deal of being in center is that computing the extension of
        # of automorphism is easier.
        if not cyclic_subgroup.is_cyclic() or not cyclic_subgroup in self.center().subgroups():
            return False

        cyclic_sig = self.sub_signature(cyclic_subgroup)

        return (cyclic_sig[0] == 0)

    def all_cyclic_central_extensions(self):
        """
        Iterates over all cyclic subgroup of the centre and for each
        cyclic subgroup check if it is a central extension of that subgroup
        in that case returns that subgroup with its sub_signature
        """
        all_cyclic_central_subgroups = [ (cur_subgroup, cur_subgroup.order()) for cur_subgroup in self.center().subgroups() if cur_subgroup.is_cyclic()]
        #getting the biggest rational subfield possible
        all_cyclic_central_subgroups.sort(key=itemgetter(1))

        for cur_subgroup in all_cyclic_central_subgroups:
            cur_sig = self.sub_signature(cur_subgroup[0])
            if cur_sig[0] == 0 :
                return (cur_subgroup[0], cur_sig[0], cur_sig[1])

        return None

    def all_cyclic_normal_extensions(self):
        """
        Iterates over all cyclic normal subgroup  and for each
        cyclic subgroup check if it has a rational fixed field.

        OUTPUT:: In case found, returns such a subgroup with their sub_signature
        """
        for cur_subgroup in self.normal_subgroups():
            if cur_subgroup.is_cyclic():
                cur_sig = self.sub_signature(cur_subgroup)
                if cur_sig[0] == 0 :
                    return (cur_subgroup, cur_sig[0], cur_sig[1])

        return None


    def all_cyclic_extensions(self):
        """
        Iterates over all cyclic subgroup of the group and for each
        cyclic subgroup check if it is a extension of that subgroup
        in that case returns that subgroup with its sub_signature

        This is because I have relaxed the condition that Br88
        require for creating the equation. cause I compute the
        automorphisms differently and I don't require g(y) \in k(y)
        """
        for cur_subgroup in self.subgroups():
            if cur_subgroup.is_cyclic():
                cur_sig = self.sub_signature(cur_subgroup)
                if cur_sig[0] == 0 :
                    return (cur_subgroup, cur_sig[0], cur_sig[1])

        return None

    def function_field(self):
        """
        Assumes that it is an automorphism group of a function field, then
        using the Brandt database it tries to find an equation for that
        function field, if succeeds, returns the signatured function field
        generated by that equation.
        """
        sub_sig = all_cyc_central_extention()
        if sub_sig:
            return sub_sig
        #self.group_id()

    def normally_decomposable(self, abl_sub_grp):
        """
        Checks if the abl_sub_grp is a ablian normal subgroup of self and
        if it is, then tries to decompose it into cyclic component each,
        normal subgroup of the self.

        INPUT::
            abl_sub_grp:  An abelian subgroup of self.

            returns a normal decomposition of abl_sub_grp or None in case of
            failure.
        """
        if not abl_sub_grp.is_normal(self) or not abl_sub_grp.is_abelian():
            verbose("%s supposed to be a normal belian"%abl_sub_grp);
            return None

        all_normal_list = self.normal_subgroups()
        subsub_normalcyclic_list = [cur_subgrp for cur_subgrp in all_normal_list if (cur_subgrp.is_subgroup(abl_sub_grp) and cur_subgrp.is_cyclic() and not cur_subgrp.order() == 1)]
        subsub_normalcyclic_list = sorted([(subgrp, subgrp.order()) for subgrp in subsub_normalcyclic_list], key=itemgetter(1), reverse=True)
        #just for efficiency
        element_selector = [1 << i for i in range(0, len(subsub_normalcyclic_list))]

        #iterate over all possible subsets
        from sage.misc.misc_c import prod
        for i in range(1, 2**len(subsub_normalcyclic_list)):
            if prod([(i & element_selector[j]) and subsub_normalcyclic_list[j][1] or 1 for j in range(0, len(subsub_normalcyclic_list))]) != abl_sub_grp.order():
                continue

            subset_of_subgrps = [subsub_normalcyclic_list[j][0] for j in range(0, len(subsub_normalcyclic_list)) if (i & element_selector[j])]

            #intersect mutually only at identity.
            mutually_exculsive = True
            for j in range(0, len(subset_of_subgrps)):
                for k in range(j+1, len(subset_of_subgrps)):
                    if len(subset_of_subgrps[j].intersection(subset_of_subgrps[k])) != 1:
                        mutually_exculsive = False
                        break

                if not mutually_exculsive:
                    break

            if not mutually_exculsive:
                continue

            #necessary and sufficient for dircet product
            return  subset_of_subgrps

        #If we reach here is not noramlly decomposable
        return None

class SignaturedQuotientGroup(SignaturedGroup):
    """
    This is basically a signatured group with the natural homomorphism that
    maps element of the SignaturedGroup to its quotients
    """
    def __init__(self, extension_group, normal_fixing_subgroup):
        """
        Computes the natural homoromphism using gap and creates the quotient
        and its signature
        The idea is pretty simple. The inertial group generator is the generator
        of the of those don't move P' and hence the middle P'' the only problem
        is that the order of that generator is smaller now because the original
        generator isn't in the group any more. So that it:

        Take the inertia gen. See what's its order mod H, takes that as the new
        generator, if you got identity discard.
        """
        assert(normal_fixing_subgroup.is_normal())
        self._extension_group = extension_group
        natural_ext_2_quo = self._extension_group._gap_().NaturalHomomorphismByNormalSubgroup(normal_fixing_subgroup)

        gap_quotient_group = natural_ext_2_quo.Range()
        gap_perm_group_iso = gap_quotient_group.IsomorphismPermGroup()
        gap_perm_group = gap_perm_group_iso.Range()

        self._natural_ext_2_gap_perm = gap_perm_group_iso.CompositionMapping(natural_ext_2_quo)
        perm_quotient_group = PermutationGroup(gap_perm_group.GeneratorsOfGroup())

        quotient_signature = []
        self._super_inertia_gens = [] #keeping track of original interia gens
             #as way of tracking the places between different fields
        for cur_inertia_subgrp in self._extension_group._signature:
            #We know that the inertia is cyclic
            reduced_inertia_gen = self._natural_ext_2_gap_perm.Image(cur_inertia_subgrp.gen())
            if not reduced_inertia_gen == gap_perm_group.Identity():
                quotient_signature.append(perm_quotient_group.subgroup([reduced_inertia_gen]))
                self._super_inertia_gens = cur_inertia_subgrp.gen()

        fix_genus = self._extension_group.sub_signature(normal_fixing_subgroup)[0]

        SignaturedGroup.__init__(self, fix_genus, perm_quotient_group, quotient_signature)

    def natural_homomorphism(self, ext_group_elm):
        """
        Is the natural homomorphism from the extension group to the
        quotient group.
        """
        assert(ext_group_elm in self._extension_group)
        return self(self._natural_ext_2_gap_perm.Image((ext_group_elm)))
