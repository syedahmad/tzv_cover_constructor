import argparse
parser = argparse.ArgumentParser("generates function fields of signatures and output their vulnerable subfields")
group = parser.add_mutually_exclusive_group()
group.add_argument("-k", "--cyclic", action="store_true", help="central Kummer extension construction")
group.add_argument("-c", "--canonical", action="store_true", help="curve equation is given in canonical form")
parser.add_argument('filenames', nargs='+',
                                       help='the filenames which contains the signature of the curve(s)')
args = parser.parse_args()
if args.cyclic == True:
    print "cyclic", args.filenames
else:
    print "canonical", args.filenames
