"""
Kummer extension Function Fields of deg q with G/C_q = S_4

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
from pdb import set_trace
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.groups.perm_gps.permgroup_named import DihedralGroup
from sage.misc.misc_c import prod

from signatured_function_field import SignaturedFunctionField

class SignaturedFunctionFieldSymmetric4(SignaturedFunctionField) :
    """
    Covering all cases with S4 group as factor group
    Simply calling the parent init, for most cases this should be enough
    as the compute_function_field is overriden
    """
    def _compute_fundamental_values(self):
        """
        This to compute the values needed in Brandt's thesis to
        compute the equation specific to this family.
        """
        #For the sake of following Brandt's naming
        SignaturedFunctionField._compute_fundamental_values(self)

        self._middle_fixed_group_order = 12 #A4

    def _compute_number_of_parameters(self):
        """
        To construct the appropriate rational function field which is
        needed to represent a general function field with given
        automorphism group with a4 as quotient, we need to know
        how many parameters we need.

        We also need to extend our constants if necessary
        """
        self.no_of_params = len(self._purely_kummer_ramified)

        self._var_str = ['a'+str(i) for i in range(0, self.no_of_params)]

    def _compute_kummer_factors(self):
        #we put all of them on power 1 because repeated factors means
        #singularity in kummer extensions and doesn't  change the
        #genus, so why should I just make the matter more complicated

        #infinity is always as ramified as place (x) so we add 1 at the end
        self.kummer_factors = [(self._rational_function_field.gen()**4+1)*(self._rational_function_field.gen()**8 - 34*self._rational_function_field.gen()**4+ 1), self._rational_function_field.gen()**8 + 14*self._rational_function_field.gen()**4+ 1 , self._rational_function_field.gen()**5-self._rational_function_field.gen(), self._rational_function_field(1)]

        #set the power of infinity so it ramifies as much as (x) ramifies
        self._kummer_factor_powers = self._kummer_factor_powers[:3]+[self._kummer_factor_powers[2]]+ self._kummer_factor_powers[3:]

        #purely kummers
        self.kummer_factors.extend([(self._rational_function_field.gen()**8 + 14*self._rational_function_field.gen()**4+ 1)**3 - cur_a.parameter *(self._rational_function_field.gen()**5-self._rational_function_field.gen())**4 for cur_a in self._params])

    def _quotient_automorphism_generators(self):
        """
        This is to be overloaded. The generators of the restricted
        automorphism group to the base field need to be hard coded

        In case of dihedral quotient and large prime we always have

        x -> \eta x where eta is n'th root of unity
        x -> 1/x

        OUTPUT::
            Returns [-x, i(x+1)/(x-1)]
        """
        crude_aut_imgs = []
        #first automorphism x -> -x
        #First we need to generate the root of unity that we need
        zeta_4 = self._primitive_nth_root_of_unity(4)

        crude_aut_imgs.append((self.gen(),-zeta_4 * self._rational_function_field.gen()))

        #second auto x -> 1/x
        r = (1/self._kummer_ext_deg)*(self._kummer_rational_side.numerator().degree())
        crude_aut_imgs.append((self.gen()/((self._rational_function_field.gen()-1)**r),zeta_4*(self._rational_function_field.gen()+1)/(self._rational_function_field.gen()-1)) )

        #cook the constant
        return [(cur_auto[0]*self._compute_y_img_constant(cur_auto[0], cur_auto[1]), self(cur_auto[1])) for cur_auto in crude_aut_imgs]

    @classmethod
    def have_compatible_structure_for(cls, signatured_group, rationalizing_subgroup):
        """
        We assume that the validity test has been done. So we only need to
        check if the quotient is cyclic
        """
        if not(rationalizing_subgroup.is_cyclic() and  signatured_group.quotient(rationalizing_subgroup).group_id() == [24, 12]):
            return False

        #direct produdct check
        return cls.is_direct_product_extension(signatured_group, rationalizing_subgroup) or signatured_group.is_central_extension_of(rationalizing_subgroup)
