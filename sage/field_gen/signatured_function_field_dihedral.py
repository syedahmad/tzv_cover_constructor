"""
Kummer extension Function Fields of deg q with G/C_q = D_2n

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
from pdb import set_trace
from sage.rings.function_field.constructor import FunctionField
from sage.rings.arith import is_prime, gcd
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.groups.perm_gps.permgroup_named import DihedralGroup
from sage.misc.misc_c import prod
from sage.misc.functional import is_even
from sage.functions.log import log

from signatured_function_field import SignaturedFunctionField

class SignaturedFunctionFieldDihedral(SignaturedFunctionField) :
    """
    Covering Brandt TypIV, TypV and TypVI
    Simply calling the parent init, for most cases this should be enough
    as the compute_function_field is overriden
    """
    def _compute_fundamental_values(self):
        """
        Computing values unique to dihedral case after calling the
        parent class instance for computing general fundamental values
        """
        SignaturedFunctionField._compute_fundamental_values(self)

        self._hedral_order = self._quotient_group_order / 2

    #Ahmad 2014: I don't see any difference between this function and the one in
    # def _compute_number_of_parameters(self):
    #     """
    #     Construct the appropriate rational function field that is
    #     needed to represent a general function field with given
    #     automorphism group with dihedral quotient
    #     """
    #     # There a three different cases that are relavant to our
    #     # case:
    #     # hyperelliptics
    #     #   q = 2 (qn, p) = 1 und n = 0 (mod 2)
    #     # non-hyperellitptic
    #     #   q >= 3 (qn,p) = 1 und p /= 2
    #     # all seems essentially similar
    #     #no_of_kummer_ram_ps = len(self._sub_signature[1])
    #     #nno_of_remaining_places = no_of_kummer_ram_ps - self._v_0 - self._hedral_order * (self._v_1 + self._v_2) - self._ramified_at_infinity
    #     #each block has power 2*hedral


    #     #the parent class so I'm decomissining it
    #     self.no_of_params = len(self._purely_kummer_ramified)

    #     self._var_str = ['a'+str(i) for i in range(0, self.no_of_params)]

    def _compute_kummer_factors(self):
        #the infinite place and (x) have same ramification but we
        #need to add infinity ramification into consideration by
        #adding a power to kummer_factor_powers
        self.c_x_factor_index = 2
        self.c_infinity_factor_index = 3
        self.kummer_factors = [self._rational_function_field.gen()**self._hedral_order-1, self._rational_function_field.gen()**self._hedral_order+1,self._rational_function_field.gen(), self._rational_function_field(1)]

        self._kummer_factor_powers = self._kummer_factor_powers[:3]+[self._kummer_factor_powers[2]]+ self._kummer_factor_powers[3:]
        #purely kummer
        self.kummer_factors.extend([self._rational_function_field.gen()**(2*self._hedral_order) - cur_param.parameter*self._rational_function_field.gen()**self._hedral_order + 1 for cur_param in self._params])


    def _quotient_automorphism_generators(self):
        """
        This is to be overloaded. The generators of the restricted
        automorphism group to the base field need to be hard coded

        In case of dihedral quotient and large prime we always have

        x -> \eta x where eta is n'th root of unity
        x -> 1/x

        OUTPUT::
            Returns [\eta x, 1/x]
        """
        crude_aut_imgs = []
        #first automorphism x -> \eta x
        #First we need to generate the root of unity that we need
        zeta_n = self._primitive_nth_root_of_unity(self._hedral_order)
        crude_aut_imgs.append((self.gen(),zeta_n * self._rational_function_field.gen()))

        #second auto x -> 1/x
        r = (1/self._kummer_ext_deg)*(self._kummer_rational_side.numerator().degree() + self._kummer_factor_powers[self.c_x_factor_index]) #x power should stay the same, as the result of morphism we'll have every factor remains the same at the top the only x is replaced by 1 so we need to compensate for everything + give back enough x power this however require that 2x_power + otherpowers = 0 mod q exactly in Br88 condition
        



        crude_aut_imgs.append((self.gen()/self._rational_function_field.gen()**r,1/self._rational_function_field.gen()) )

        #cook the constant
        return [(cur_auto[0]*self._compute_y_img_constant(cur_auto[0], cur_auto[1]), self(cur_auto[1])) for cur_auto in crude_aut_imgs]

    @classmethod
    def have_compatible_structure_for(cls, signatured_group, rationalizing_subgroup):
        """
        We assume that the validity test has been done. So we only need to
        check if the quotient is cyclic
        """
        if not(rationalizing_subgroup.is_cyclic() and is_even(signatured_group.order()/ rationalizing_subgroup.order())  and signatured_group.quotient(rationalizing_subgroup).is_isomorphic(DihedralGroup((signatured_group.order()/ rationalizing_subgroup.order())/2))):
            return False

        #direct produdct check
        return cls.is_direct_product_extension(signatured_group, rationalizing_subgroup) or signatured_group.is_central_extension_of(rationalizing_subgroup)
