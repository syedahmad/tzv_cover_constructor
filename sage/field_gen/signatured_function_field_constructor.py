r"""
Factories to construct Function Fields

AUTHORS:

    - Syed Ahmad Lavasani (2012): Initial version

EXAMPLES::

    sage: K.<x> = FunctionField(QQ); K
    Rational function field in x over Rational Field
    sage: L.<x> = FunctionField(QQ); L
    Rational function field in x over Rational Field
    sage: K is L
    True
"""
#****************************************************************************
#       Copyright (C) 2012 Syed Ahmad Lavasani <syd.lavasani@gmail.com>
#
#  Distributed under the terms of the GNU General Public License (GPL)
#  as published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.structure.factory import UniqueFactory

from pdb import set_trace

class SignaturedFunctionFieldFactory(UniqueFactory):
    """
    Create a function field defined by a automorphism group and pre-defined
    ramification structure.
    The returned function field is unique in the sense that if you
    call this function twice with an equal ``genus``, ``signatured_group``,
    ``rationalizing_field``, ``constant_field`` and ``names`` it returns the
    same python object in both calls.

    INPUT:

        - ``genus``
        - ``signatured_group``
        - ``rationalizing_field``
        - ``constant_field`
        - ``names`` -- variable names (as a tuple of length 1 or string)
        - ``category`` -- a category (defaults to category of function fields)

    EXAMPLES::

        sage: K.<x> = FunctionField(QQ)
        sage: R.<y>=K[]
        sage: y2 = y*1
        sage: y2 is y
        False
        sage: L.<w>=K.extension(x-y^2) #indirect doctest
        sage: M.<w>=K.extension(x-y2^2) #indirect doctest
        sage: L is M
        True
    """
    def create_key(self, signatured_group, rationalizing_subgroup, constant_field, names=None):
        if not(isinstance(names,tuple) or names==None):
            raise ValueError, "names should be a tuple of two variable names like ('y', 'x')"
        return (signatured_group, rationalizing_subgroup, constant_field, names)

    def create_object(self, version, key, **extra_args):
        return self.signatured_function_field_selector(key[0], key[1], key[2], names=key[3])

    def signatured_function_field_selector(self, signatured_group, rationalizing_subgroup, constant_field, names = None):
        """
        This class calls the "have_compatible_structure" function of all children
        of SignaturedFunctionField class and determined which class needs to be
        chosen for the specific signature.
        """
        from signatured_function_field import SignaturedFunctionField
        #we need to import all subclasses as well so this function works
        from signatured_function_field_cyclic import SignaturedFunctionFieldCyclic
        from signatured_function_field_dihedral import SignaturedFunctionFieldDihedral
        from signatured_function_field_alternating_4 import SignaturedFunctionFieldAlternatvie4
        from signatured_function_field_symmetric_4 import SignaturedFunctionFieldSymmetric4
        from signatured_function_field_alternating_5 import SignaturedFunctionFieldAlternatvie5
        #from signatured_function_field_abelian import SignaturedFunctionFieldAbelian

        for cur_function_field_type in SignaturedFunctionField.__subclasses__():
            # Check whether function field type is appropriate for the signature
            if cur_function_field_type.have_compatible_structure_for(signatured_group, rationalizing_subgroup):
                return cur_function_field_type(signatured_group, rationalizing_subgroup, constant_field, names)

        raise ValueError, "Could not find a function field type to reconstruct for %s" % signatured_group

SignaturedFunctionField = SignaturedFunctionFieldFactory("function_field_constructor.SignaturedFunctionField")
