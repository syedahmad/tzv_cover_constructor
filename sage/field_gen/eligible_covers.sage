"""
Tries to generates signatured function field with automorphism group

AUTHOR:

- Syed Ahmad Lavasani (2012)

"""
import  sys
from jennifer_digest import JenniferDigest

class EligibleCurveFinder:
    # cover genus   =   0  1     2      3             4
    #eligible_deg_gen = [[],[],[(1,2)],[(2,2)], [(2, 2), (3,2)], [(2,2),(3,2),(4,2)]]
    eligible_deg_gen = [[],[],[(1,2)],[(2,2)], [(2, 2)], [(2,2),(3,2)]]
    full_auto_list = {3: [(168,42), (6,1),(2,1),(4,2),(8,3),(24,12),(96,64),(16,13),(48,33),(3,1),(6,2),(9,1),(48,48),(32,9),(24,5),(14,2),(16,11),(12,4),(8,2),(8,5),(4,1),(4,2),(2,1)], 4:[(120,34),(72,42),(72,40),(40,8),(36,12),(32,19),(24,3),(18,2),(15,1),(12,2),(10,2),(36,10),(6,2),(6,1),(24,12),(20,4),(18,3),(16,7),(12,5),(12,4),(12,3),(10,1),(8,4),(6,2),(5,1),(12,4),(8,3),(8,3),(6,2),(4,1),(6,1),(4,2),(4,1),(3,1),(3,1),(3,1),(4,2),(4,2),(2,1),(2,1),(2,1)]}

    def  __init__(self, sig_group_filename, genus):
        """
        for now we are doing it genus by genus
        """
        self.genus = genus
        self.sig_group_filename = sig_group_filename
        self.sig_group_list = []
        self.eligible_groups = {}

    def generate_all_signatured_group(self):
        file_digester = JenniferDigest(self.genus, self.sig_group_filename)
        self.sig_group_list = file_digester.digest_monodromies(full_auto_only=True, no_repeat=True)

    def is_eligibile(self, sign_grp):
        """
        We check eligibility by computing the fixed signatures of monodromies
        of proper degree. This varies genus by genus
        """
        #return False
        #just for test constructability of everything
        #return True
        eligible = False
        for cur_deg_gen in self.eligible_deg_gen[self.genus]:
            if (sign_grp.order() % (cur_deg_gen[0] * 3) == 0):
                for f in sign_grp.conjugacy_classes_representatives():
                    if f.order() == cur_deg_gen[0]:
                        cur_sub_sig = sign_grp.sub_signature(sign_grp.subgroup([f]))
                        if cur_sub_sig[0] == cur_deg_gen[1]:
                            print len(self.eligible_groups[self.genus]), ": ", sign_grp.group_id(), cur_deg_gen, [i.order() for i in cur_sub_sig[1]], sign_grp.signature_orders(), sign_grp.sub_signature(sign_grp)[0], [i.order() for i in sign_grp.sub_signature(sign_grp)[1]]
                            eligible = True
                            return True

                for h in sign_grp.subgroups():
                    if h.order() == cur_deg_gen[0]:
                        cur_sub_sig = sign_grp.sub_signature(h)
                        if cur_sub_sig[0] == cur_deg_gen[1]:
                            print "subgroup!!!", len(self.eligible_gr^oups[self.genus]), ": ", sign_grp.group_id(), cur_deg_gen, [i.order() for i in cur_sub_sig[1]], sign_grp.signature_orders(), sign_grp.sub_signature(sign_grp)[0], [i.order() for i in sign_grp.sub_signature(sign_grp)[1]]
                            eligible = True
                            return True

        if (sign_grp.group_id() == [24, 8]):
            set_trace()
            gap_group = gap.SmallGroup(12, 1)
            gap_perm_group_iso = sign_grp._gap_().IsomorphicSugroups(gap_group)
            gap_gens = gap_group.GeneratorsOfGroup()
            gap_perm_group = gap_perm_group_iso.Image(gap_group)

            fix_group = sign_grp.subgroup([gap_perm_group.GeneratorsOfGroup()])
            sign_grp.sub_signature(fix_group)[0], [i.order() for i in sign_grp.sub_signature(fix_group)[1]]

        return eligible

    def constructable(self, sign_grp):
        """
        We go through all Abelian normall subgroups to check if
        we get to genus 0
        """
        constructablity = False
        #we are obviously more interested in cyclic subgroups as it
        #ends up to be much simpler
        cyclic_first_abelian_normal_subgroups = []
        noncyclic_abelian_normal_subgroups = []
        for cur_fix_group in sign_grp.normal_subgroups():
            if not cur_fix_group.is_abelian() or cur_fix_group.order()==1:
                continue

            if cur_fix_group.is_cyclic():
                cyclic_first_abelian_normal_subgroups.append(cur_fix_group)
            else:
                noncyclic_abelian_normal_subgroups.append(cur_fix_group)

        cyclic_first_abelian_normal_subgroups.extend(noncyclic_abelian_normal_subgroups)

        for cur_fix_group in cyclic_first_abelian_normal_subgroups:
            if not cur_fix_group.is_subgroup(sign_grp.center()): 
                
                print cur_fix_group.group_id(), "not in", sign_grp.center().group_id()
                print "its centralizer is", cur_fix_group.centralizer(sign_grp).group_id()
                continue
            cur_sub_sig = sign_grp.sub_signature(cur_fix_group)
            if cur_sub_sig[0] == 0:
                print cur_fix_group.group_id(), "cyclic:", cur_fix_group.is_cyclic()
                constructablity = True
                if not cur_fix_group.is_cyclic():
                    normal_decomposition = sign_grp.normally_decomposable(cur_fix_group)
                    if (normal_decomposition):
                        print "Possible normal decompsition: ", [cur_sub_subs.group_id() for cur_sub_subs in normal_decomposition]
                        constructablity = True

                else:
                    print "Is central extension? ", cur_fix_group.is_subgroup(sign_grp.center())
                    if not cur_fix_group in sign_grp.center().subgroups():
                        semi_dp = False
                        H = sign_grp.quotient(cur_fix_group)
                        for s in sign_grp.subgroups():
                            map_h = H.isomorphism_to(s)
                            if map_h != None:
                                semi_dp = True
                                H = map_h(H)
                                break
                            #semi_dp = H.is_subgroup(sign_grp)
                        print "semi-direct product:", semi_dp
                        if (semi_dp):
                            print "direct product:", H.is_normal(sign_grp)
                            if not H.is_normal(sign_grp):
                                if H.order() == 2:
                                    print "Dihedral"
                    # We don't break to get all possibility
                    #break
            else:
                print cur_fix_group.group_id(), "has fixed genus ", cur_sub_sig[0]

        return constructablity

    def eligible_morphisms(self, sign_grp):
        pass

    def check_all_eligibility(self):
        self.eligible_groups[self.genus] = []
        for cur_sig_grp in self.sig_group_list:
            if self.is_eligibile(cur_sig_grp):
                self.eligible_groups[self.genus].append(cur_sig_grp)

    def check_all_constructibility(self):
        for  cur_sig_grp in self.eligible_groups[self.genus]:
            print "Checking curve with group:", cur_sig_grp.group_id(), cur_sig_grp.signature_orders()
            constructable = self.constructable(cur_sig_grp)
            print cur_sig_grp.group_id(), " is ",  constructable and "constructable" or "unconstructable"

if __name__ == "__main__":
    #test
    # all_eligible_curves = EligibleCurveFinder("/lu101/sahosse/doc/phd/sage/field_gen/test_g4.txt", genus = 4)

    # all_eligible_curves.generate_all_signatured_group()
    # all_eligible_curves.check_all_eligibility()
    # all_eligible_curves.check_all_constructibility()

    for i in [4,5]:
        all_eligible_curves = EligibleCurveFinder("grpmono0"+str(i)+".txt", genus = i)

        all_eligible_curves.generate_all_signatured_group()
        all_eligible_curves.check_all_eligibility()
        all_eligible_curves.check_all_constructibility()

        # The code sniept suggesting that [12, 1][1,3] isn't full-auto grp.
        # if (sign_grp.group_id() == [24, 8]):
        #     import pdb
        #     pdb.set_trace()
        #     gap_group = gap.SmallGroup(12, 1)
        #     gap_perm_group_iso = sign_grp._gap_().IsomorphicSugroups(gap_group)[1]
        #     gap_gens = gap_group.GeneratorsOfGroup()
        #     gap_perm_group = gap_perm_group_iso.Image(gap_group)

        #     fix_group = sign_grp.subgroup([gap_perm_group.GeneratorsOfGroup()])
        #     sign_grp.sub_signature(fix_group)[0], [i.order() for i in sign_grp.sub_signature(fix_group)[1]]
