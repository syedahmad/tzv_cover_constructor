﻿"""
Signatured Function Fields and their factory

 We have problem that obviously our signatured function field are very similar so make sense that they are children of the same class but the problem is that until their creation we don't know which object should be used.

The design will be as follows:
   - The parent class Signatured Function field daughter of  FunctionField_polymod.
   - For every automorphism group of rational function that's G/C the would be a children of the signature function field.

   - There would be a factory class thet returns the correct class for choice    G/C

AUTHOR:

- Syed Ahmad Lavasani (2012)

2014-03: Adding the generation of automorphism group based on change of generator.

"""
from pdb import set_trace
from sys import stdout

from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.function_field.constructor import FunctionField
from sage.rings.function_field.function_field import FunctionField_polymod
from sage.misc.misc_c import prod
from sage.rings.arith import gcd

class RationalParam:
    """
    Rational parmeters arises from ramified places that only ramified in the non-rational
    extension. Because those arise in rational extension can be completly taken care of
    (do I know what I'm talking about?)

    This class is keeping track of which parameter is arised from which inertia group
    so if one need to find the compositum of two fields one knows which paramters needs
    to be identfied.
    """
    def __init__(self, parameter, inertia_group):
        """
        Initiates the parameter data

        INPUT:
            parameter: the actual generator in the fraction field of the rational
                function field
            parameter_name: the string representation of the parameter as
                computed by the SignaturedFunctionField
            inertia group: a rep of the conjugacy class of the inertia groups over
                the place parametrized by the parametr.
        """

        self.parameter = parameter
        self.inertia_group = inertia_group


class SignaturedFunctionField(FunctionField_polymod) :
    """
    This function field is having the following properties:

    - cyclic subgroup of centre.
    - signature over the fixed field of whole automorphism group.
    - signature over the fixed field of cyclic group.
    - The quotient of the big group over cyclic group.
    - We don't care about small p's so only q and the signature makes differe

    However to generate this function field unlike FunctionField_polymod
    we only feed the automorphism_group, signature and the cyclic subgroup
    the function field needs to generate its own rational function field
    and the equation and then calls the FunctionField_polymod init with
    that equation

    Az Madar-e aroos: This class is designed to get a Brandt curve family and a
    signatured group plus a genus and compute the equation of the curve
    """
    def __init__(self, signatured_group, rationalizing_subgroup, constant_field, names = None):
        """
        INPUT::
        """
        from sage.misc.misc import set_verbose
        set_verbose(2)

        self._signatured_group = signatured_group
        self._genus = signatured_group.genus()
        self._rationalizing_subgroup = rationalizing_subgroup
        self._constant_field = constant_field
        self._paramless_constant_field = constant_field
        self._all_gens_names = (names == None) and ['y','x'] or names

        self._sub_signature = self._signatured_group.sub_signature(self._rationalizing_subgroup)

        #we only consider G = CxH case
        self._assume_direct_product_decomposition = True
        if (not self.check_structure_validity()):
                raise ValueError, "The fixed field of the subgroup is not rational"

        self._compute_fundamental_values()
        self._construct_rational_function_field()
        my_poly = self._compute_polynomial()
        FunctionField_polymod.__init__(self, my_poly, names=(self._all_gens_names[0],))
        print self
        print "of genus", self._genus
        set_trace()
        self._compute_automorphism_group()

    # """
    # def __init__(self, polynomaial, names, autmorhpism_gens, galois_group, full_signature):
    # """    # Create a signatured function field

    # INPUT:

    #     - ``polynomial`` -- a univariate polynomial over a function field.
    #     - ``names`` -- variable names (as a tuple of length 1 or string)
    #     - ``automorphism_gens`` -- data to be send to the automorphism function to generate the automorphism group.
    #     - ``galois_group`` the subgroup of the automorphism group such that it's the automorphism group of F/k(x).
    #     - ``signature`` a tuple that contains the ramification of the field over k(x).
    # """

    def _compute_fundamental_values(self):
        """
        This is to compute the values needed in Brandt's thesis to
        compute the equation.
        """
        #For the sake of following Brandt's naming
        self._quotient_group = self._signatured_group.quotient(self._rationalizing_subgroup)
        #we use this so much that makes sense to store it in a variable
        self._quotient_group_order = self._quotient_group.order()
        self._kummer_ext_deg = self._rationalizing_subgroup.order()

        #this is the total ramification index only for those ramfied downstairs
        #set_trace()
        (self._reduziert_Verzweigungstyp, self._Versweiguntyp, self._purely_kummer_ramified, self._versweigun_gruppen) = self._signatured_group.Versweiguntyp_of(self._rationalizing_subgroup)

        print "all detail: ", self._signatured_group.Versweiguntyp_of(self._rationalizing_subgroup)
        print "Ramification:",(self._reduziert_Verzweigungstyp, self._Versweiguntyp, self._purely_kummer_ramified)
        #e(P|P_0) = _kummer_ext_deg/gcd(kummer_factor_power, _kummer_ext_deg)
        #= _kummer_ext_deg/kummer_factor_power =>
        # kummer_factor_power = _kummer_ext_deg/e(P|P_0)
        self._kummer_factor_powers = [self._kummer_ext_deg/(full_ram/red_ram) if not red_ram == full_ram else 0 for (red_ram, full_ram) in zip(self._reduziert_Verzweigungstyp, self._Versweiguntyp)]
        self._kummer_factor_powers.extend([self._kummer_ext_deg/(full_ram/1) for full_ram in self._purely_kummer_ramified])
        #power _kummer_ext deg is waste of resources but i doubt such a thig
        #note on purely kummer ramification. There will be |G|/|C| number
        #of places with this degree as they are unnramified in F^G/C but I
        #believe that Brandt algorithm group all of them in one factor
        #print "Kummer factor powers (before infinity repair):", self._kummer_factor_powers

    def _compute_number_of_parameters(self):
        """
        any further actoin should be implemented by appropriate subclass
        This is because each sub class needs different number of parameters
        added to the function field
        """
        self.no_of_params = len(self._purely_kummer_ramified)

        self._var_str = ['a'+str(i) for i in range(0, self.no_of_params)]

    def _construct_rational_function_field(self):
        """
        Using the parameters given from subclass generate the rational function
        field
        """
        #First we need to store all parameters in var_str
        #this depends on the type of factor group
        #pdb.set_trace()
        self._compute_number_of_parameters()
        #then we make the polynomial ring we works on.
        self._param_ring = len(self._var_str) and PolynomialRing(self._constant_field, names=tuple(self._var_str)) or self._constant_field

        self._params = [RationalParam(self._param_ring.gens()[i], self._versweigun_gruppen[1][i]) for i in range(0, len(self._var_str))]
        #it is useful to have param names only array cause we need to use them
        #to evaluate at different x
        self._param_names = [cur_param.parameter for cur_param in self._params]
        self._base_field = self._param_ring.fraction_field()
        self._rational_function_field =  FunctionField(self._base_field, names = self._all_gens_names[1])

    def _compute_kummer_factors(self):
        """
        We return an array each element is a factor corresponding to a
        ramified place. The last factor is always correspond to places
        purely ramify in the kummer extension

        CONVENTION: If infinity is ramified we set the place corresponding to
        infinity equal to 1
        """
        pass

    def _compute_polynomial(self):
        """
        should be implemented by appropriate subclass
        """
        #for infinity if it is ramified we need to make sure that the powers
        #doesn't add up to q
        self._compute_kummer_factors()
        #we need to compute kummer factor before we are able to fix infinity
        set_trace()
        self._fix_infinity()

        R = PolynomialRing(self._rational_function_field, names =  self._all_gens_names[0])
        self._kummer_rational_side = prod([cur_factor**cur_power for (cur_factor, cur_power) in zip(self.kummer_factors, self._kummer_factor_powers)])

        return R.gen()**self._rationalizing_subgroup.order()-self._kummer_rational_side

    def check_structure_validity(self):
        """
        Hurwitz:
        (g - 1) - 1/2*sum(signatured_group.sub_signatures.order() - 1) + q
        == 0
        """
        #((self._genus - 1) - 1/2*(sum([cur_ram.order() -1 for cur_ram in self._signatured_group])) + self._rationalizing_subgroup.order()) == 0
        #this is done by the sub_signature module so we don't need to        #repeat it
        return self._sub_signature[0] == 0

    def _fix_infinity(self):
        """
        This function will take care of infinity,
        It is the job of _kummer_factor function to select a prime to be
        infinity.

        This is written for kummer extensions y^q = f(x) for q is a prime power:
        if q /| deg(f(x)) infinity is ramified so this function make sures that
        this condition is only good if q is prime we need to take a more complicated approach
        """
        #   It's power manipulation business either way (ramified or unramified)
        #we just need to choose the correct power
        if (1 in self.kummer_factors and not self._kummer_factor_powers[self.kummer_factors.index(1)] == 0):
            self._ramified_at_infinity = True
            self._infinite_place = self.kummer_factors.index(1)
        else:
            self._ramified_at_infinity = False
            self._infinite_place = (not 1 in self.kummer_factors) and -1 or self.kummer_factors.index(1)

        #we compute the true power of each kummer factor
        #this kill infinity power as well 1.degree() = 0
        aux_power_list = [i.element().numerator().degree()*j for (i,j) in zip(self.kummer_factors, self._kummer_factor_powers)]
        total_degree = sum(aux_power_list)

        #check if we are already in ideal situation for infinit
        #set_trace()
        if self._ramified_at_infinity: #if ramified at infinity infinity power should be none-zero
            if self._kummer_ext_deg/gcd(self._kummer_factor_powers[self._infinite_place], self._kummer_ext_deg) == self._kummer_ext_deg/gcd(total_degree,self._kummer_ext_deg):
                #already the correct ramification at infinity
                return
        elif (total_degree % self._kummer_ext_deg) == 0: #should be unramifie
            #infinity already unramified
            return 

        #find the kummer factor with least degree such that q /| its
        #degree. this lead to least cumbersome equation but isn't
        # clear if it's the best one
        useful_factor_degrees = [(cur_factor_total_power[0], self.ramification_repair_cost(cur_factor_total_power[1], total_degree)) for cur_factor_total_power in enumerate(aux_power_list) if not cur_factor_total_power[0] == self._infinite_place]

        from operator import itemgetter
        min_factor = min(useful_factor_degrees, key=itemgetter(1))

        if min_factor[1]==self._kummer_ext_deg:
        #this a convention for failure
            raise ValueError, "Unable to repair the ramification at infinity"

        self._kummer_factor_powers[min_factor[0]] *= min_factor[1]

    # def unramify_inifinity(self):
    #     """
    #     This is written for kummer extensions y^q = f(x) for q is a prime:
    #     if q | deg(f(x)) infinity is unramified so this function make sures that
    #     this condition is satisfied
    #     """
    #     total_degree = prod(self.kummer_factors).element().numerator().degree()
    #     #otherwise infinity is already unramified
    #     if (total_degree % self._kummer_ext_deg):
    #         #find the kummer factor with least degree such that q /| its
    #         #degree
    #         useful_factor_degrees = [(cur_factor[0], self.unramification_cost(cur_factor[1].element().numerator().degree(), total_degree)) for cur_factor in enumerate(self.kummer_factors) if cur_factor[1].element().numerator().degree() % self._kummer_ext_deg]

    #         from operator import itemgetter
    #         min_factor, unramification_deg = min(useful_factor_degrees, key=itemgetter(1))
    #         #if self._rationalizing_subgroup.order == 2 we are doomed
    #         self.kummer_factors.append(self.kummer_factors[min_factor]**unramification_deg)

    def ramification_repair_cost(self, factor_degree, total_degree):
        """
        check what's the smallest power of current factor needed to multiplied
        in to the kummer factors to make infinity unramified/ramified

        in general case where the kummer extension isn't prime we
        can't ignore the gcd any more.
        """
        from sage.rings.arith import xgcd
        g, u, v = xgcd(factor_degree, self._kummer_ext_deg)

        #TODO: The desired degree doesn't give us all the choice for example
        #if infinity is totally ramified in Kummer extension of degree 3 then
        #desired power can be either 1 or 2 but I think we always is going
        #for 1, more efficiently we should settle for 2 if it's already 2.
        #another example is when kummer extension is 4 then either 1 or 3
        #works, to get 3 you only need 1 more factor for 1 you need 3 if your
        #total degree is two this will settle on the latter.
        desired_power = (self._ramified_at_infinity and self._kummer_factor_powers[self._infinite_place] or 0) - total_degree

        if desired_power % g:# this factor doesn't work return unacceptable
                             # cost
            return self._kummer_ext_deg

        return u*(desired_power / g) % self._kummer_ext_deg

    def _extend_constant_field(self, constant_min_polies, const_gen_name='t'):
        """
        This will make sure that at least a root of constant_min_polies
        is in the constant field so the equation of the curve and the
        automorphisms can be defined over its constant field.

        This is only defined for finite constant field for now
        """
        if not self._constant_field.is_finite():
            raise NotImplemented, "Extending constant field for infinite fields is not implemented"

        #All we need is to find the lcm of the samllest factors of the
        #the given polynomial.
        extension_degrees = [self._constant_field.degree()]
        for cur_min_poly in constant_min_polies:
            cur_poly_ring = PolynomialRing(self._constant_field, cur_min_poly.parent().variable_name())
            min_poly_factors = cur_poly_ring(cur_min_poly).factor()
            min_factor_deg = min([cur_factor[0].degree() for cur_factor in min_poly_factors])

            extension_degrees.append(min_factor_deg)

        from sage.rings.arith import lcm
        from sage.rings.finite_rings.constructor import FiniteField
        extension_deg = lcm(extension_degrees)
        self._constant_field = FiniteField(self._constant_field.characteristic()** extension_deg, const_gen_name)
        self._paramless_constant_field = self._constant_field

    def _compute_automorphism_group(self):
        """
        Compute the automorphism group by determining a set of generators and then
        Asking the group to generate the automorphism group in traditional way
        (mapping gap generators to automorphisms images)
        """
        #set_trace()
        self._auto_gens = self._compute_automorphism_generators()
        #we call the automorphis_group function to compute and store the
        #automorphism group using
        #set_trace()
        self._automorphism_group = self.automorphism_group(tuple(self._auto_gens), tuple(self._signatured_group.group_id()))

    def _compute_automorphism_generators(self):
        """
        This is totally different for central and non-central extension.
        I need an algorithm for non-central case.

        Here I assume that it is enough to extend the generators of
        the lower field as it essentially will give mn elements
        """
        extended_gens_img = []
        #first we ask for the generators of the automorphism of the
        #fixed field
        #set_trace()
        self._rational_auto_gens = self._quotient_automorphism_generators()
        self._galois_gen = self._galois_group_generators()


        #although this seems much faster way of generating the group
        # self._direct_product_decomposition == False is more lazy
        # way of doing it. So I'm going with that
        if (self._assume_direct_product_decomposition == True):
            #downstaris gens
            extended_gens_img.extend(self._rational_auto_gens)

            #Upstairs gen
            x_img = self(self._rational_function_field.gen())
            y_img = self._galois_gen(self.gens()[0])

            extended_gens_img.append((y_img, x_img))

        else:
            raise NotImplemented
            x = self._rational_function_field.gen()
            y = self._gen

            for cur_rational_auto_gen_img in self._rational_auto_gens:
                #we extend them with the Galois group generator
                #to extends we first extend the rational_auto_gens
                #with change of generator that will give us a primitve element
                #then we chain it with the Galois group generator
                (img_func_field, extended_sigma, extended_sigma_inverse) = self.change_generators(cur_rational_auto_gen_img, y = y, names = ['xp','yp'])
                #we need to just map xp to x, and yp to y so SAGE
                #knows it is actually an automorphism. NB it is not the
                #inverse of extend sigma
                automorphize_morphism = extended_sigma.codomain().hom([y,cur_rational_auto_gen_img])
                extended_sigma = automorphize_morphism * extended_sigma
                for i in range(0, self._rationalizing_subgroup.order()):
                    cur_galois_aut = self._galois_gen**i
                    x_img = extended_sigma(self._rational_function_field.gen())
                    y_img = extended_sigma(cur_galois_aut(self.gens()[0]))
                    extended_gens_img.append((y_img, x_img))

        return extended_gens_img

    def _primitive_nth_root_of_unity(self, n):
        """
        Returning a primitve nth root of unity in the constant field. The case
        of finite and infinite field needs to be delt differently due to
        function naming

        OUTPUT::
            Return \zeta_n
        """
        #First we need to generate the root of unity that we need
        #we generate a primitive one
        if (not self._constant_field.is_finite()):
            primitive_root = self._constant_field.primitive_root_of_unity()

        else: #finite case: primitve_element is the same as multiplitive generator
            primitive_root = self._constant_field.multiplicative_generator()

        if (primitive_root.multiplicative_order() % n):
            raise ValueError, "Constant filed does not contain the desired root of unity" #bigger extension is needed

        return primitive_root**(primitive_root.multiplicative_order()/n)

    def _quotient_automorphism_generators(self):
        """
        This is to be overloaded. The generators of the restricted
        automorphism group to the base field need to be hard coded

        OUTPUT::
            Returns list of images of x under different generators
        """
        pass

    def _compute_y_img_constant(self, y_img_crude, x_img):
        """
        To compute the image of y under an automorphism the child
        class will give the image of y in terms of B(x)*y, there
        is an adjustment of the constant beta is needed to compute
        the correct image beta*B(x)*y

        OUTPUT::
           Returns beta as described above
        """
        #set_trace()
        #we have y = f(x), need to evaluate f(x_img)
        rational_img = self._kummer_rational_side.element()(self._param_names == [] and x_img or [x_img]+self._param_names)
        #(y_img)^q= f(x_img) so (y_crude)^q/f(x_img)
        #we need (y_crude*beta/y_img)^q = 1 so
        #we have beta^q = f(x_img)/y_crude^q
        crude_differential = rational_img/(y_img_crude**self._kummer_ext_deg)
        #All the acrobat we need to do convert the differential which we
        #know is a number to an element of the constant field without
        #paramenters
        crude_diff_n = self._rational_function_field(crude_differential.element()).numerator()
        crude_diff_d = self._rational_function_field(crude_differential.element()).denominator()

        beta_to_q = self._paramless_constant_field(crude_diff_n)/self._paramless_constant_field(crude_diff_d)
        #it will throw up if the y_img_crude was chosen in a wrong way

        #now computing q'th root of beta
        return beta_to_q.nth_root(self._kummer_ext_deg)

    def _galois_group_generators(self):
        """
        This is simply returns the generator of the Galois group, that
        is an automorphism [y->\zeta_q*y, x->]. Should be called after
        the field generation

        OUTPUT::
            Return (\zeta_q*y, x)
        """
        #First we need to generate the root of unity that we need
        zeta_q = self._primitive_nth_root_of_unity(self._kummer_ext_deg)

        return self.hom((zeta_q * self._gen, self._rational_function_field.gen()))

    def vulnerable_subfields(self, vulnerable_genus = 2):
        """
        return a list of pairs, first elemen vulnerable subfields, second element
        is the automorphism whose fixed field is the vulnerable subfield
        """
        vulnerable_pairs = []
        y = self.gens()[0]
        x = self.rational_function_field().gens()[0]
        for cur_deg_gen in range(2, self._genus):
            if (self._signatured_group.order() % (cur_deg_gen * 3) != 0):
                continue

            auto_of_sign_element = self._signatured_group.isomorphism_to(self._automorphism_group)
            for fixer in self._signatured_group.conjugacy_classes_representatives():
                if fixer.order() == cur_deg_gen:
                    cur_sub_sig = self._signatured_group.sub_signature(self._signatured_group.subgroup([fixer]))
                    print "fixer has order", fixer.order()
                    print "fixed genus is ", cur_sub_sig[0]
                    print "subsignature is", [cur_auto.order() for cur_auto in cur_sub_sig[1]]
                    if cur_sub_sig[0] == vulnerable_genus:
                            #import pdb
                            #pdb.set_trace()
                            f = self._automorphism_group(auto_of_sign_element(fixer))
                            print "Class rep f with"
                            stdout.flush()
                            print 'f(x) = ', f(x)
                            stdout.flush()
                            print 'f(y) = ', f(y)
                            stdout.flush()
                            (curfield, subEmbed) = f.fixed_field(['u','z'])
                            stdout.flush()
                            print '*****************************RESULT*****************************'
                            print 'The fixed field of f is'
                            print curfield
                            try:
                                print 'of genus ', curfield.genus()
                            except:
                                print 'unable to compute genus'

                            print
                            print 'using embedding'
                            print subEmbed
                            stdout.flush()
                            zImg = subEmbed(curfield.gens()[0])
                            tImg = subEmbed(curfield.rational_function_field().gen())
                            assert(zImg == f(zImg))
                            assert(tImg == f(tImg))

                            vulnerable_pairs.append((curfield(), (f(y),f(x))))
                            self.curve_db.insert_vulnerable_subfield(self.cover_id, curfield, 2, f, f(x), f(y))

        return vulnerable_pairs

    @classmethod
    def have_compatible_structure_for(cls, signatured_group, rationalizing_subgroup):
        """
        This function should be implemented by each child. Ivt's called by the
        class factory to check if the child can generate the function field e
        equation for specific group/subgroup/ramification situation.
        """
        pass

    @classmethod
    def is_direct_product_extension(cls, signatured_group, rationalizing_subgroup):
        """
        Normally we would like to generate kummer function fields whose quotient of
        Automorphism group by the kummer automorphism doesn't form a normal subgroup
        but for now we are going with this restricion, which is checked by this
        function

        OUTPUT::
            weather the quotien of rationalizing_subgroup is a normal subgroup of
            signatured group itself.
        """
        #first check if the quotient is a subgroup
        quotient_is_sub = False
        H = signatured_group.quotient(rationalizing_subgroup)
        for s in signatured_group.subgroups():
            map_h = H.isomorphism_to(s)
            if map_h != None:
                quotient_is_sub = True
                H = map_h(H)
                break

        if not quotient_is_sub:
            return False

        #sage bug doesn't accept trivial group as subgroup :(
        return H.order() == 1 or H.is_normal(signatured_group)

