"""
The usefulness of these classes is under investigation

Database for Signatured Function Field Ordered by their Automorphism Group 

AUTHORS:
        
- Syed Ahmad Lavasani (2012)

"""
class BrandtCurveFamilyType:
    r"""
    I need to have a container class which store each types of curve
    in Brandt's thesis. Hence, as far as I know it should have a 
    "Ramification type", this is a gap small group id and a bunch of 
    integer as ramification index. I also need to store the equation
    which is of sage.symbolic.expression.Expression type
    """
    def __init__(self,typ, cyclic_ext_size, quotient_group_id, Versweiguntyp, equation, kummer_factors):
        """
        INPUT::
        typ: tuple with major and minor index from Brandt's thesis
        cyclic_ext_size: (prime) integer of the size of kummer extension
        quotient_group_id: gap index of G/C quotient where G is the 
                           Automorphism group and C is the Galois group
                           of the kummer extension.
        Versweiguntyp: tuple of ramification index of F/F^G of those
                       primes that are ramified in F^C/F^G
        """
        self.typ = typ
        self.cyclic_ext_size = cyclic_ext_size
        self.quotient_group_id = quotient_group_id
        self.verzweigungstyp = verzweigungstyp
        self.kummer_factors = kummer_factors


class SignaturedFunctionFieldDatabase:
    r"""
    This basically contains two dictionarys:
       - dictionary maps signatures = (gap index)+(Ramification) to signatured function fields.
    
       - dictianry maps from big p brandt signature to callable functions.
    """
    def __init__(self):
        """
        Only to initialize the dictionaries. In real life we serialize
        the object so we don't need to compute them everytime
        """
        pass

                                             
    



