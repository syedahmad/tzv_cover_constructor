#!/usr/bin/env python

from sys import argv

if len(argv) < 2:
    print "usage: solve_jennifer_extra_line_feed.py grpmono_file"
    sys.exit(0)

grpmono_file = open(argv[1])
grpmono_temp_file = open(argv[1]+".fixed", "w+")

last_line = None

for cur_line in grpmono_file:
    if cur_line[2] != 'f' and cur_line[2] != '(' and last_line != None:
        grpmono_temp_file.write("\n")
    
    grpmono_temp_file.write(cur_line[:-1])
    last_line = cur_line

grpmono_temp_file.write("\n")

grpmono_file.close()
grpmono_temp_file.close()
