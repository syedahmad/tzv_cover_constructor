# This file was *autogenerated* from the file test_subsig-upstair.sage.
from sage.all_cmdline import *   # import sage library
_sage_const_3 = Integer(3); _sage_const_2 = Integer(2); _sage_const_1 = Integer(1); _sage_const_0 = Integer(0); _sage_const_337 = Integer(337); _sage_const_6 = Integer(6); _sage_const_5 = Integer(5); _sage_const_4 = Integer(4); _sage_const_48 = Integer(48); _sage_const_168 = Integer(168); _sage_const_9 = Integer(9); _sage_const_8 = Integer(8); _sage_const_96 = Integer(96); _sage_const_42 = Integer(42); _sage_const_13 = Integer(13); _sage_const_12 = Integer(12); _sage_const_24 = Integer(24); _sage_const_14 = Integer(14); _sage_const_7 = Integer(7); _sage_const_33 = Integer(33); _sage_const_64 = Integer(64)#This file is to generate the group of candidates of genus 3 curves and their fixed field.
#/lu101/sahosse/doc/RecherchePhD/Codes/sage/AutGrps/

set_verbose(_sage_const_0 )

#from fixed_field_comp import fixed_field
from sys import exit, stdout


#@profile
def compute_all_fixed_field(coverField, coverAutGrp):

    print "Computing fixed field of ", coverField, " with automorphism groupid ", coverAutGrp.group_id()

    #First we change the bottom field to the one fixed with the whole
    #Autogroup
    for f in coverAutGrp.conjugacy_classes_representatives():
        if (f  in coverAutGrp.center()):
            #let check the quotient group.
            #If it's cyclic we are lucky.
            if (f.order() == _sage_const_1 ): continue
            (cover_field_over_mid, short_2_mid, mid_2_short) = coverField.change_generators(f.fixed_element(), names=['x','y'])

            y = cover_field_over_mid.gens()[_sage_const_0 ]
            x = cover_field_over_mid.rational_function_field().gens()[_sage_const_0 ]

            #now we need to re-generate the aut grp for over-mid cover
            #gensImg = []
            #for g in coverAutGrp.gens():
            #    cur_mid_auto = short_2_mid * g.as_hom() * mid_2_short
            #    gensImg.append((cur_mid_auto(y),cur_mid_auto(x)))

            #mid_auto_grp = cover_field_over_mid.automorphism_group(gensImg, coverAutGrp.group_id())

            #update f to its new form
            #f_aut = short_2_mid * f.as_hom() * mid_2_short

            #cyc_cent_subgroup = mid_auto_grp.subgroup([f])
            #downstairs_auto_reps = [cur_coset[0] for cur_coset in tall_auto_grp.cosets(cyc_cent_subgroup)]
            #quotient_grp = tall_auto_grp.quotient(cyc_cent_subgroup)

            print "Class rep f with"
            stdout.flush()
            print 'f(x) = ', f(x)
            stdout.flush()
            print 'f(y) = ', f(y)
            stdout.flush()
            
            #because x is already fixed by f then our new x and old x are the 
            #same
            (curfield, subEmbed) = f.fixed_field(['x','z'])
            stdout.flush()

            polyOnFix = PolynomialRing(curfield, names=('y',)); (y,) = polyOnFix._first_ngens(1)
            poly_composite = polyOnFix(cover_field_over_mid.polynomial())
            poly_component = poly_composite.factor(proof=false)
            
            cur_extension = curfield
            total_degree = _sage_const_1 
            for cur_component in poly_component:
                if (total_degree  == f.order()):
                    break;

                assert(not(total_degree > f.order()))

                if (cur_component[_sage_const_0 ].degree()> _sage_const_1 ):
                    cur_extension = cur_extension.extension(cur_component[_sage_const_0 ], names=('y',)); (y,) = cur_extension._first_ngens(1)
                    if (cur_extension.rational_function_field() != cur_extension.base_field()):
                        (cur_extension, to_sim, from_sim) = cur_extension._make_simple('y')
                    total_degree *= cur_component[_sage_const_0 ].degree()
            
            print 'The fixed field of f is'
            print curfield
            # try:
            #     print 'of genus ', curfield.genus()
            # except:
            #     print 'unable to compute genus'
            print
            print 'using embedding'
            print subEmbed
            stdout.flush()
            zImg = subEmbed(curfield.gens()[_sage_const_0 ])
            tImg = subEmbed(curfield.rational_function_field().gen())

            print 'The cover over fixed field is'
            print cur_extension
            #now we need to try to extend curfield using y lg
            
            #Now using above data we need to compute the
            #Aut group of the curfield.
            
            #the classes of generators of A in A/C should still be able 
            #to generate A/C
            #genstImg = [(subEmbed^-1)(f(tImg)) for f in coverAutGrp.gens()]#, 1/x]  #,  x]
            #genszImg = [(subEmbed^-1)(f(zImg)) for f in coverAutGrp.gens()]
            #gensImg = zip(genszImg, genstImg)

            #curfield_autgrp = curfield.automorphism_group(gensImg, quotient_grp.group_id())
            #now we need to find the fixed field of curfield_autgrp
            #bottom_field = curfield_autgrp.fixed_field()
            #print "H/X^G: ", bottom_field

#(12,4)
#(D_12)
nicePrime = _sage_const_13 
extenPower = _sage_const_1  #this will give me 8'th root of unity

k = FiniteField(nicePrime**extenPower)

a = k(primitive_root(nicePrime))
zeta_3 = a**((k.order()-_sage_const_1 )/_sage_const_3 )
zeta_6 = a**((k.order()-_sage_const_1 )/_sage_const_6 )
#zeta_4 = a^((k.order()-1)/4)
#zeta_8 = a^((k.order()-1)/8)

#zeta_7 = a^((k.order()-1)/7)
#sqtn7 = k(-7).square_root()

kx = FunctionField(k, names=('x',)); (x,) = kx._first_ngens(1)
kxY = kx['Y']; (Y,) = kxY._first_ngens(1);

#k = FiniteField(nicePrime)
#kB.<b> = PolynomialRing(k)
#kb = kB.fraction_field()
#kx.<x> = FunctionField(kb)
#kxY.<Y> = kx[];

b=_sage_const_1 
kxy = kx.extension(Y**_sage_const_2  -x*(x**_sage_const_6  + b*x**_sage_const_3  + _sage_const_1 ), names=('y',)); (y,) = kxy._first_ngens(1)

#Defining the group

#x->zeta_3*x
#y->zeta_6*y
#x->1/x
#y->y/x^4

gensXImg = [_sage_const_1 /x,zeta_3*x]#, 1/x]  #,  x]
gensYImg = [y/x**_sage_const_4 , zeta_6*y]#, y/x^4]#, -y]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_12 ,_sage_const_4 ])

compute_all_fixed_field(kxy, AutGrp)

exit(_sage_const_1 )

nicePrime = _sage_const_337 
extenPower = _sage_const_1  #this will give me 8'th root of unity

k = FiniteField(nicePrime**extenPower)

a = k(primitive_root(nicePrime))
zeta_3 = a**((k.order()-_sage_const_1 )/_sage_const_3 )
zeta_6 = a**((k.order()-_sage_const_1 )/_sage_const_6 )
zeta_4 = a**((k.order()-_sage_const_1 )/_sage_const_4 )
zeta_8 = a**((k.order()-_sage_const_1 )/_sage_const_8 )

zeta_7 = a**((k.order()-_sage_const_1 )/_sage_const_7 )
sqtn7 = k(-_sage_const_7 ).square_root()


kx = FunctionField(k, names=('x',)); (x,) = kx._first_ngens(1)
kxY = kx['Y']; (Y,) = kxY._first_ngens(1);

#(48,33)
kxy = kx.extension(Y**_sage_const_4  - x**_sage_const_3  + _sage_const_1 , names=('y',)); (y,) = kxy._first_ngens(1)

#Defining the group
#We need that 9 has root 4. This is to say that we need 3 to have root 2. which 
#can be checked using Quadratic Resiprocity, this rule says:
#p = +- 1 mod 12.
#further more we need fourth root of unity so 
# p = +1 mod 4 => p - 1 = q 4 = 12 q' + t 4 where t = 0, 1, 2, 3
# p = + 1 + tq (12) = 1, 5, 9, 13 but not 11 = -1 (12) so that
# case is impossible so we need to choose a prime that
# p = 1 (12)
# otherwise you lose some of the fourth root of 9
# and that will take some of the automorphisms
gensXImg = [(x+_sage_const_2 )/(x-_sage_const_1 ), zeta_6**_sage_const_2 *x, x]
gensYImg = [zeta_4*a**(k(_sage_const_9 ).generalised_log()[_sage_const_0 ]/_sage_const_4 )/(x -_sage_const_1 )*y, y, zeta_4*y]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_48 ,_sage_const_33 ])

compute_all_fixed_field(kxy, AutGrp)

#(96,64)
kxy = kx.extension(Y**_sage_const_4  - x**_sage_const_4  -_sage_const_1 , names=('y',)); (y,) = kxy._first_ngens(1)#Y^3 - x^3*Y + x)

#Defining the group

#x-> ix
#y-> iy
#x-> (zeta_8)y, y->(zeta_8)x
#x-> 1/x, y->y/x

gensXImg = [zeta_4*x, x, _sage_const_1 /x, zeta_8*y]
gensYImg = [y, zeta_4*y, y/x, zeta_8*x]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_96 ,_sage_const_64 ])

compute_all_fixed_field(kxy, AutGrp)

#(168, 42)
kxy = kx.extension(x**_sage_const_3 *Y + Y**_sage_const_3  + x, names=('y',)); (y,) = kxy._first_ngens(1)

gensXImg = [zeta_7**_sage_const_3 *x, y/x, ((zeta_7-zeta_7**_sage_const_6 )*x+(zeta_7**_sage_const_2 -zeta_7**_sage_const_5 )*y+zeta_7**_sage_const_4 -zeta_7**_sage_const_3 )/((zeta_7**_sage_const_4 -zeta_7**_sage_const_3 )*x+(zeta_7-zeta_7**_sage_const_6 )*y+ zeta_7**_sage_const_2 -zeta_7**_sage_const_5 )]
gensYImg = [zeta_7*y, _sage_const_1 /x, ((zeta_7**_sage_const_2  - zeta_7**_sage_const_5 )*x+(zeta_7**_sage_const_4 -zeta_7**_sage_const_3 )*y + zeta_7-zeta_7**_sage_const_6 )/((zeta_7**_sage_const_4 -zeta_7**_sage_const_3 )*x+(zeta_7-zeta_7**_sage_const_6 )*y + zeta_7**_sage_const_2 -zeta_7**_sage_const_5 )]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_168 ,_sage_const_42 ])
compute_all_fixed_field(kxy, AutGrp)


# #(24,12)
# #(S_4)
# nicePrime = 13 #we only needs second root of unity

# k = FiniteField(nicePrime)
# kA.<a> = PolynomialRing(k)
# ka = kA.fraction_field()

# kx.<x> = FunctionField(ka)
# kxY.<Y> = kx[];

# kxy.<y> = kx.extension(Y^4 + x^4 + 1 + a*x^2*Y^2 + a*Y^2 + a*x^2)

# #Defining the group

# #x-> -x
# #y-> -y
# #x-> y, y-> x
# #x-> 1/x, y->y/x

# #searching for zeta_8
# f = kxy.hom([-x, y])

# gensXImg = [-x, x, 1/x, y]
# gensYImg = [y, -y, y/x, x]

# gensImg = zip(gensYImg, gensXImg)

# AutGrp = kxy.automorphism_group(gensImg, [24,12])
# # 
# compute_all_fixed_field(kxy, AutGrp)

#Hyperelliptic

#(24, 5) y^2=x(x^6-1)
kxy = kx.extension(Y**_sage_const_2  - x*(x**_sage_const_6 -_sage_const_1 ), names=('y',)); (y,) = kxy._first_ngens(1)

#Defininig generators based on gap gens
zeta_6 = a**((k.order()-_sage_const_1 )/_sage_const_6 )
zeta_4 = a**((k.order()-_sage_const_1 )/_sage_const_4 )
gensXImg = [-_sage_const_1 /x, zeta_6**_sage_const_3 *x, x, zeta_6**_sage_const_2 *x]
gensYImg = [y/x**_sage_const_4 , zeta_4 * y, -y, zeta_6*y]
gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_24 ,_sage_const_5 ])

compute_all_fixed_field(kxy, AutGrp)

#(48,48) y^2=x^8 + 14*x^4 + 1
kxy = kx.extension(Y**_sage_const_2  - x**_sage_const_8  -_sage_const_14 *x**_sage_const_4  - _sage_const_1 , names=('y',)); (y,) = kxy._first_ngens(1)

gensXImg = [x , zeta_4*x, zeta_4*(x+_sage_const_1 )/(x-_sage_const_1 )]
gensYImg = [-y, y       , -_sage_const_4 *y/(x-_sage_const_1 )**_sage_const_4        ]
gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_48 ,_sage_const_48 ])

compute_all_fixed_field(kxy, AutGrp)

#(12,4)
#(D_12)
#nicePrime = 13

#k = FiniteField(nicePrime)
kB = PolynomialRing(k, names=('b',)); (b,) = kB._first_ngens(1)
kb = kB.fraction_field()
kx = FunctionField(kb, names=('x',)); (x,) = kx._first_ngens(1)
kxY = kx['Y']; (Y,) = kxY._first_ngens(1);

#zeta_3 = proot^((k.order()-1)/3)
#zeta_6 = proot^((k.order()-1)/6)

kxy = kx.extension(Y**_sage_const_2  -x*(x**_sage_const_6  + b*x**_sage_const_3  + _sage_const_1 ), names=('y',)); (y,) = kxy._first_ngens(1)

#Defining the group

#x->zeta_3*x
#y->zeta_6*y
#x->1/x
#y->y/x^4

gensXImg = [_sage_const_1 /x,zeta_3*x]#, 1/x]  #,  x]
gensYImg = [y/x**_sage_const_4 , zeta_6*y]#, y/x^4]#, -y]

gensImg = zip(gensYImg, gensXImg)

AutGrp = kxy.automorphism_group(gensImg, [_sage_const_12 ,_sage_const_4 ])

compute_all_fixed_field(kxy, AutGrp)

#Special prime is needed     

