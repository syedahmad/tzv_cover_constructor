#F1 = FiniteField(7)['x,y'].fraction_field()
#x,y = F1.gens()
#print F1.hom([a,a])(x*y)

F2 = FiniteField(7)['a'].fraction_field()
a, = F2.gens()
phi = F2.hom([2*a])

k.<x> = FunctionField(F2)
tau = k.hom([x], phi)

F1 = FiniteField(7)['b,c'].fraction_field()
b,c = F1.gens()

l.<x> = FunctionField(F1)
R.<y> = l[]
Fxy.<y> = l.extension(y^2 - x^3-1)

lam = F2.hom([b+c])

psi = k.hom([Fxy(y)],lam)
