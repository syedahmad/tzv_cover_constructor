suspected_groups = ((72,42),(72,40),(36,12),(36,10),(18,3), (18, 5))

elig_sub = gap.SmallGroup([18,5])
for cur_grp_id in suspected_groups:
    G = gap.SmallGroup(cur_grp_id)
    if len(G.IsomorphicSubgroups(elig_sub)) > 0:
        print elig_sub.IdGroup(), " < ",G.IdGroup()
    else:
        print elig_sub.IdGroup(), " not < ",G.IdGroup()
