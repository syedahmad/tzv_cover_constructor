# This file was *autogenerated* from the file test_signatured_function_field.sage.
from sage.all_cmdline import *   # import sage library
_sage_const_3 = Integer(3); _sage_const_2 = Integer(2); _sage_const_1 = Integer(1); _sage_const_0 = Integer(0); _sage_const_4 = Integer(4); _sage_const_12 = Integer(12); _sage_const_17 = Integer(17); _sage_const_24 = Integer(24)
"""
Testing the signatured function fields of different types of quotient Group

AUTHORS:

Syed Ahmad Lavasani (2012)
"""

import unittest
from os.path import dirname
from os import getcwd, chdir
import sys
import pdb

try:
    src_dir  = dirname(dirname(__file__))
except NameError:
    #the best we can do to hope that we are in the test dir
    src_dir = dirname(getcwd())

src_dir = "/lu101/sahosse/Dropbox/doc/phd/sage/field_gen/"
sys.path.append(src_dir)

#adding the src dir to the path for importing
from pdb import set_trace

from sage.groups.perm_gps.permgroup_named import DihedralGroup
from signatured_function_field_constructor import SignaturedFunctionField
from jennifer_digest import JenniferDigest
from sage.misc.misc import set_verbose

class KnownValues(unittest.TestCase):
    pass

class BasicTests(unittest.TestCase):
    constant_field = FiniteField(_sage_const_17 )
    with_cyclic_quotient = ("[ 9, 1 ][ 0, 3, 9, 9 ][ 3, 2, 7 ][ f2, f1, f1^2*f2 ]","[ 14, 2 ][ 0, 2, 7, 14 ][ 2, 3, 14 ][ f1, f2, f1*f2^6 ]")

    with_dihedral_quotient = ("[ 16, 13 ][ 0, 2, 2, 2, 4 ][ 2, 3, 10, 4 ][ f1, f2, f1*f2*f3, f3 ]","[ 24, 5 ][ 0, 2, 4, 12 ][ 2, 6, 12 ][ f1, f1*f2*f4^2, f2*f3*f4 ]", "[ 12, 4 ][ 0, 2, 2, 2, 6 ][ 2, 2, 3, 6 ][ f1, f1*f3^2, f2, f2*f3 ]")
    
    with_a4_quotient = ("[ 48, 33 ][ 0, 2, 3, 12 ][ 7, 3, 11 ][ f1*f3, f2*f3, f1*f2^2*f4*f5 ]",)

    with_s4_quotient = ("[ 48, 48 ][ 0, 2, 4, 6 ][ 2, 10, 8 ][ f1, f1*f2*f3^2*f5, f2*f3*f4*f5 ]",)

    normal_abelian_fixers = ( "[ 36, 10 ][ 0, 2, 2, 2, 3 ][ 2, 3, 6, 9 ][ f1, f2, f1*f2*f3^2*f4^2, f3*f4 ]","[ 12, 5 ][ 0, 2, 2, 3, 6 ][ 2, 3, 4, 12 ][ f1, f2, f3, f1*f2*f3^2 ]",) #("[ 8, 3 ][ 0, 2, 2, 2, 2, 2 ][ 2, 2, 3, 3, 4 ][ f1, f1, f2, f2*f3, f3 ]", "[ 24, 12 ][ 0, 2, 2, 2, 3 ][ 2, 2, 4, 3 ][ f1, f1*f2^2, f3, f2*f4 ]",) # The genus 3 do not have normal decompositions
    test_digester_g3 = JenniferDigest(genus = _sage_const_3  )
    test_digester_g4 = JenniferDigest(genus = _sage_const_4  )


    def ntest_dihedral(self):
        """
        Testing the curve with the automorphism group with a cyclic sub
        group whose quotient is dihedral

        The signature is coming from Jeniffer's data
        """
        for cur_jennifer_sig in self.with_dihedral_quotient:
            print "Constructing function field for ", cur_jennifer_sig
            success_construct = False
            sign_grp = self.test_digester_g3.digest_single_group(cur_jennifer_sig)
        
            #finding the dihedral quetient
            for kummer_aut_grp in sign_grp.normal_subgroups():                
                if kummer_aut_grp.is_cyclic() and kummer_aut_grp.is_subgroup(sign_grp.center()):
                #testing if the quotient is isomorphic to the dihedral
                #group of the same size
                    quot_grp = sign_grp.quotient(kummer_aut_grp)
                    if is_even(quot_grp.order()) and quot_grp.is_isomorphic(DihedralGroup(quot_grp.order()/_sage_const_2 )) and sign_grp.sub_signature(kummer_aut_grp)[_sage_const_0 ] == _sage_const_0 :
                        gen_funcfield = SignaturedFunctionField(signatured_group = sign_grp, rationalizing_subgroup = kummer_aut_grp, constant_field = self.constant_field, names = ('y','x'))
                        print gen_funcfield
                        success_construct = True
        
            if not success_construct:
                raise ValueError, "No dihedral quotient found in %s", cur_jennifer_sig

    def ntest_cyclic(self):
        """
        Testing the curve with the automorphism group equal to cyclic
        group of order 14 of genus 3. This should be a hyperelliptic
        curve with function field y^2 = (x^7 - 1)

        The signature is coming from Jeniffer's data
        [ 9, 1 ][ 0, 3, 9, 9 ][ 3, 2, 7 ][ f2, f1, f1^2*f2 ]
        [ 14, 2 ][ 0, 2, 7, 14 ][ 8, 3, 6 ][ f1*f2^3, f2, f1*f2^2 ]
        """
        for cur_jennifer_sig in self.with_cyclic_quotient:
            print "Constructing function field for ", cur_jennifer_sig
            success_construct = False
            sign_grp = self.test_digester_g3.digest_single_group(cur_jennifer_sig)

            #finding the cyclic quetient
            for kummer_aut_grp in sign_grp.normal_subgroups():
                if kummer_aut_grp.is_cyclic() and kummer_aut_grp.is_subgroup(sign_grp.center()) and sign_grp.quotient(kummer_aut_grp).is_cyclic() and sign_grp.quotient(kummer_aut_grp).order()>_sage_const_1  and sign_grp.sub_signature(kummer_aut_grp)[_sage_const_0 ] == _sage_const_0 :
                    cyclic_funcfield = SignaturedFunctionField(signatured_group = sign_grp, rationalizing_subgroup = kummer_aut_grp, constant_field = self.constant_field, names = ('y','x'))

                    success_construct = True
                    print cyclic_funcfield
        
            if not success_construct:
                raise ValueError, "No cyclic quotient found in %s", cur_jennifer_sig


    def ntest_alternating_4(self):
        """
        Testing the curve with the automorphism group with a cyclic sub
        group whose quotient is A4

        The signature is coming from Jeniffer's data
        """
        for cur_jennifer_sig in self.with_a4_quotient:
            print "Constructing function field for ", cur_jennifer_sig
            success_construct = False
            sign_grp = self.test_digester_g3.digest_single_group(cur_jennifer_sig)
        
            #finding the dihedral quetient
            for kummer_aut_grp in sign_grp.normal_subgroups():                
                if kummer_aut_grp.is_cyclic() and kummer_aut_grp.is_subgroup(sign_grp.center()):
                #testing if the quotient is isomorphic to the dihedral
                #group of the same size
                    if sign_grp.quotient(kummer_aut_grp).group_id() == [_sage_const_12 , _sage_const_3 ]:
                        gen_funcfield = SignaturedFunctionField(signatured_group = sign_grp, rationalizing_subgroup = kummer_aut_grp, constant_field = self.constant_field, names = ('y','x'))
                        print gen_funcfield
                        success_construct = True
        
            if not success_construct:
                raise ValueError, "No alterantive quotient found in %s", cur_jennifer_sig

    def test_symmetric_4(self):
        """
        Testing the curve with the automorphism group with a cyclic sub
        group whose quotient is S4

        The signature is coming from Jeniffer's data
        """
        for cur_jennifer_sig in self.with_s4_quotient:
            print "Constructing function field for ", cur_jennifer_sig
            success_construct = False
            sign_grp = self.test_digester_g3.digest_single_group(cur_jennifer_sig)
        
            #finding the dihedral quetient
            for kummer_aut_grp in sign_grp.normal_subgroups():                
                if kummer_aut_grp.is_cyclic() and kummer_aut_grp.is_subgroup(sign_grp.center()):
                #testing if the quotient is isomorphic to the dihedral
                #group of the same size
                    if sign_grp.quotient(kummer_aut_grp).group_id() == [_sage_const_24 , _sage_const_12 ]:
                        gen_funcfield = SignaturedFunctionField(signatured_group = sign_grp, rationalizing_subgroup = kummer_aut_grp, constant_field = self.constant_field, names = ('y','x'))
                        print gen_funcfield
                        success_construct = True
        
            if not success_construct:
                raise ValueError, "No alterantive quotient found in %s", cur_jennifer_sig

    def ntest_normal_abelian(self):
        """
        Thes groups have a normal abelian subgroup whose fixed field is 
        rational. This test is to build such function fields
        """
        for cur_jennifer_sig in self.normal_abelian_fixers:
            print "Constructing function field for ", cur_jennifer_sig
            success_construct = False
            sign_grp = self.test_digester_g4.digest_single_group(cur_jennifer_sig)
        
            #finding the dihedral quetient
            for abelian_aut_grp in sign_grp.normal_subgroups():                
                if abelian_aut_grp.is_abelian() and (not abelian_aut_grp.is_cyclic() or abelian_aut_grp.is_cyclic()) and sign_grp.sub_signature(abelian_aut_grp)[_sage_const_0 ] == _sage_const_0 : #rational quotient
                    normal_decomposition = sign_grp.normally_decomposable(abelian_aut_grp)
                #testing if the quotient is isomorphic to the dihedral
                #group of the same size
                    if not normal_decomposition == None:
                        gen_funcfield = SignaturedFunctionField(signatured_group = sign_grp, rationalizing_subgroup = abelian_aut_grp, constant_field = self.constant_field, names = ('y','x'))
                        print gen_funcfield
                        success_construct = True
        
            if not success_construct:
                raise ValueError, "No normal abelian with normal decomposition found in %s", cur_jennifer_sig
        

if __name__ == "__main__":
    set_verbose(_sage_const_3 )
    unittest.main()
