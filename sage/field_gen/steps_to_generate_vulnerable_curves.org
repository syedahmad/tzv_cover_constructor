* Large automorphism group
** Get the signature from section 7.5 of the thesis.
** From MSSV01 check if it is hyperelliptic.
** If hyperelliptic 
*** open ~/doc/phd/sage/field_gen/grpmono05.txt
*** Find and copy the signature line
*** open ~/doc/phd/sage/field_gen/covers/5_24_14_cen.txt
*** replace the signature line
*** open a shell
   $ cd ~/doc/phd/sage/field_gen/
   $ sage gen_field.sage -g 5 -k covers/5_x_y_cen.txt
** If not hyperelliptic 
*** open ~/doc/phd/sage/field_gen/grpmono05.txt
*** Find and copy the signature line
*** open ~/doc/phd/sage/field_gen/covers/5_24_14_can.txt
*** Approach 1: open file:///files3/home/sahosse/doc/phd/Biblio/Sw-Students/results.html
    (open it in conkeror)
*** Find the signature
*** if there is root of unity then us sage to recover the cyclotomic polynomial (see example 192, 181)
*** copy the matrices from conkeror to the file.  
**** Replace ] with ],
**** add [ and ] at he 
**** Replace 1 with 1, 0 with 0, 
**** Add all missing cammas
**** replace constant with one in terms of root of unity
**** replace the relevant part in the equation as well. 
*** Replace the automorphism and equation parts
*** save the file with new approperiate name.
*** $ sage gen_field.sage -g 5 -c covers/5_x_y_can.txt
* Small automorphism group
